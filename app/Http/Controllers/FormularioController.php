<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use DataTables;

use App\Formulario;

class FormularioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Formulario::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('edit', function($row){
   
                        $btn = '<a href="javascript:void(0)" data-id="'.$row->id.'" class="edit btn btn-sm" data-toggle="modal" data-target="#staticBackdrop" id="edit"><i class="fas fa-edit text-info"></i></a>';
                            
                        return $btn;
                    })
                    ->addColumn('delete', function($row){
   
                        $btndos = '<button value="'.$row->id.'" data-id="'.$row->formulario.'" class="btn btn-sm deleteProduct" id="delete"><i class="fas fa-trash text-danger"></i></button>';
   
                        return $btndos;
                    })
                    ->rawColumns(['edit','delete'])
                    ->make(true);
        }

        $formularios = Formulario::get();
        return view('form_view', compact('formularios'));
    }

    
    public function create(Request $request)
    {
        if ($request->ajax()) {
            $agregar = new Formulario;

            try {
                $this->validate($request, ['formulario' => 'required|unique:formularios,formulario',
                                'formulario_fecha_publicacion_inicio' => 'required',
                                'formulario_fecha_publicacion_fin' => 'required',
                            ]);
            } catch (ValidationException $exception) {
               return response()->json($exception->validator->errors(), 422);
            }

            if ($request->input('formulario_descripcion') == null) {
                $description = 'no hay description';
            } else {
                $description = $request->input('formulario_descripcion');
            } 

            $agregar->formulario = $request->input('formulario');
            $agregar->formulario_descripcion = $description;
            $agregar->formulario_estatus = $request->input('formulario_estatus');
            $agregar->formulario_fecha_publicacion_inicio = $request->input('formulario_fecha_publicacion_inicio');
            $agregar->formulario_fecha_publicacion_fin = $request->input('formulario_fecha_publicacion_fin');
            $agregar->formulario_token = $request->_token;
            $agregar->save();
        
            if ($agregar->count()>=1) {
                return response()->json(['alert' => 'Se agrego corecctamente',
                                         'error' => 'falseCreate',
                                         'data' => [$agregar->id, $agregar->formulario_token]
                                    ]);
            } else {
                return response()->json(['alert' => 'HAY un error valida tus campos',
                                         'error' => true
                                    ]);
            }
        }
    }

    
    public function edit($id)
    {
        $formularios = Formulario::findOrFail($id);
        return response()->json($formularios);
    }

   
    public function update(Request $request, $id)
    {
        $update = Formulario::findOrFail($id);
    
        $request->validate([
            'formulario'   => ['required', Rule::unique('formularios')->ignore($update->id,'id')],
        ]);

        $formSearch = Formulario::where('id',$id)->get();
        $data = '';
        foreach ($formSearch as $value) {
            $data = $value;
        }

        if ($request->input('formulario_fecha_publicacion_inicio') == null) {
            $fechaInicio = $data->formulario_fecha_publicacion_inicio; 
        } else {
            $fechaInicio = $request->formulario_fecha_publicacion_inicio;
        }

        if ($request->input('formulario_fecha_publicacion_fin') == null) {
            $fechaFin = $data->formulario_fecha_publicacion_fin; 
        } else {
            $fechaFin = $request->formulario_fecha_publicacion_fin;
        }  

        if ($request->input('formulario_descripcion') == null) {
            $description = 'no hay description';
        } else {
            $description = $request->input('formulario_descripcion');
        }

        $update->formulario = $request->input('formulario');
        $update->formulario_descripcion = $description;
        $update->formulario_estatus = $request->input('formulario_estatus');
        $update->formulario_fecha_publicacion_inicio = $fechaInicio;
        $update->formulario_fecha_publicacion_fin = $fechaFin;
        $update->save();

        if ($update->count()>=1) {
            return response()->json(["alert"=>"Se ha ACTUALIZADO corecctamente", "error"=>false]);
        }else {
            return response()->json(["alert"=>"Acurrio un error valida tus campos", "error"=>true]);
        }
    }

    
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            $delete = Formulario::findOrFail($id);
            $delete->delete();

            if ($delete->count() >= 1) {
                return response()->json(['alert' => 'se elimino correctamente "'.$delete->formulario.'"', 'error' => false]);
            } else {
                return response()->json(['alert' => 'Ocurrio un error al eliminar "'.$delete->formulario.'"', 'error' => true]);
            }
        }
    }
}
