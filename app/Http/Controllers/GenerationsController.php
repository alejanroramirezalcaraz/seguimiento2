<?php

namespace App\Http\Controllers; 

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use DataTables;
use App\ProgramaEducativo;
use App\User;

class GenerationsController extends Controller
{
	
    public function index(Request $request)
    {
    	if ($request->ajax()) {
            $data = DB::table('generaciones')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('edit', function($row){
   
                           $btn = '<a href="javascript:void(0)" data-id="'.$row->id.'" class="edit btn btn-sm" data-toggle="modal" data-target="#staticBackdrop" id="edit"><i class="fas fa-edit text-info"></i></a>';
                            
                        return $btn;
                    })
                    ->addColumn('delete', function($row){
   
                        $btndos = '<button value="'.$row->id.'" data-id="'.$row->id.'" class="btn btn-sm deleteProduct" id="delete"><i class="fas fa-trash text-danger"></i></button>';
   
                        return $btndos;
                    })
                    ->rawColumns(['edit','delete'])
                    ->make(true);
        }
      
        $users = User::get();
        $generaciones = DB::table('generaciones')->get();
        $programas = DB::table('programas_educativos')->get();
       
        return view('generations_view',['generaciones'=>$generaciones,'programas'=> $programas]);
    }

      public function created(Request $request)
    {
        if ($request->ajax()) {
            $agregar = new User;

            try {
                $this->validate($request, [ ]);
            } catch (ValidationException $exception) {
               return response()->json($exception->validator->errors(), 422);
            }

             DB::table('generaciones')->insert([
            'id'=>null,
            'generacion_codigo'=>$request->input('codigo'),
            'generacion'=>$request->input('generacion'),
            'generacion_programa_educativo'=>$request->input('programa'),
            'generacion_programa_descripcion'=>$request->input('descripcion'),
            'generacion_nivel'=>$request->input('nivel'),
            'generacion_modalidad'=>$request->input('modalidad'),
            'generacion_cuatrimestre_egreso'=>$request->input('cuatrimestre'),
            'generacion_anio'=>$request->input('año'),
            'generacion_estatus'=>$request->input('estado'),

        ]); 
        
            if ($agregar->count()>=1) {
                return response()->json(['alert' => 'Se agrego corecctamente', 'error' => false]);
            } else {
                return response()->json(['alert' => 'HAY un error valida tus campos', 'error' => true]);
            }
        }
    }

     public function delete(Request $request, $id)
    {

        if ($request->ajax()) {
            
            $delete = DB::table('generaciones')->where('id', $id)->delete();


            return response()->json(['alert' => 'Se elimino corecctamente', 'error' => false]);
            
        }
    }

     public function json($nombre)

    {

        $nombre = ProgramaEducativo::where('programa_nombre',$nombre)->get();

header('Content-type: application/json; charset=utf-8');
return response()->json(['nombre'=>$nombre]);
       
     
    }

     public function edit($id)
    {
      
        $users = DB::table('generaciones')->where('id',$id)->first();

        return response()->json($users);
    }

    public function update(Request $request, $id)
    {
        

        DB::table('generaciones')
        ->where('id',$id)
        ->update([
  'generacion_codigo'=>$request->input('codigo'),
            'generacion'=>$request->input('generacion'),
            'generacion_programa_educativo'=>$request->input('programa'),
            'generacion_programa_descripcion'=>$request->input('descripcion'),
            'generacion_nivel'=>$request->input('nivel'),
            'generacion_modalidad'=>$request->input('modalidad'),
            'generacion_cuatrimestre_egreso'=>$request->input('cuatrimestre'),
            'generacion_anio'=>$request->input('año'),
            'generacion_estatus'=>$request->input('estado'),

        ]);
        
        
        return response()->json(["alert"=>"Se ha ACTUALIZADO corecctamente", "error"=>false]);

        
    }
    
}
