<?php

namespace App\Http\Controllers;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use nusoap_client;
class StudentController extends Controller
{
	public function index(Request $request)
	{
		$respuestas = DB::table('respuestas')->get()->where('respuesta_reporte_id',$request->listado_reporte_id); 
		
		$preguntas = DB::table('preguntas')->get();

		$opciones = DB::table('opciones')->get();

		$alumnos = DB::table('alumnos')->get()->where('alumno_anio_egreso',$request->listado_anio_egreso)->where('alumno_programa_educativo',$request->listado_programa_educativo); 

        $programa_educativo = $request->listado_programa_educativo;
        $año_egreso=$request->listado_anio_egreso;
		return view('student_view',["respuestas"=>$respuestas,"preguntas"=>$preguntas,"opciones"=>$opciones,'alumnos'=>$alumnos,'programa_educativo'=>$programa_educativo,'año_egreso'=>$año_egreso]);



		
	}



	public function prueba(){

		$alumno = new \nusoap_client('http://www.mi-escuelamx.com/ws/wsUTSEM/Datos.asmx?wsdl', true);

		$alumno->soap_defencoding = 'UTF-8';
		$alumno->decode_utf8 = FALSE;

		$params = ['lsGeneracion'=>'2016',
		'lsCarrera'=>'AS'];

		$result = $alumno->call('AlumnosGeneracion', $params);

		$data = '';
		if ($result) {

			$data = $result['AlumnosGeneracionResult'];
			return $data;

		/*	foreach ($data as $datos ) {
				
	

			DB::table('alumnos')->insert([


				'alumno_id' => null,
				'alumno_nombre'=> $datos->['mail'],
				'alumno_ap_paterno'=>null,
				'alumno_ap_materno'=>null,
				'alumno_genero'=>null,
				'alumno_matricula'=>null,
				'alumno_generacion'=>null,
				'alumno_programa_educativo'=>null,
				'email'=>null,
				'alumno_telefono'=>null,
				'alumno_anio_egreso'=>null,
				'created_at'=> null,
				'updated_at'=> null,

			]);

		}

		*/

		}




	}
}
