<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;


class DataCaptureController extends Controller
{
    public function index()
    {
    	$reportes = DB::table('reportes')->get(); 
        return view('data_capture_view',['reportes'=>$reportes]);
    }

    public function programsCapture(Request $request)
    {
        // esta line devuelve un exsel con los usuarios 
        //return Excel::download(new UsersExport, 'users.xlsx');

    	$listado = DB::table('listado')->get()->where('listado_reporte_id',$request->id); 
    	$alumnos = DB::table('alumnos')->get();
    	$preguntas = DB::table('preguntas')->get();
    	$opciones = DB::table('opciones')->get();
        $reporte_id = $request->id;
        $reporte = DB::table('reportes')->where('reporte_id',$request->id)->first(); 

        return view('data_capture_programs_view',['listado'=>$listado,'alumnos'=>$alumnos,'preguntas'=>$preguntas,'opciones'=>$opciones,'reporte_id'=>$reporte_id,'reporte'=>$reporte]);

    }


    public function excel_reporte(Request $request){


 // esta line devuelve un exsel con los usuarios 
 //return Excel::download(new UsersExport,'users.xlsx');

        $listado = DB::table('listado')->get()->where('listado_reporte_id',$request->valor); 
        $alumnos = DB::table('alumnos')->get();
        $preguntas = DB::table('preguntas')->get();
        $opciones = DB::table('opciones')->get();
        $reporte_id = $request->id;
        $respuestas = DB::table('respuestas')->get()->where('respuesta_reporte_id',$request->valor);
        return view('general_report_view',['listado'=>$listado,'alumnos'=>$alumnos,'preguntas'=>$preguntas,'opciones'=>$opciones,'reporte_id'=>$reporte_id,'respuestas'=>$respuestas]);


        return view('general_report_view');
        
    }

    public function lista(Request $request)
    {
        // esta line devuelve un exsel con los usuarios 
        //return Excel::download(new UsersExport, 'users.xlsx');

        $listado = DB::table('listado')->get()->where('listado_reporte_id',$request->id); 
        $alumnos = DB::table('alumnos')->get()->where('alumno_programa_educativo',$request->listado_programa_educativo)->where('alumno_anio_egreso',$request->listado_anio_egreso);
        $preguntas = DB::table('preguntas')->get();
        $opciones = DB::table('opciones')->get();
        $respuestas = DB::table('respuestas')->get()->where('respuesta_reporte_id',$request->reporte_id);

        $programa_educativo = $request->listado_programa_educativo;
        $año_egreso=$request->listado_anio_egreso;


        return view('list_view',['listado'=>$listado,'alumnos'=>$alumnos,'preguntas'=>$preguntas,'opciones'=>$opciones,'respuestas'=>$respuestas,'programa_educativo'=>$programa_educativo,'año_egreso'=>$año_egreso]);

    }

    
    public function report2(Request $request)
    {
        $respuestas = DB::table('respuestas')->get()->where('respuesta_reporte_id',$request->listado_reporte_id); 
        
        $preguntas = DB::table('preguntas')->get();

        $opciones = DB::table('opciones')->get();

        $alumnosf= DB::table('alumnos')->get()->where('alumno_anio_egreso',$request->listado_anio_egreso)->where('alumno_programa_educativo',$request->listado_programa_educativo)->where('alumno_genero','Femenino');

        $alumnosm = DB::table('alumnos')->get()->where('alumno_anio_egreso',$request->listado_anio_egreso)->where('alumno_programa_educativo',$request->listado_programa_educativo)->where('alumno_genero','Masculino'); 


        $programa_educativo = $request->listado_programa_educativo;
        $año_egreso=$request->listado_anio_egreso;


        
        return view('report2_view',["respuestas"=>$respuestas,"preguntas"=>$preguntas,"opciones"=>$opciones,'alumnosf'=>$alumnosf,'alumnosm'=>$alumnosm,'programa_educativo'=>$programa_educativo,'año_egreso'=>$año_egreso]);



        
    }



















}
