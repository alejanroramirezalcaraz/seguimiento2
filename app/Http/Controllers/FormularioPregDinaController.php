<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

use App\Formulario;
use App\Pregunta;
use App\Opcion;

class FormularioPregDinaController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }


    public function indexFormSend(Request $request, $id, $token)
    {
        $data = Formulario::where([
                    ['id', $id],
                    ['formulario_token', $token]
                ])
                ->select(['id','formulario_token','formulario','formulario_descripcion','formulario_fecha_publicacion_inicio','formulario_fecha_publicacion_fin'])
                ->get();
        return view('form_view_ask', compact('data'));
    }


    public function updateForm(Request $request, $id)
    {
        if ($request->ajax()) {

            $dataFormulario ='';
            // $dataFormularioDescripcion = '';

            $formulario = array($request->input('formulario'));
            $array_num_formulario = count($formulario);
            for ($i = 0; $i < $array_num_formulario; $i++){
                $dataFormulario = $formulario[$i];
            }

            // $formularioDes = array($request->input('formulario_descripcion'));
            // $array_num_formularioDes = count($formulario);
            // for ($i = 0; $i < $array_num_formularioDes; $i++){
            //     $dataFormularioDescripcion = $formularioDes[$i];
            // }

            $updateRelacion = DB::table('formularios')
                        ->where('id',$id)
                        ->update([
                            'formulario' => $dataFormulario,
                            'formulario_descripcion' => $request->input('formulario_descripcion')
                                ]);
        }
    }


    public function createPregForm(Request $request)
    {
        if ($request->ajax()) {

            $pregunta = array($request->pregunta);
            $valPregunta = '';
            foreach ($pregunta as $value) {
                $valPregunta = $value;
            }

            $token = $request->token;
            $verPreg = Pregunta::where('pregunta_token', $token)->get();

            if ($request->pregunta_obligatorio == null) {
                $pregunta_obligatorio = 'on';
            } else {
                $pregunta_obligatorio = $request->pregunta_obligatorio;
            }

            if ($verPreg->count() >= 1) {
                $updatePreg = DB::table('preguntas')
                        ->where('pregunta_token', $token)
                        ->update(['pregunta' => $valPregunta,
                                'pregunta_obligatorio' => $pregunta_obligatorio,
                                'pregunta_token' => $token]);
            } else {
                $addPreg = new Pregunta;
                $addPreg->pregunta = $valPregunta;
                $addPreg->pregunta_obligatorio = $pregunta_obligatorio;
                $addPreg->pregunta_token = $token;
                $addPreg->save();

                if ($addPreg->count() >= 1) {
                    $AddRelForPreg =DB::table('formulario_pregunta')
                                    ->insert([
                                        'formulario_id'=>$request->input('formulario_id'),
                                        'pregunta_id'=>$addPreg->id,
                                        'created_at' =>  \Carbon\Carbon::now(),
                                        'updated_at' => \Carbon\Carbon::now(),   
                                    ]);
                }
            }
            return response()->json(true);            
        }
    }

    
    public function createPregOpc(Request $request)
    {
        if ($request->ajax()) {

            $verOpcion = Opcion::where('opcion_futura_token', $request->opcion_futura_token)->get();
            $verPreg = Pregunta::where('pregunta_token', $request->token)->get();
            $pregId='';
            foreach ($verPreg as $preg) {
               $pregId = $preg->id;
            }

            if ($request->opcion_futura != null || $request->opcion ==1 || $request->opcion == 2 || $request->opcion == 6) {
                if ($verOpcion->count() >= 1) {
                    $updatePreg = DB::table('opciones')
                                ->where([['opcion_futura_token', $request->opcion_futura_token]])
                                ->update([
                                        'opcion_pregunta_id'=>$pregId,
                                        'opcion' => $request->opcion,
                                        'opcion_futura' => $request->opcion_futura,
                                        'opcion_token' => $request->token,
                                        'opcion_futura_token' => $request->opcion_futura_token,
                                        ]);

                    if ($updatePreg >= 1) {
                            $verPreg = Pregunta::where('pregunta_token', $request->token)->get();
                            $verPregOpc = Opcion::where('opcion_futura_token', $request->opcion_futura_token)->select('id')->get();
                            foreach ($verPregOpc as $opc) {
                                foreach ($verPreg as $preg) {
                                $AddRelForPreg =DB::table('pregunta_opcion')
                                                        ->where([['pregunta_id',$preg->id],['opcion_id',$verPregOpc->id]])
                                                        ->update([
                                                            'pregunta_id' => $preg->id,
                                                            'opcion_id'   => $verPregOpc->id,
                                                            'created_at'  => \Carbon\Carbon::now(),
                                                            'updated_at'  => \Carbon\Carbon::now(),   
                                                        ]);
                                }
                            }
                        }
                } else {
                        $addOption = new Opcion;
                        $addOption->opcion_pregunta_id = $pregId;
                        $addOption->opcion = $request->opcion;
                        $addOption->opcion_futura = $request->opcion_futura;
                        $addOption->opcion_token = $request->token;
                        $addOption->opcion_futura_token = $request->opcion_futura_token;
                        $addOption->save();

                        if ($addOption->count() >= 1) {
                            $verPreg = Pregunta::where('pregunta_token', $request->token)->get();
                            foreach ($verPreg as $preg) {
                            $AddRelForPreg =DB::table('pregunta_opcion')
                                                    ->insert([
                                                        'pregunta_id' => $preg->id,
                                                        'opcion_id'   => $addOption->id,
                                                        'created_at'  => \Carbon\Carbon::now(),
                                                        'updated_at'  => \Carbon\Carbon::now(),   
                                                    ]);
                            }
                        }
                }
            }
            
        }

    }


    public function deletePregDin($id)
    {
        $DelPregDin = DB::table('preguntas')->where('pregunta_token', $id)->delete();
        if ($DelPregDin>=1) {
            return response()->json(['error' => true]);
        } else {
            return response()->json(['error' => false]);            
        }
    }

        
    public function deletePregDinOpc($id)
    {
        $DelOpciDin = DB::table('opciones')->where('opcion_token', $id)->delete();
        if ($DelOpciDin>=1) {
            return response()->json(['error' => true]);
        } else {
            return response()->json(['error' => false]);            
        }
    }

    
    public function deleteAllOpc($id)
    {
        $DelAllOpc = DB::table('opciones')->where('opcion_token',$id)->delete();
        if ($DelOpciDin>=1) {
            return response()->json(['error' => true]);
        } else {
            return response()->json(['error' => false]);            
        }
    }
}
