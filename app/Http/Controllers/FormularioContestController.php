<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;

use App\Formulario;
use App\Generacion;
use App\ProgramaEducativo;
use App\Respuesta;

class FormularioContestController extends Controller
{
       public function formEncuesta($forID, $aluID, $token, $codigo)
    {
        $viewForm = Formulario::where('id',$forID)->select('id','formulario','formulario_descripcion')->get();

        $viewFormPreg = DB::table('formulario_pregunta')
                            ->join('formularios','formulario_pregunta.formulario_id','=','formularios.id')
                            ->join('preguntas','formulario_pregunta.pregunta_id','=','preguntas.id')
                            ->select('preguntas.id',
                                    'preguntas.pregunta',
                                    'preguntas.pregunta_obligatorio',
                                    'preguntas.pregunta_token')
                            ->where('formulario_pregunta.formulario_id',$forID)
                            ->get();

        // $viewFormPregOpc = DB::table('formulario_pregunta')
        //                     ->join('formularios','formulario_pregunta.formulario_id','=','formularios.id')
        //                     ->join('preguntas','formulario_pregunta.pregunta_id','=','preguntas.id')
        //                     ->join('pregunta_opcion','preguntas.id','=','pregunta_opcion.pregunta_id')
        //                     ->join('opciones','pregunta_opcion.opcion_id','=','opciones.id')
        //                     ->select('opciones.id',
        //                             'opciones.opcion',
        //                             'opciones.opcion_token',
        //                             'opciones.opcion_futura',
        //                             'opciones.opcion_futura_token',
        //                             'preguntas.pregunta_obligatorio')
        //                     ->where('formulario_pregunta.formulario_id',$forID)
        //                     ->get();
        
    
        return view('form_view_alumns', compact('viewForm','viewFormPreg','aluID', 'codigo'));
        // return view('form_view_alumns', compact('viewForm','viewFormPreg','aluID','viewFormPregOpc'));
            // return $viewFormPreg;
            // return $viewPregOpc;
            // return $resFP;
    }

    
    public function formOpcion($pregID)
    {        
        $viewPregOpc = DB::table('pregunta_opcion')
                            ->join('preguntas','pregunta_opcion.pregunta_id','=','preguntas.id')
                            ->join('opciones','pregunta_opcion.opcion_id','=','opciones.id')
                            ->select('opciones.id',
                                    'opciones.opcion',
                                    'opciones.opcion_token',
                                    'opciones.opcion_futura',
                                    'opciones.opcion_futura_token',
                                    'preguntas.pregunta_obligatorio')
                            ->where('pregunta_opcion.pregunta_id', $pregID)
                            ->get();
                            
        if ($viewPregOpc->count() >= 1) {
            return response()->json(['value' => $viewPregOpc, 'error' => false]);
        }else{
            return response()->json(['value' => 'no hay datos...', 'error' => true]);
        }
    }

    
    public function formEncuestaAdd(Request $request)
    {
        // return $request;
        if ($request->ajax()) {
             
            $pregunta=[];
            $opcion=[];
            $preguntas=[];
            $opciones=[];
            $opcionFutura=[];


            // foreach ($request->pregunta_token as $key => $prId) {
            //     $pregunta = $prId;                 
            //     foreach ($request->input('pregunta_id'.$pregunta) as $preg) {
            //         $preguntas = $preg;
            //         foreach ($request->opcion_token as $key => $opId) {
            //                 $opcion = $opId;
            //             foreach ($request->input('opfu'.$opcion) as $opfu) {
            //                 $opcionFutura = $opfu;
            //                 return $opcionFutura;
            //                 if ($opfu != null) {
            //                     if ($pregunta == $opcion) {
            //                         $addRespuesta = new Respuesta;
            //                         $addRespuesta->respuesta_reporte_id = null;
            //                         $addRespuesta->respuesta_formulario_id = $request->formulario_id;
            //                         $addRespuesta->respuesta_pregunta_id = $preguntas;
            //                         $addRespuesta->respuesta = $opcionFutura;
            //                         $addRespuesta->respuesta_alumno_id = $request->alumno_id;
            //                         $addRespuesta->respuesta = 'no hay respuesta';
            //                         $addRespuesta->respuesta_status = '1';
            //                         $addRespuesta->save();
            //                     }
            //                 } else {
            //                     foreach ($request->input($opcion) as $opcioID) {
            //                         if ($pregunta == $opcion) {
            //                             $addRespuesta = new Respuesta;
            //                             $addRespuesta->respuesta_reporte_id = null;
            //                             $addRespuesta->respuesta_formulario_id = $request->formulario_id;
            //                             $addRespuesta->respuesta_pregunta_id = $preguntas;
            //                             $addRespuesta->respuesta_opcion_id = $opcioID;
            //                             $addRespuesta->respuesta_alumno_id = $request->alumno_id;
            //                             $addRespuesta->respuesta = 'no hay respuesta';
            //                             $addRespuesta->respuesta_status = '1';
            //                             $addRespuesta->save();
            //                         }
            //                     }
            //                 }
            //             }
            //         }
            //     }    
            // }

            foreach ($request->pregunta_token as $key => $prId) {
                $pregunta = $prId;                 
                foreach ($request->input('pregunta_id'.$pregunta) as $preg) {
                    $preguntas = $preg;
                    foreach ($request->opcion_token as $key => $opId) {
                            $opcion = $opId;
                        foreach ($request->input($opcion) as $opcioID) {
                            if ($pregunta == $opcion) {
                                $addRespuesta = new Respuesta;
                                $addRespuesta->respuesta_reporte_id = null;
                                $addRespuesta->respuesta_formulario_id = $request->formulario_id;
                                $addRespuesta->respuesta_pregunta_id = $preguntas;
                                $addRespuesta->respuesta_opcion_id = $opcioID;
                                $addRespuesta->respuesta_alumno_id = $request->alumno_id;
                                $addRespuesta->respuesta = 'no hay respuesta';
                                $addRespuesta->respuesta_codigo = $request->codigo;
                                $addRespuesta->respuesta_status = '1';
                                $addRespuesta->save();
                            }
                        }
                    }
                }     
            }
            
            if ($addRespuesta->count() >= 1) {
                return response()->json(['resp' => $request->formulario, 'error' => false]);
            } else {
                return response()->json(['resp' => $request->formulario, 'error' => true]);
            }
                       
        }
    }

    
    public function formViewAlumnsEnd(Request $request)
    {   
        // return $request;
        return View::make('form_view_alumns_end')->with(['error' => $request->error, 'data' => $request->data]);
        // return view('form_view_alumns_end', ['error' => $request->error, 'data' => $request->data]);

        // return Redirect::to('/form/encuesta/view/end')->with(['error' => $request->error, 'data' => $request->data]);

        // return response("status", 301)
        // ->header('location', 'http://desarrolloweb.com');
// 
        // return response("Esta página se refrescará en 5 segundos hacia...", 200)
        // ->header('Cache-Control', 'max-age=3600')
        // ->header('Refresh', '5; url=http://www.desarrolloweb.com');

        // return response()
        // ->view('form_view_alumns_end', ['error' => $request->error, 'data' => $request->data])
        // ->header('Status Code', 200)
        // ->header('Refresh', '1; url=/form/encuesta/view/end');
    }
}
