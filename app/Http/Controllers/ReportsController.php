<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use DataTables;

use App\User;
use App\Reporte;

class ReportsController extends Controller
{
    public function index(Request $request)
    {
    	if ($request->ajax()) {
            $data = DB::table('reportes')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('edit', function($row){
                           $btn = '<a href="javascript:void(0)" data-id="'.$row->reporte_id.'" class="edit btn btn-sm" data-toggle="modal" data-target="#staticBackdrop" id="edit"><i class="fas fa-edit text-info"></i></a>';
                            
                        return $btn;
                    })
                    ->addColumn('delete', function($row){
                        $btndos = '<button value="'.$row->reporte_id.'" data-id="'.$row->reporte_nombre.'" class="btn btn-sm deleteProduct" id="delete"><i class="fas fa-trash text-danger"></i></button>';
   
                        return $btndos;
                    })
                    ->rawColumns(['edit','delete'])
                    ->make(true);
        }
      
        $users = User::get();
        $reportes = DB::table('reportes')->get();

        $respuestas = DB::table('respuestas')
                        ->join('formularios','formularios.id','=','respuestas.respuesta_formulario_id')
                        ->select('respuestas.respuesta_formulario_id','respuestas.respuesta_codigo','formularios.formulario')
                        ->groupBy('respuestas.respuesta_formulario_id','respuestas.respuesta_codigo','formularios.formulario')
                        ->get();

        return view('reports_view', compact('reportes', 'respuestas'));
    }


    public function created(Request $request)
    {
        if ($request->ajax()) {

            $agregar = new User;

            try {
                $this->validate($request, [
                                'reporte_nombre'            => 'required',
                                'reporte_codigo'            => 'required',
                                'reporte_respuesta_codigo'  => 'required',     
                            ]);
            } catch (ValidationException $exception) {
               return response()->json($exception->validator->errors(), 422);
            }

            $reportes = DB::table('reportes')->get();
            foreach ($reportes as $key) {
                if ($key->reporte_codigo == $request->input('reporte_codigo')) {
                	return response()->json(['alert' => 'código de reporte existente', 'error' => true]);
                }	
            }

			// $agregar = DB::table('reportes')->insert([
			// 'reporte_id' => null,
			// 'reporte_nombre' => $request->input('user_nombre'),
			// 'reporte_codigo' => $request->input('reporte_codigo'),
			// ]);

            $agregar= new Reporte;
            $agregar->reporte_nombre = $request->input('reporte_nombre');
            $agregar->reporte_codigo = $request->input('reporte_codigo');
            $agregar->save();

            if ($agregar->count() >= 1) {
               $updateResp = DB::table('respuestas')
                        ->where([['respuesta_formulario_id',$request->reporte_formulario_id],
                                ['respuesta_codigo',$request->reporte_respuesta_codigo]])
                        ->update(['respuesta_reporte_id' => $agregar->reporte_id]);  
            }
           
            $reporte = DB::table('reportes')->where('reporte_codigo',$request->input('reporte_codigo'))->first(); 

            $generaciones = DB::table('generaciones')->get();


            foreach ($generaciones as $generacion) {
                DB::table('listado')->insert([
                    'listado_id'=>null, 
                    'listado_reporte_id'=>$reporte->reporte_id,
                    'listado_programa_educativo'=>$generacion->generacion_programa_descripcion,
                    'listado_codigo'=>$generacion->generacion_codigo,
                    'listado_nivel_educativo'=>$generacion->generacion_nivel,
                    'listado_modalidad'=>$generacion->generacion_modalidad,
                    'listado_cuatrimestre_de_egreso'=>$generacion->generacion_cuatrimestre_egreso,  
                    'listado_anio_egreso'=>$generacion->generacion_anio,
                ]);
            }

		 return response()->json(['alert' => 'Se agrego corecctamente', 'error' => false]);
            
         
        }
    }


    public function delete(Request $request, $id)
    {

        if ($request->ajax()) {
        	$delete = DB::table('reportes')->where('reporte_id', $id)->delete();
            return response()->json(['alert' => 'Se elimino corecctamente', 'error' => false]);
            
        }
    }

    public function edit($id)
    {
        $users = DB::table('reportes')->where('reporte_id',$id)->first();

        $respuestas = DB::table('respuestas')
                        ->join('formularios','formularios.id','=','respuestas.respuesta_formulario_id')
                        ->select('respuestas.respuesta_formulario_id','respuestas.respuesta_codigo','formularios.formulario')
                        ->where('respuesta_reporte_id', $id)
                        ->get();

        return response()->json(['users' => $users, 'respuestas' => $respuestas]);
    }

    public function update(Request $request, $id)
    {
        $usersUpdate = User::findOrFail($id);

        $upReporte = Reporte::find($id);
        $request->validate([
            'reporte_codigo'   => ['required', Rule::unique('reportes')->ignore($upReporte->id,'id')
                ],
            'user_nombre' => ['required', Rule::unique('reportes')->ignore($upReporte->id,'id')
                ],
        ]);
        $upReporte->reporte_nombre = $request->input('reporte_nombre');
        $upReporte->reporte_codigo = $request->input('reporte_codigo');
        $upReporte->save();

        if ($upReporte->count() >= 1) {
            $upResp = DB::table('respuestas')
                        ->where([['respuesta_formulario_id',$request->reporte_formulario_id],
                                ['respuesta_codigo',$request->reporte_respuesta_codigo]])
                        ->update(['respuesta_reporte_id'=>$upReporte->reporte_id]);
        }

        // DB::table('reportes')
        // ->where('reporte_id',$id)
        // ->update([
        //     'reporte_nombre' => $request->input('user_nombre'),
        //     'reporte_codigo' => $request->input('reporte_codigo'),
        // ]);
                
        return response()->json(["alert"=>"Se ha ACTUALIZADO corecctamente", "error"=>false]);

    }


}

