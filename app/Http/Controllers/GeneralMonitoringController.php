<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GeneralMonitoringController extends Controller
{
  public function index(Request $request)
  {
   $respuestas = DB::table('respuestas')->get()->where('respuesta_reporte_id',$request->listado_reporte_id); 
   
   $preguntas = DB::table('preguntas')->get();

   $opciones = DB::table('opciones')->get();

   $alumnos = DB::table('alumnos')->get()->where('alumno_anio_egreso',$request->listado_anio_egreso)->where('alumno_programa_educativo',$request->listado_programa_educativo); 
   $listado =DB::table('listado')->where('listado_reporte_id',$request->listado_reporte_id);
   

    $programa_educativo = $request->listado_programa_educativo;
        $año_egreso=$request->listado_anio_egreso;

   return view('general_monitoring_view',["respuestas"=>$respuestas,"preguntas"=>$preguntas,"opciones"=>$opciones,'alumnos'=>$alumnos,"listado"=>$listado,'programa_educativo'=>$programa_educativo,'año_egreso'=>$año_egreso]);
}

 

public function grafic()
{

   return "hola";
   $reportes = DB::table('reportes')->get(); 
   
   return view('general_monitoring_view',['reportes'=>$reportes]);
}
}
