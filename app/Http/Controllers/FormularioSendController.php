<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use App\Formulario;
use App\Generacion;
use App\ProgramaEducativo;
use App\Respuesta;

use App\Mail\messageLinkReceived;

class FormularioSendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }    

    public function index(Request $request)
    {
        $proEdu = ProgramaEducativo::where('programa_estatus',1)->get(); 
        $gen = Generacion::where('generacion_estatus',1)->get();
        $form = Formulario::where('formulario_estatus',1)->get();
        return view('form_send_email', compact('proEdu','gen','form'));
    }

    public function formSearchAlum($programa, $generacion)
    {
            try {
                $alumno = new \nusoap_client('http://www.mi-escuelamx.com/ws/wsUTSEM/Datos.asmx?wsdl', true);

                if ($alumno->getError()) {
                    return '<h2>Constructor error</h2><pre>' . $err . '</pre>';
                }

                $alumno->soap_defencoding = 'UTF-8';
                $alumno->decode_utf8 = FALSE;

                $params = ['lsGeneracion' => $generacion ? $generacion : '',
                           'lsCarrera' => $programa ? $programa : ''];

                $resultData = $alumno->call('AlumnosGeneracion', $params);

                if ($resultData) {
                    $verificData = $resultData['AlumnosGeneracionResult']['diffgram'];
                }
            
            if ($verificData == '' || count($verificData) == 0) {
                return response()->json(['data' => 'Buscando datos...', 'error' => true]); 
            } else {
                    
                $data = $verificData['NewDataSet']['Generacion'];

                $cons = array();
                foreach ($data as $datareq) {
                    $cons = DB::table('alumnos')->where('alumno_matricula', $datareq['matricula'])->get();
                }

                // return $cons;
                    if ($cons->count() >= 1) {
                        foreach ($data as $reqUpd) {
                            $updateAlumnos = DB::table('alumnos')
                            ->where('alumno_matricula',$reqUpd['matricula'])
                            ->update([
                                    'alumno_nombre'                =>  $reqUpd['nombre'],
                                    'alumno_ap_paterno'            =>  $reqUpd['apaterno'],
                                    'alumno_ap_materno'            =>  $reqUpd['amaterno'],
                                    'alumno_genero'                =>  $reqUpd['sexo'],
                                    'alumno_matricula'             =>  $reqUpd['matricula'],
                                    'alumno_generacion'            =>  'no hay dato',
                                    'alumno_programa_educativo'    =>  $reqUpd['desc_carrera'],
                                    'email'                        =>  $reqUpd['mail'],
                                    'alumno_telefono'              =>  $reqUpd['telefono'],
                                    'alumno_anio_egreso'           =>  $generacion,
                                    'alumno_nivel'                 =>  $reqUpd['desc_nivel'],
                                    'alumno_situacion'             =>  $reqUpd['desc_situacion'],
                                    'created_at'                   =>  \Carbon\Carbon::now(),
                                    'updated_at'                   =>  \Carbon\Carbon::now()
                            ]);
                        }
                            if ($updateAlumnos >= 1) {
                                return response()->json(['data'         => $data, 
                                                        'lsGeneracion'  => $generacion,
                                                        'lsCarrera'     => $programa,
                                                        'error'         => false]);
                            } else {
                                return response()->json(['data' => 'no hay datos actualizados...', 'error' => true]);
                            }
                    } else {
                        foreach ($data as $reqAdd) {
                            $addAlumnos = DB::table('alumnos')->insert([
                                        'alumno_nombre'                =>  $reqAdd['nombre'],
                                        'alumno_ap_paterno'            =>  $reqAdd['apaterno'],
                                        'alumno_ap_materno'            =>  $reqAdd['amaterno'],
                                        'alumno_genero'                =>  $reqAdd['sexo'],
                                        'alumno_matricula'             =>  $reqAdd['matricula'],
                                        'alumno_generacion'            =>  'no hay dato',
                                        'alumno_programa_educativo'    =>  $reqAdd['desc_carrera'],
                                        'email'                        =>  $reqAdd['mail'],
                                        'alumno_telefono'              =>  $reqAdd['telefono'],
                                        'alumno_anio_egreso'           =>  $generacion,
                                        'alumno_nivel'                 =>  $reqAdd['desc_nivel'],
                                        'alumno_situacion'             =>  $reqAdd['desc_situacion'],
                                        'created_at'                   =>  \Carbon\Carbon::now(),
                                        'updated_at'                   =>  \Carbon\Carbon::now()
                                ]);
                        }
                            if ($addAlumnos >= 1) {
                                return response()->json(['data'         => $data, 
                                                        'lsGeneracion'  => $generacion,
                                                        'lsCarrera'     => $programa,
                                                        'error'         => false]);
                            } else {
                                return response()->json(['data' => 'no hay datos nuevos...', 'error' => true]);
                            }
                    }
                }  
            } catch(Exception $e) {
                return $e->getMessage();
            }
    }


    public function sendFormEmail(Request $request)
    {   
        // return $request;
        if ($request->ajax()) {
            try {
            $data = request()->validate([
                            'formulario'        => 'required',
                            'fecha_publicacion' => 'required',
                            'alumno'            => 'required',
                            'csrf_token'        => 'required'
                        ]);

            $seId = DB::table('alumnos')->where('alumno_matricula', $request->alumno)->get();

            $alumno_id='';
            foreach ($seId as $value) {
                $alumno_id = $value->alumno_id;
            }

            } catch (ValidationException $exception) {
               return response()->json($exception->validator->errors(), 422);
            }
                foreach ($request->formulario as $formulario) {
                    $dataSeach = DB::table('formulario_alumno')->insert([
                        'formulario_id'         =>  $formulario,
                        'alumno_id'             =>  $alumno_id,
                        'email'                 =>  $request->email,
                        'token'                 =>  $request->csrf_token,
                        'fecha_publicacion'     =>  $request->fecha_publicacion,
                        'created_at'            => \Carbon\Carbon::now(),
                        'updated_at'            => \Carbon\Carbon::now(), 
                    ]);
                }

                $array = ['formulario'      => $request->formulario,
                            'alumno'        => $alumno_id,
                            'csrf_token'    => $request->csrf_token,
                            'codigo'        => $request->codigo];

                if ($dataSeach >= 1)  {

                    Mail::to('ramirezalcarazcesaralejandro@gmail.com')->send(new messageLinkReceived($array));
                    // return new messageLinkReceived($array);
                    return response()->json(['resp' => 'se han mandado correctamente a '. $request->email, 'error' => false], 200);
                } else {
                    return response()->json(['resp' => 'Ocurrio un error...', 'error' => true], 200);
                }       
        }
    }



}
