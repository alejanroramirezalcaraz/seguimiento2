<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function showLoginForm()
    {
         return view('auth.login');
    }

    public function login(Request $request)
    {
        $credentials = $this->validate(request(), [
            $this->username()   => 'required|string',
            'password'          => 'required|string'
        ]);


        $admin = User::where('email', $request['email'])->get();
        $ad = '';
        foreach ($admin as $key => $value) {
            $ad = $value;
        }
        
            if ($ad != null || $ad != '') {
                foreach ($admin as $ad) {
                    $password = $request['password'];
                    if($ad['email'] == $request['email'])
                    {
                        if (password_verify($password, $ad['password'])) 
                            {   
                                if ($ad['user_estatus'] == 1)
                                {
                                    if (Auth::guard()->attempt($credentials, $request['remember'])) 
                                        {
                                            return redirect()->intended('/users');
                                        }
                                }else{
                                    return redirect('/login')->with('alert-danger','No cuenta con el acceso para iniciar session...');
                                }
                            }else {
                                return back()->withErrors(['password' => 'La contraseña NO coincide con su correo'])
                                             ->withInput($request->only([$this->username(), 'remember']));
                            }  
                    }
                }
            }else{
                return view('auth.login')
                            ->withErrors(['email' => 'NO coincide su correo registrado con nuestros datos'])
                            ->withInput($request->only([$this->username(), 'remember']));
            }
    }
    
    // protected $redirectTo = '/users';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/');
    }
}
