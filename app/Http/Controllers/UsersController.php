<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use DataTables;

use App\User;

class UsersController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
   
    public function index(Request $request)
    {
    	if ($request->ajax()) {
            $data = User::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('edit', function($row){
   
                           $btn = '<a href="javascript:void(0)" data-id="'.$row->id.'" class="edit btn btn-sm" data-toggle="modal" data-target="#staticBackdrop" id="edit"><i class="fas fa-edit text-info"></i></a>';
                            
                        return $btn;
                    })
                    ->addColumn('delete', function($row){
   
                        $btndos = '<button value="'.$row->id.'" data-id="'.$row->user_nombre.'" class="btn btn-sm deleteProduct" id="delete"><i class="fas fa-trash text-danger"></i></button>';
   
                        return $btndos;
                    })
                    ->rawColumns(['edit','delete'])
                    ->make(true);
        }
      
        $users = User::get();
        return view('users_view', compact('users'));  
    }

    public function created(Request $request)
    {
        if ($request->ajax()) {
            $agregar = new User;

            try {
                $this->validate($request, ['email' => 'required|unique:users,email',
                                'user_nombre' => 'required',
                                'user_ap_paterno' => 'required',
                                'user_ap_materno' => 'required',
                                'user_telefono' => 'required|unique:users,user_telefono',
                                'user_nivel' => 'required',
                                'user_estatus' => 'required',
                                'password' => 'required',
                            ]);
            } catch (ValidationException $exception) {
               return response()->json($exception->validator->errors(), 422);
            }

            $agregar->user_nombre = $request->input('user_nombre');
            $agregar->user_ap_paterno = $request->input('user_ap_paterno');
            $agregar->user_ap_materno = $request->input('user_ap_materno');
            $agregar->user_telefono = $request->input('user_telefono');
            $agregar->user_nivel = $request->input('user_nivel');
            $agregar->user_estatus = $request->input('user_estatus');
            $agregar->email = $request->input('email');
            $agregar->password = Hash::make($request->input('password'));
            $agregar->save();
        
            if ($agregar->count()>=1) {
                return response()->json(['alert' => 'Se agrego corecctamente', 'error' => false]);
            } else {
                return response()->json(['alert' => 'HAY un error valida tus campos', 'error' => true]);
            }
        }
    }

    public function edit($id)
    {
        $users = User::findOrFail($id);
        return response()->json($users);
    }

    public function update(Request $request, $id)
    {
        $usersUpdate = User::findOrFail($id);
    
        $request->validate([
            'email'   => ['required', Rule::unique('users')->ignore($usersUpdate->id,'id')],
            'user_telefono' => ['required', Rule::unique('users')->ignore($usersUpdate->id,'id')],
        ]);

        $search = User::where('password', $request->input('password'))->get();
       
        $data = '';
        foreach ($search as $res) {
            $data = $res;
        }
        if ($search->count() >= 1) {
            $password = $data->password; 
        } else {
            $password = Hash::make($request->password);
        } 
        
        $usersUpdate->user_nombre = $request->input('user_nombre');
        $usersUpdate->user_ap_paterno = $request->input('user_ap_paterno');
        $usersUpdate->user_ap_materno = $request->input('user_ap_materno');
        $usersUpdate->user_telefono = $request->input('user_telefono');
        $usersUpdate->user_nivel = $request->input('user_nivel');
        $usersUpdate->user_estatus = $request->input('user_estatus');
        $usersUpdate->email = $request->input('email');
        $usersUpdate->password = $password;
        $usersUpdate->save();

        if ($usersUpdate->count()>=1) {
            return response()->json(["alert"=>"Se ha ACTUALIZADO corecctamente", "error"=>false]);
        }else {
            return response()->json(["alert"=>"Acurrio un error valida tus campos", "error"=>true]);
        }
    }

    public function delete(Request $request, $id)
    {
        if ($request->ajax()) {
            $delete = User::findOrFail($id);
            $delete->delete();

            if ($delete->count() >= 1) {
                return response()->json(['alert' => 'se elimino correctamente "'.$delete->user_nombre.'"', 'error' => false]);
            } else {
                return response()->json(['alert' => 'Ocurrio un error al eliminar "'.$delete->user_nombre.'"', 'error' => true]);
            }
            
        }
    }
}
