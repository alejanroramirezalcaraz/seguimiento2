<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\ProgramaEducativo;

class EducationalProgramsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index(Request $request)
    {
        $alumno = new \nusoap_client('http://www.mi-escuelamx.com/ws/wsUTSEM/Datos.asmx?wsdl', true);

        $alumno->soap_defencoding = 'UTF-8';
        $alumno->decode_utf8 = FALSE;

        $result = $alumno->call('Carreras');

        $data = array();
        if ($result) {
            $data = $result['CarrerasResult']['diffgram']['NewDataSet']['Carreras'];
        }
        	
        for ($i = 0; $i < count($data); $i++) {

            $consPr = ProgramaEducativo::where('programa_nombre',  $data[$i]['cve_carrera'])->get();

            if($consPr->count() >= 1) {
                $update = DB::table('programas_educativos')->where('programa_nombre',$data[$i]['cve_carrera'])
                                ->update(['programa_codigo' => 'ninguno',
                                  'programa_nombre' => $data[$i]['cve_carrera'],
                                  'programa_descripcion' => $data[$i]['descripcion'],
                                  'programa_area' => 'ninguno',
                                  'programa_estatus' => '1',
                                ]);            
            } else {
                $add = DB::table('programas_educativos')
                            ->insert(['programa_codigo' => 'ninguno',
                                      'programa_nombre' => $data[$i]['cve_carrera'],
                                      'programa_descripcion' => $data[$i]['descripcion'],
                                      'programa_area' => 'ninguno',
                                      'programa_estatus' => '1',
                                    ]);
            }
        }

        if ($request->ajax()) {
            $data = ProgramaEducativo::latest()->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('edit', function($row){
   
                           $btn = '<a href="javascript:void(0)" data-id="'.$row->programas_id.'" class="edit btn btn-sm" data-toggle="modal" data-target="#staticBackdrop" id="edit"><i class="fas fa-edit text-info"></i></a>';
                            
                        return $btn;
                    })
                    ->addColumn('delete', function($row){
   
                        $btndos = '<button value="'.$row->programas_id.'" data-id="'.$row->programa_nombre.'" class="btn btn-sm deleteProduct" id="delete"><i class="fas fa-trash text-danger"></i></button>';
   
                        return $btndos;
                    })
                    ->rawColumns(['edit','delete'])
                    ->make(true);
        }
      
        $programas_educativos = DB::table('programas_educativos')->get();
        return view('educational_programs_view',['programas_educativos'=>$programas_educativos]);      
    }
}
