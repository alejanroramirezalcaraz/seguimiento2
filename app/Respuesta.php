<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    protected $table = 'respuestas';
    protected $primaryKey = 'id';
    protected $fillable = ['respuesta','respuesta_codigo','respuesta_status']; 	
  	protected $foreighKey = ['respuesta_reporte_id',
							'respuesta_formulario_id',
							'respuesta_pregunta_id',
							'respuesta_opcion_id',
							'respuesta_alumno_id'];
}
