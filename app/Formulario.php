<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pregunta;

class Formulario extends Model
{
    protected $table = 'formularios';
    protected $primaryKey = 'id';
    protected $fillable = [
        'formulario', 
        'formulario_descripcion',
        'formulario_estatus',
        'formulario_fecha_publicacion_inicio',
        'formulario_fecha_publicacion_fin',
        'formulario_token'
    ];

    public function Pregunta()
    {
        return $this->belongsToMany(Pregunta::class)->withTimestamps();
    }
}
