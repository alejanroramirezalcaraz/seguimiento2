<?php

namespace App;

use App\Notifications\ResetPasswordNotification;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'user_nombre', 
        'user_ap_paterno',
        'user_ap_materno',
        'user_telefono',
        'user_nivel',
        'user_estatus',
        'email',
        'password'
    ];

    protected $hidden = [
        'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

}
