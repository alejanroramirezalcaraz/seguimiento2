<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;

class Handler extends ExceptionHandler
{

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) 
        {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }
        else if ($exception instanceof \Illuminate\Auth\AuthenticationException)
        {
           return redirect('/login')->with('alert-info','por favor es necesario iniciar session...');
        }

        return parent::unauthenticated($request, $exception);
    }

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);

        // if ($exception instanceof API\APIError)
        //    return \Response::json(['code' => '...', 'msg' => '...']);
        // return parent::render($request, $exception);

        // $error = $this->convertExceptionToResponse($exception);
        // $response = [];
        // if($error->getStatusCode() == 422) {
        //    $response['error'] = $exception->getMessage();
        //    if(Config::get('app.debug')) {
        //        $response['trace'] = $exception->getTraceAsString();
        //        $response['code'] = $exception->getCode();
        //    }
        // }
        // return response()->json($response, $error->getStatusCode());

       //  if ($request->ajax() || $request->wantsJson()) {

       //     $exception = $this->prepareException($exception);

       //     if ($exception instanceof \Illuminate\Http\Exception\HttpResponseException) {
       //         return $exception->getResponse();
       //     } elseif ($exception instanceof \Illuminate\Auth\AuthenticationException) {
       //         return $this->unauthenticated($request, $exception);
       //     } elseif ($exception instanceof \Illuminate\Validation\ValidationException) {
       //         return $this->convertValidationExceptionToResponse($exception, $request);
       //     }

       //     // we prepare custom response for other situation such as modelnotfound
       //     $response = [];
       //     $response['error'] = $exception->getMessage();

       //     if(config('app.debug')) {
       //         $response['trace'] = $exception->getTrace();
       //         $response['code'] = $exception->getCode();
       //     }

       //     // we look for assigned status code if there isn't we assign 500
       //     $statusCode = method_exists($exception, 'getStatusCode') 
       //                     ? $exception->getStatusCode()
       //                     : 422;

       //     return response()->json($response, $statusCode);
       // }

       // return parent::render($request, $exception);

    }
}
