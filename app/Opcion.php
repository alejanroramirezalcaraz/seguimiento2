<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Pregunta;

class Opcion extends Model
{
    protected $table = 'opciones';
    protected $primaryKey = 'id';
    protected $fillable = [
					    'opcion',
					    'opcion_futura',
					    'opcion_token',
					    'opcion_futura_token'
					    ];

	protected $foreighKey = 'opcion_pregunta_id';
	
	public function Pregunta()
    {
        return $this->belongsToMany(Pregunta::class)->withTimestamps();
    }
}
