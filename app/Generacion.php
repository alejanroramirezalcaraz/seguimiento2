<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Generacion extends Model
{
    protected $table = 'generaciones';
    protected $primaryKey = 'id';
	protected $fillable = [
							'generacion',
							'generacion_codigo',
							'generacion_anio',
							'generacion_estatus'
							];

}
