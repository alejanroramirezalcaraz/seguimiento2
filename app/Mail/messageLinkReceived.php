<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class messageLinkReceived extends Mailable
{
    use Queueable, SerializesModels;

    // public $subjectr = 'UTSEM';
    public $demo;

    public function __construct($demo)
    {
        $this->demo = $demo;
    }

    public function build()
    {
        return $this->view('emails.message_link')
                    ->subject('Universidad tecnologica del sur del estado de morelos.');
    }
}
