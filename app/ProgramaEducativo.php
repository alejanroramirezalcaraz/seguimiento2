<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramaEducativo extends Model
{
    protected $table = 'programas_educativos';
    protected $primaryKey = 'programas_id';
    protected $fillable = [
    						'programas_id',
    						'programa_codigo',
    						'programa_nombre',
    						'programa_descripcion',
    						'programa_area',
    						'programa_estatus'
    						];
}
