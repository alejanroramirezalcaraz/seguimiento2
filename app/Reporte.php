<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reporte extends Model
{
    protected $table = 'reportes';
    protected $primaryKey = 'reporte_id';
    protected $fillable = [
					'reporte_codigo',
					'reporte_nombre',
					    ];
	public $timestamps = false;
}
