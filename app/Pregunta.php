<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Formularios;
use App\Generacion;
use App\Pregunta;

class Pregunta extends Model
{
    protected $table = 'preguntas';
    protected $primaryKey = 'id';
    protected $fillable = [
					    'pregunta',
					    'pregunta_obligatorio',
					    'pregunta_token'
					    ];

	public function Formularios()
    {
        return $this->belongsToMany(Formularios::class)->withTimestamps();
    }

    public function Opcion()
    {
        return $this->belongsToMany(Opcion::class)->withTimestamps();
    }
}
