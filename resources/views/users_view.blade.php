<!---- 
PROPÓSITO DE ESTA SECCIÓN: es la plantilla general del sistema
NOMBRE DEL DESARROLLADOR:alejandro ramirez
FECHA:2/10/2020

MODIFICADO POR:
alejandro ramirez -> 02/10/2020
jesus chirinos -> 15/02/2020
----> 

@extends('template')

@section('seccion')
<div class="shadow p-3 mb-5 bg-white rounded">
  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
    <h5 class="text-success float-left">Usuarios</h5>
    <a type="button" href="javascript:void(0)" class="btn btn-outline-success" data-toggle="modal" data-target="#staticBackdrop" id="add">Agregar<i class="fas fa-plus text-success"></i></a>  
  </div>

  <div class="container ">
    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">        
          <table id="showDataTable" class="table table-striped table-bordered align-items-center table-flush table-hover align-self-center text-center" cellspacing="0" width="100%">
            <thead class="thead-light">
                <tr>
                  <th>#</th>
                  <th>Nombre</th> 
                  <th>Apellido paterno</th>
                  <th>Apellido materno</th>
                  <th>Teléfono</th>
                  <th>Área o departamento</th>
                  <th>Correo electrónico</th>
                  <th>Estatus</th>
                  <th>Editar</th>
                  <th>Eliminar</th>
               </tr>
            </thead>
            <tfoot>
              <tr>
                  <th>#</th>
                  <th>Nombre</th> 
                  <th>Apellido paterno</th>
                  <th>Apellido materno</th>
                  <th>Teléfono</th>
                  <th>Área o departamento</th>
                  <th>Correo electrónico</th>
                  <th>Estatus</th>
                  <th>Editar</th>
                  <th>Eliminar</th>
               </tr>
            </tfoot>
            <tbody>            
            </tbody>        
          </table>                  
        </div>
      </div>
    </div>  
  </div>    
</div>



<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabeTitulModal">Registro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">

          <div class="row justify-content-center">
            <div class="col-sm-11">
              <div class="alert alert-danger" id="alerta"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              </div>
            </div>
          </div>

           <form class="form-group" action="#" id="formControl" method="POST" enctype="multipart/form-data" >

           {{ csrf_field() }}

          <input type="hidden" name="id" id="id">

          <div class="form-row">
            <div class="form-group col-sm-4">
              <label>Nombre de usuario *</label>
              <input required="" type="text" class="form-control" name="user_nombre" id="user_nombre">
              <span class="text-danger span" id="user_nombre_errors"></span>      
            </div>

            <div class="form-group col-sm-4">
              <label>apellido paterno*</label>
              <input required="" type="text" class="form-control" name="user_ap_paterno" id="user_ap_paterno">
              <span class="text-danger span" id="user_ap_paterno_errors" >El campo es obligatorio</span>
            </div>

            <div class="form-group col-sm-4">
              <label>apellido materno*</label>
              <input required="" type="text" class="form-control" name="user_ap_materno" id="user_ap_materno">
              <span class="text-danger span" id="user_ap_materno_errors" >El campo es obligatorio</span>
            </div>
          </div>
          
          <div class="form-group">
            <label>Telefono*</label>
            <input required="" type="tel" class="form-control" name="user_telefono" maxlength="10" oninput="if(this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" id="user_telefono">
            <span class="text-danger span" id="user_telefono_errors" >El campo es obligatorio</span>
          </div>

          <div class="form-row">
            <div class="form-group col-sm-6">
              <label>Area o departamento *</label>
              <input required="" type="text" class="form-control" name="user_nivel" id="user_nivel">
              <span class="text-danger span" id="user_nivel_errors" >El campo es obligatorio</span>
            </div>

            <div class="form-group col-sm-6">
              <label>Estatus</label>
              <select class="form-control" name="user_estatus" id="user_estatus">
                <option value="1">Activo</option>
                <option value="0">Inactivo</option>
              </select>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-sm-6">
              <label>Correo electrónico *</label>
              <input required="" type="email" class="form-control" name="email" id="email">
              <span class="text-danger span" id="email_errors" >El campo es obligatorio</span>
            </div>

            <div class="form-group col-sm-6">
              <label>Contraseña *</label>
              <input type="password" class="form-control" name="password" id="password" placeholder="***********************************************">
              <span class="text-danger span" id="password_errors" >El campo es obligatorio</span>
            </div>
          </div> 

          <button type="submit" class="btn btn-outline-success float-right mb-3 mt-3" id="btn">Cargando</button>
        </form>
      </div>
    </div>
  </div>
</div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css"/>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

<script>
$(function(){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

    $('#alerta').hide();
    $('.span').hide();

    var table = $('#showDataTable').DataTable({
        // dom: 'fB<"top"l>rt<"bottom"ip><"clear">',
        dom: '<"float-left"Bl><"float-right"f>t<"float-left"i><"float-right"p>',
        buttons: {
            dom: {
                button: {
                    className: 'btn btn-outline-secundary mr-1 mb-2' //Primary class for all buttons
                }
            },
            buttons: [                  
                {
                    extend: 'excelHtml5', 
                    className: 'btn btn-outline-success',
                    title: 'reporte_en_excel',
                },
                {
                    extend: 'pdf',
                    className: 'btn btn-outline-danger',
                    title: 'reporte_en_pdf',
                },
                {
                    extend: 'copy',
                    className: 'btn btn-outline-info',
                    // text: '<i class="fas fa-print"></i> IMPRIMIR',
                    title: 'Copiado',
                }
            ]
        },
        retrieve: true,
        deferRender: true,
        processing: true,
        serverSide: true,
        ajax: "{{ route('index.users') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'user_nombre', name: 'user_nombre'},
            {data: 'user_ap_paterno', name: 'user_ap_paterno'},
            {data: 'user_ap_materno', name: 'user_ap_materno'},
            {data: 'user_telefono', name: 'user_telefono'},
            {data: 'user_nivel', name: 'user_nivel'},
            {data: 'email', name: 'email',
            'render': function(data, type, row) {
                return '<span class="badge badge-primary">'+data+'</span>';
            }},
            {data: 'user_estatus', width: '10', className: 'text-center',
            'render': function(data, type, row) {
                return (data == 1) ? '<span class="badge text-success">Activo</span>' 
                    : '<span class="badge text-danger"> Inactivo </span>';
            }},
            {data: 'edit', name: 'edit'},
            {data: 'delete', name: 'delete', orderable: false, searchable: false},
        ],"language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando del _START_ al _END_ con un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          },
          "buttons": {
              "copy": "Copiar",
              "colvis": "Visibilidad",
              "pdf": "<i class='fas fa-file'></i>",
              "excel": "<i class='fas fa-file-excel'></i>"
          }
      }
    });


    $('#add').click(function () {
        $('#formControl').trigger("reset");
        $('#alerta').hide();
        $(".span").hide();
        $("#staticBackdropLabeTitulModal").show().html('Registrar Usuario');
        $('#btn').show().html('Registrar');
        $("#formControl").attr("action","{{Route('created.users')}}");
    });


   $('body').on('click', '#edit', function () {
      var id = $(this).data('id');
     
      $.get("{{url('/users/search/edit/')}}" +'/'+ id, function (data) {
          $("#staticBackdropLabeTitulModal").show().html('<h6>Usuario: '+ data.user_nombre + ' ' + data.user_ap_paterno + ' ' + data.user_ap_materno + '</h6>');
          $('#alerta').hide();
          $(".span").hide();
          $('#btn').show().html('Actualizar');
          $("#formControl").attr("action","{{url('/users/update/')}}" +'/'+ id);
          $('#id').val(data.id);
          $('#user_nombre').val(data.user_nombre);
          $('#user_ap_paterno').val(data.user_ap_paterno);
          $('#user_ap_materno').val(data.user_ap_materno);
          $('#user_telefono').val(data.user_telefono);
          $('#user_nivel').val(data.user_nivel);
          $('#user_estatus').val(data.user_estatus);
          $('#email').val(data.email);
          $('#password').val(data.password);
      })
    });


    $(document).ready(function(){
      $('#formControl').on('submit', function(e){
        e.preventDefault();
        $.ajax({
          url: $(this).attr('action'),
          type: $(this).attr('method'),
          data: $(this).serialize(),
          dataType: 'json',
          beforeSend: function(){
            $('#btn').fadeIn().html('Enviando...');
            setTimeout(function(){ $('#btn').fadeIn() }, 1000);
          },
          success: function(response){
            // $('#formControl').trigger("reset");
            $('#staticBackdrop').modal('hide');
            $("#formControl")[0].reset();
            if (response.error == false) {
              toastr.success(response.alert, "Usuarios", {
                  "closeButton": true,
                  "debug": true,
                  "newestOnTop": false,
                  "progressBar": true,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "showDuration": "13000",
                  "hideDuration": "1000",
              });
              table.draw();
            }else if (response.error == true){
              toastr.error(response.alert, "Usuarios", {
                  "closeButton": true,
                  "debug": true,
                  "newestOnTop": false,
                  "progressBar": true,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "showDuration": "13000",
                  "hideDuration": "1000",
              });
            }   
          },
          error: function(response){
            if (response.status === 422) {
              var saltos = '';
              var errors = response.responseJSON.errors;
              $.each(errors, function(key, val) {
                $('#alerta').show().html(saltos += val[0] + '<br>').fadeIn();
                setTimeout(function(){ $('#alerta').fadeOut(); }, 10000);
              });
              if (errors['user_nombre']) {
                $('#user_nombre_errors').show().html(errors['user_nombre'][0]);
              }
              if (errors['user_ap_paterno']) {
                $('#user_ap_paterno_errors').show().html(errors['user_ap_paterno'][0]);
              }
              if (errors['user_ap_materno']) {
                $('#user_ap_materno_errors').show().html(errors['user_ap_materno'][0]);
              }
              if (errors['user_telefono']) {
                $('#user_telefono_errors').show().html(errors['user_telefono'][0]);
              }
              if (errors['user_nivel']) {
                $('#user_nivel_errors').show().html(errors['user_nivel'][0]);
              }
              if (errors['email']) {
                $('#email_errors').show().html(errors['email'][0]);
              }
              if (errors['password']) {
                $('#password_errors').show().html(errors['password'][0]);
              }
            }
          }
        });
      });
    });


   $(document).on('click', '#delete', function () {
      var id = $(this).val();
      var user = $(this).data('id');
      swal({
        title: "¿Seguro que deseas continuar?",
        text: "Eliminar a "+user+", no podrás deshacer este paso!",
        icon: "warning",
        buttons: [true, "¡Aceptar!"],
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajax({
            method: 'DELETE',
            url: "{{url('/users/delete/')}}/"+id,
            dataType: 'json',
            data: {id:id},
            success: function (response) {
              if (response.error == false) {
                swal({
                  title : "Correcto!",
                  text  : response.alert,
                  icon  : "success",
                })
                table.draw();
              }else if (response.error == true){
                swal({
                  title : "Ocurrio un error dentro del servidor!",
                  text  : response.alert,
                  icon  : "error",
                });
              }
            }
          });
        } else {
          swal({title : "Cancelado",
            text  : "Tu registro está seguro..."});
        }
      });
    });


  });
</script>

@endsection