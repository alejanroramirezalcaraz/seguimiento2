<!--
----------------------------------------------------------------------------------
PROPÓSITO DE ESTA SECCIÓN: vista donde se muestera  el data table de los programas educativos 
NOMBRE DEL DESARROLLADOR:alejandro ramirez
FECHA:4/10/2020
---------------------------------------------------------------------------------
-->
@extends('template')

@section('seccion')

<div class="shadow p-3 mb-5 bg-white rounded">
  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
    <span class="float-left">
      <h5 class="text-success">historial de encuestas </h5>
    </span>
      <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#staticBackdrop">Agregar<i class="fas fa-plus text-success"></i></button>  
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">        
          <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
              <th>#</th> 
              <th>pereodo de captura</th>               
              <th >estatus laboral</th>
              
              <th>Colocación de primer empleo</th>
              <th>colocado por la universidad</th>
              <th>localidad del empleo</th>
              <th>tipo de organización</th>
              <th>tamaño de la organización</th>
              <th>sueldos con prestaciones integradas</th>
              <th>ocupación</th>
              <th>continuidad de estudios</th>
              <th>edad al egresr</th>
            
                       </tr>
                   </thead>
                   <tbody>
                    <tr>
                      <td>1</td>
                      <td>enero 2019</td>
                      <td>solo trabaja </td>
                      
                      <td>un año</td>
                      <td> si-no</td>
                      <td>ZONA DE INFLUENCIA </td>
                      <td>privada</td>
                      <td>pequeña</td>
                      <td>hasta 8 salarios minimos</td>  
                      <td>supervisor</td>  
                      <td>misma univercidad</td>
                      <td>23-24</td>

                    </tr>
                    <tr>
                      <td>1</td>
                      <td>enero 2018</td>
                      <td>solo trabaja </td>
                      
                      <td>un año</td>
                      <td> si-no</td>
                      <td>ZONA DE INFLUENCIA </td>
                      <td>privada</td>
                      <td>pequeña</td>
                      <td>hasta 8 salarios minimos</td>  
                      <td>supervisor</td>  
                      <td>misma univercidad</td>
                      <td>23-24</td>

                    </tr>
                    <tr>
                      <td>1</td>
                      <td>enero 2017</td>
                      <td>solo trabaja </td>
                      
                      <td>un año</td>
                      <td> si-no</td>
                      <td>ZONA DE INFLUENCIA </td>
                      <td>privada</td>
                      <td>pequeña</td>
                      <td>hasta 4 salarios minimos</td>  
                      <td>operador</td>  
                      <td>misma univercidad</td>
                      <td>23-24</td>

                    </tr>
                    <tr>
                      <td>1</td>
                      <td>enero 2016</td>
                      <td>solo trabaja </td>
                      
                      <td>un año</td>
                      <td> si-no</td>
                      <td>ZONA DE INFLUENCIA </td>
                      <td>privada</td>
                      <td>pequeña</td>
                      <td>hasta 4 salarios minimos</td>  
                      <td>oprador</td>  
                      <td>misma univercidad</td>
                      <td>23-24</td>

                    </tr>
                    <tr>
                      <td>1</td>
                      <td>enero 2015</td>
                      <td>solo trabaja </td>
                      
                      <td>un año</td>
                      <td> si-no</td>
                      <td>ZONA DE INFLUENCIA </td>
                      <td>privada</td>
                      <td>pequeña</td>
                      <td>hasta 4 salarios minimos</td>  
                      <td>operador</td>  
                      <td>misma univercidad</td>
                      <td>23-24</td>

                    </tr>
          
         </tbody>        
     </table>                  
 </div>
</div>
</div>  
</div>    
</div>
@endsection