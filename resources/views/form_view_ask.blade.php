<!--
PROPÓSITO DE ESTA SECCIÓN: formualrios 
NOMBRE DEL DESARROLLADOR: Jesus chirinos
FECHA:18-10-2020

MODIFICADO POR:

-->

@extends('template')

@section('seccion')

<div class="loader-page">
  <label class="d-flex justify-content-center text-loading">Cargando...</label>
</div>

<div class="shadow mb-5 bg-white rounded">
  <div class="card-header d-flex flex-row align-items-center justify-content-between">
    <div class="container">
      <form id="formDatos" method="POST">
      {{ csrf_field() }}
      @foreach ($data as $da)
        <small id="tituloForm">Fecha de publicaciòn: {{$da->formulario_fecha_publicacion_inicio}} - Fecha de cierre: {{$da->formulario_fecha_publicacion_fin}}</small>
        <div class="dropdown-divider"></div>
        <div class="row mt-3">
          <div class="col-sm-12">
            <div class="form-group">
              <label>Formulario</label>
              <input type="hidden" id="idForm" name="id" value="{{$da->id}}">
              <input type="text" id="formulario" name="formulario" class="form-control form-control-lg" placeholder="Titulo del formulario" value="{{$da->formulario}}">
            </div>

            <div class="form-group">
              <label>Descripción</label>
              <input readonly type="text" id="formulario_descripcion" name="formulario_descripcion" class="form-control form-control-sm" placeholder="Descripciòn del formulario" value="{{$da->formulario_descripcion}}">
            </div>
          </div>
        </div>
      @endforeach
      </form>
    </div>
  </div> 
</div>


<div class="shadow bg-white rounded contenedorPreguntas" id="">
  <div class="card-header d-flex flex-row align-items-center justify-content-between contenedorPreguntasDos" id="contenedorPreguntasDos">
    <div class="container">
        
      <form id="formPreguntas" class="formPreguntas" method="POST">
        {{ csrf_field() }}
        
        <div class="obligatorio">
          <label class="switch">
            <input type="checkbox" class="pregunta_obligatorio" name="pregunta_obligatorio" value="off">
            <span class="slider round"></span>
          </label>
          <label class="textObligatorio">campo obligatorio *</label>
        </div>

        <!-- <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input" id="customSwitch1">
          <label class="custom-control-label" for="customSwitch1">Toggle this switch element</label>
        </div> -->

        <input type="hidden" class="formulario_id" name="formulario_id" value="@foreach($data as $da){{$da->id}}@endforeach">    
        <input type="hidden" name="token" class="idToken">

        <div class="form-row">
          <div class="form-group col-sm-6">
            <label for="inputEmail4">Pregunta</label>
            <input type="text" name="pregunta" id="pregunta" class="form-control form-control-lg border border-top-0 border-right-0 border-left-0 pregunta" placeholder="pregunta sin título...">
          </div>

          <div class="form-group col-sm-6">
            <label for="inputEmail4">Tipo de pregunta</label>
            <select class="form-control form-control-lg seleccionTipoPregunta" name="opcion">
              <option selected="" hidden="">selecciona una respuesta...</option>
              <!-- <option value="1">Respuesta corta o abierta</option> -->
              <!-- <option value="2">Respuesta de párrafo</option> -->
              <option value="3">Casilla de verificación</option>
              <option value="4">Opción múltiple</option>
              <option value="5">Lista desplegable</option>
              <!-- <option value="6">Fecha y hora</option> -->
            </select>
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-sm tipoPregunta">
            <!-- <fieldset id="text_undefined_0" class="cont_opcion_futura">
              <input type="hidden" id="opcion" class="opcion" value="1">
              <input type="text" name="opcion_futura[]" id="opcion_futura" class="form-control pb-4 pt-4 border border-top-0 border-right-0 border-left-0 opcion_futura" placeholder="Escriba un ejemplo para respuesta de pregunta corta.">
            </fieldset> -->
            <small>doble click si no tendrá mensaje</small>
          </div>
        </div>
        
        <div class="row">
          <div class="col-6 col-md">
            <button class="btn btn-light rounded-circle float-right mr-2 agregar"><i class="fas fa-arrow-down"></i></button>

            <a class="btn btn-light rounded-circle float-right mr-2 remove" href="javascript: void(0)"><i class="far fa-trash-alt"></i></a>

            <!-- <button class="btn btn-light btn-block rounded-bottom">agregar</button> -->
          </div>
        </div>

        <div class="dropdown-divider dividir"></div>

      </form>
    </div>
  </div>
</div>

<a style="background-color: #e3eaef; border-color: #e3eaef;" class="dropdown-item mb-5 text-center" href="{{url('/form')}}"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>Salir</a>

<!-- <a style="background-color: #e3eaef; border-color: #e3eaef;" class="dropdown-item mb-5 text-center" href="{{url('/form')}}"  onclick="event.preventDefault(); document.getElementById('formPreguntas').submit();"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>Salir</a> -->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css"/>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

<script>
  // $("#spinner").attr({'min':'0', 'max':'15'}).css({'width':'15rem', 'height':'15rem'}).hide();
   // $(".loader-page").fadeIn().css({'visibility':'visible', 'opacity':'initial'});
              // setTimeout(function() { $(".loader-page").fadeOut();}, 1200);

  // $(window).on('load', function() {
  //   $(".loader-page").css({'visibility':'visible', 'opacity':'initial'}).fadeIn().show();
  //   setTimeout(function() { $(".loader-page").fadeOut();}, 2000);
  // });

  // $.get(data, function (response) { });

  // var parametros = {
  //           "nombre" : $("#txtNombre").val(),
  //           "costo" : $("#txtCosto").val()
  //     }
  // $('#contenedorPreguntasDos').css({'content-visibility':'hidden'});

  // var arr = [option_futur];
  // arr.forEach(function(elemento) {
  //     console.log(elemento);
  // });

  // var miArray = [option_futur];
  //         for (var valor of miArray) {
  //           console.log(valor);
  //         }

  // var miArray = [option_futur];
  //         for (var i = 0; i < miArray.length; i+=1) {
  //           console.log(miArray[i]);
  //         }

  // let nombre = '';
  // console.log(event.target.name=="formulario");
  // console.log(e);

  // var miObjeto = {'data':option_futur};
  //   for (var propiedad in miObjeto) {
  //     if (miObjeto.hasOwnProperty(propiedad)) {
  //       console.log("propiedad '" + propiedad + "'valor: " + miObjeto[propiedad]);
  //     }
  //   }

  // window.onload = function(){
  // }

  // function obligatorioClick(elemento) {
  //   var obligatorio = $(".obligatorio").find("input[type=checkbox]").prop('checked');
  //   return obligatorio;
  // }

  // for (var i=0; i < idRad; i++){ }

  // console.log(e);
  // var valOpFu = $('#opcion_futura').val();
  // var valOpFuTo = $(this).attr('id');
  // console.log(valOpFu, valOpFuTo);

  // setInterval(function() { 
  //      $.each(field_selectors, function() { 
  //        var input = $(this);
  //        var old = input.attr("data-old-value");
  //        var current = input.val();
  //        if (old !== current) { 
  //          if (typeof old != 'undefined') { 
  //            ... your code ...
  //          }
  //          input.attr("data-old-value", current);
  //        }   
  //      }   
  // }, 500);

  // var valOpFu = $('#opcion_futura');
  //  // var testimonials= $('.testimonial'); 
  //  for (var i = 0; i < valOpFu.length; i+=1) { 
  //  // Using $() to re-wrap the element. 
  //   var DATA = valOpFu[i];
  //   console.log(DATA, valOpFuTo); 
  //   }

  // var log = $('input[id=opcion_futura]').val();
  // console.log(log);

  // var query = ""; 
  // // // // [type=text][name!=""]
  //   $('input[id=opcion_futura]').each(function (index, domEle) {
  //       // alert(index + ': ' + $(domEle).val());
  //       var query = $(domEle).val();
  //       // var query = $(domEle).attr('data-id');
  //       console.log(query, valOpFuTo);
  //   });

  // $(window).on('load', function(e){
        //   var option = $('.seleccionTipoPregunta').val();
        //   console.log(Opcion);
        // });


       // let pregunta, pregunta_obligatorio, token, formulario_id, opcion;
          // pregunta = $('#pregunta').val();
          // pregunta_obligatorio = $('.obligatorio').val();
          // token = $('.idToken').val();
          // formulario_id = $('.formulario_id').val();
          // option = $('.seleccionTipoPregunta').val();
          // console.log(pregunta, pregunta_obligatorio, token, formulario_id, opcion);
          // obligatorio = obligatorioClick(this);
          // option_futur = $('#opcion_futura').val();
          // option_futur = $('.tipoPregunta').find('input[name="opcion_futura"]');
          // console.log(option_futur);
          // if ($(this).data('id') == null) {token='0000000000000';} else {token = $(this).data('id');}

          // var miArray = $('#opcion_futura').val();
          // for (var i = 0; i < miArray.length; i++) {
          //   console.log(miArray[i]);
          // }

          // if ($(this).data('id') == $('.obligatorio').attr('id')) {
          //   obligatorio = $(this).find("input[type=checkbox]").prop('checked');
          // }

  $(window).on('load', function () {
    setTimeout(function () {
      $(".loader-page").css({'visibility':"hidden",'opacity':"0"})
    }, 100);
    var tokenIn = Math.round(Math.random() * (10000000000000 - 1) + 1);
    $('.contenedorPreguntasDos').attr('id', 'contenedorPreguntasDos'+tokenIn);
    $('.formPreguntas').attr('data-id', tokenIn);
    $('.idToken').attr('value', tokenIn);
    $('.tipoPregunta').attr('id', 'tipo_' + tokenIn);
    $('.seleccionTipoPregunta').attr('id', tokenIn);
    $('.obligatorio').attr('id', tokenIn);
    $('.opcion_futura').attr('data-id', tokenIn);
    $('.cont_opcion_futura').attr('id', 'text_' + tokenIn + '_1');
    $('a.remove').attr('data-id', 'contenedorPreguntasDos'+tokenIn);
    $('a.remove').attr('id', tokenIn);
    $('.agregar').attr('id', tokenIn);
    $('.dividir').hide();
  });

  

  $(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

      $(document).ready(function() {
        $('.agregar').on('click', function(e) {
          e.preventDefault();
          var id = $(this).attr('id');
          var elementos = null;
          for (var i = 0; i < 1; i++){
            var token = $.now();
            if (id != null) {
                elementos = $('#contenedorPreguntasDos'+id).clone(true);
            } else {
                elementos = $('#contenedorPreguntasDos'+id).clone(true);
            }
                        
            var preguntCount = 'pregunta sin titulo...';
            elementos.attr('id', 'contenedorPreguntasDos'+token);
            elementos.find('a.remove').attr('data-id', 'contenedorPreguntasDos'+token).attr('id', token);
            elementos.find('.pregunta').attr('placeholder', preguntCount);
            elementos.find('.seleccionTipoPregunta').attr('id', token);
            elementos.find('.tipoPregunta').attr('id','tipo_' + token).empty();
            elementos.find('.obligatorio').attr('id', token);
            elementos.find('.formPreguntas').attr('data-id', token).trigger("reset");
            elementos.find('.idToken').attr('value', token);
            elementos.find('.agregar').attr('id', token);
            // elementos.find('.dividir').show();
            elementos.appendTo('.contenedorPreguntas');
          }
          $('.dividir').show();
        });


        $('.remove').on('click', function(e) {
          var data_id = $(this).attr('data-id');
          var id = $(this).attr('id');
          // $(this).closest('.contenedorPreguntas').remove();
          $.ajax({
              url: "{{url('/form/delete/preg/din/')}}/"+id,
              type: 'DELETE',
              data: $(this).serialize(),
              dataType: "application/json",
           });
          $('#' + data_id).remove();
        });


        $('.seleccionTipoPregunta').on('blur change', function(e) {
          e.preventDefault();
          var opciones = $(this).val();
          var id = $(this).attr('id');
          var html = '';

            $.ajax({
              url: "{{url('/form/delete/preg/din/opc/all/')}}/"+id,
              type: 'DELETE',
              data: {'id':id},
              dataType: "application/json"
            });

            switch(opciones){
              case '1':
                $('#tipo_'+id).empty().html('<fieldset id="text_'+id+'_1" class="cont_opcion_futura">'+
                    '<input type="hidden" id="opcion" class="opcion" value="1">'+
                    '<input type="text"  class="form-control pb-4 pt-4 border border-top-0 border-right-0 border-left-0" data-id="'+id+'" name="opcion_futura[]" id="opcion_futura" placeholder="Escribe un ejemplo para respuesta de pregunta corta">'+
                    '</fieldset><small>doble click si no tendra mensaje</small>');
              break;

              case '2':
                $('#tipo_'+id).show().empty().html('<fieldset id="textarea_'+id+'_1" class="cont_opcion_futura">'+
                  '<input type="hidden" id="opcion" class="opcion" value="2">'+
                  '<textarea class="form-control textarea" data-id="'+id+'" name="opcion_futura[]"  id="opcion_futura" rows="2"  placeholder="Escribe un ejemplo para respuesta de Texto largo"></textarea>'+
                  '</fieldset><small>doble click si no tendra mensaje</small>');          
              break;

              case '3':
                $('#tipo_'+id).show().empty().html('<fieldset id="radio_'+id+'_1" class="cont_opcion_futura">' +
                      '<input type="radio" class="control-input" disabled>' +
                      '<input type="hidden" id="opcion" class="opcion" value="3">'+
                      '<input type="text" class="form-control-sm border border-bottom-0  border-top-0 border-right-0 border-left-0 font-weight-bold radio" data-id="'+id+'" name="opcion_futura[]"  id="opcion_futura" placeholder="respuesta de pregunta" >' +
                      '<a href="javascript: void(0)" class="text-dark removeRadio" id="'+id+'_1"><i class="fas fa-times"></i></a>'+
                      '</fieldset>');
              break;

              case '4':
                $('#tipo_'+id).show().empty().html('<fieldset id="checkbox_'+id+'_1" class="cont_opcion_futura">'+
                      '<input type="checkbox" class="control-input" disabled>'+
                      '<input type="hidden" id="opcion" class="opcion" value="4">'+
                      '<input type="text" class="form-control-sm border border-bottom-0 border-top-0 border-right-0 border-left-0 font-weight-bold checkbox" data-id="'+id+'" name="opcion_futura[]"  id="opcion_futura" placeholder="respuesta de pregunta" >'+
                      '<a href="javascript: void(0)" class="text-dark removeCheck" id="'+id+'_1"><i class="fas fa-times"></i></a>'+
                      '</fieldset>');
              break;

              case '5':
                $('#tipo_'+id).show().empty().html('<fieldset class="input-group" id="select_'+id+'_1" class="cont_opcion_futura">' +
                        '<input type="hidden" id="opcion" class="opcion" value="5">'+
                        '<input type="text" class="form-control form-control-sm border border-top-0 border-right-0 border-left-0 font-weight-bold select" data-id="'+id+'" name="opcion_futura[]"  id="opcion_futura" placeholder="Lista desplegable opción de respuesta">' +
                      '<div class="input-group-append">' +
                        '<a href="javascript: void(0)" class="text-dark removeSelect" id="'+id+'_1"><i class="fas fa-times"></i></a>' +
                      '</div>' +
                      '</fieldset>');
              break;

              case '6':
                $('#tipo_'+id).show().empty().html('<fieldset id="date_'+id+'_1" class="cont_opcion_futura">'+
                  '<input type="hidden" id="opcion" class="opcion" value="6">'+
                  '<input type="datetime-local" class="form-contro pb-2 pt-2 pl-5 pr-5" data-id="'+id+'" name="opcion_futura[]"  id="opcion_futura" placeholder="respuesta de pregunta">'+
                  '</fieldset><small>doble click si no tendra mensaje</small>');
              break;
            }
        });
        

        $('.tipoPregunta').on("click", ".radio", function(e) {
          e.preventDefault();
          let id, value, idRad, radio;
          id = $(this).attr('data-id');
          value = $(this).val();
          idRad = $('.radio').length + 1;
          if (value.length == 0 || value.trim() == '') {
            radio ='<fieldset id="radio_'+id+'_'+idRad+'" class="cont_opcion_futura">' +
                   '<input type="radio" class="control-input" disabled>' +
                   '<input type="hidden" id="opcion" class="opcion" value="3">' +
                   '<input type="text" data-id="'+id+'" name="opcion_futura[]"  id="opcion_futura" class="form-control-sm border border-bottom-0 border-top-0 border-right-0 border-left-0 font-weight-bold radio" placeholder="respuesta de pregunta">' +
                   '<a href="javascript: void(0)" class="text-dark removeRadio" data-id="'+id+'" id="'+id+'_'+idRad+'"><i class="fas fa-times"></i></a>' +
                   '</fieldset>';
          } else {
            return false; 
          }         
          $('#tipo_'+id).append(radio);
        });


        $('.tipoPregunta').on("click", ".checkbox", function(e) {
          e.preventDefault();
            let id, value, idChec, checkbox;
            id = $(this).attr('data-id');
            value = $(this).val();
            idChec = $('.checkbox').length + 1;
            if (value.length == 0 || value.trim() == '') {
              checkbox ='<fieldset id="checkbox_'+id+'_'+idChec+'" class="cont_opcion_futura">'+
                        '<input type="checkbox" class="control-input" disabled>'+
                        '<input type="hidden" id="opcion" class="opcion" value="4">' +
                        '<input type="text" data-id="'+id+'" name="opcion_futura[]"  id="opcion_futura" class="form-control-sm border border-bottom-0 border-top-0 border-right-0 border-left-0 font-weight-bold checkbox" placeholder="respuesta de pregunta" >'+
                        '<a href="javascript: void(0)" class="text-dark removeCheck" data-id="'+id+'" id="'+id+'_'+idChec+'"><i class="fas fa-times"></i></a>'+
                        '</fieldset>';
            } else {
              return false;
            }
          $('#tipo_'+id).append(checkbox);
        });


        $('.tipoPregunta').on("click", ".select", function(e) {
          e.preventDefault();
          let id, value, idSel, select;
          id = $(this).attr('data-id');
          value = $(this).val();
          idSel = $('.select').length + 1;
          if (value.length == 0 || value.trim() == '') {
              select = '<fieldset id="select_'+id+'_'+idSel+'" class="input-group cont_opcion_futura">' +
                          '<input type="hidden" id="opcion" class="opcion" value="5">' +
                          '<input type="text" data-id="'+id+'" name="opcion_futura[]"  id="opcion_futura" class="form-control form-control-sm border border-top-0 border-right-0 border-left-0 font-weight-bold select" placeholder="Lista desplegable opción de respuesta">' +
                        '<div class="input-group-append">' +
                          '<a href="javascript: void(0)" class="text-dark removeSelect" data-id="'+id+'" id="'+id+'_'+idSel+'"><i class="fas fa-times"></i></a>' +
                        '</div>' +
                        '</fieldset>';
          } else {
            return false;
          }
          $('#tipo_'+id).append(select);
        });
      });
    
      
      $('.tipoPregunta').on("click", ".removeRadio", function(e) {
        e.preventDefault();
        var id = $(this).attr('id');
        var data_id =$(this).attr('data-id');
        $.ajax({
            url: "{{url('/form/delete/preg/din/opc/')}}/"+data_id,
            type: 'DELETE',
            data: {'id':data_id},
            dataType: "application/json"
        });
        $('#radio_'+id).remove();
        // $('#'+id).parent('fieldset').remove();
      });

      $('.tipoPregunta').on("click", ".removeCheck", function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        var data_id =$(this).attr('data-id');
        $.ajax({
            url: "{{url('/form/delete/preg/din/opc/')}}/"+data_id,
            type: 'DELETE',
            data: {'id':data_id},
            dataType: "application/json"
        });
        $('#checkbox_'+id).remove();
      });

      $('.tipoPregunta').on("click", ".removeSelect", function(e){
        e.preventDefault();
        var id = $(this).attr('id');
        var data_id =$(this).attr('data-id');
        $.ajax({
            url: "{{url('/form/delete/preg/din/opc/')}}/"+data_id,
            type: 'DELETE',
            data: {'id':data_id},
            dataType: "application/json"
        });
        $('#select_'+id).remove();
      });
    


      $(document).ready(function() {
        $('#formDatos').on('keyup', function(e){
          e.preventDefault();
          var id = $('#idForm').val();
           $.ajax({
              url: "{{url('/form/update/preg/din/')}}/"+id,
              type: $(this).attr('method'),
              data: $(this).serialize(),
              dataType: "application/json"
           });
        });
       

        $('#formPreguntas').off().change(function(e){
          e.preventDefault();
           $.ajax({
              url: "{{Route('create.preg')}}",
              type: $(this).attr('method'),
              // data: {'pregunta':pregunta, 'pregunta_obligatorio':pregunta_obligatorio, 'token':token,'formulario_id':formulario_id},
              data: $(this).serialize(),
              dataType: "application/json"
           });
        });


        $('.tipoPregunta').on('click','.cont_opcion_futura', function(e){
          e.preventDefault();

          let field, fieldset, token, opcion_futura_token, opcion_futura, opcion, keys;
          field = $('input[id=opcion_futura]');
          fieldset = $('input[id=opcion]');

          $.each(field, function(key, value) {
            $.each(fieldset, function(nu, data) {
              opcion = $(data).val();
            });

            token = $(this).attr("data-id");
            keys = key + 1;
            opcion_futura_token = token +'_'+keys;
            opcion_futura = $(value).val();

            $.ajax({
                url: "{{Route('create.preg.opc')}}",
                type: 'POST',
                data: {'token':token,'opcion_futura':opcion_futura,'opcion_futura_token':opcion_futura_token,'opcion':opcion},
                dataType: "application/json"
            });
          });
        });

      });
});

  

</script>

@endsection