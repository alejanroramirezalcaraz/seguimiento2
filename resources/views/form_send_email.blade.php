<!--
PROPÓSITO DE ESTA SECCIÓN: formualrios 
NOMBRE DEL DESARROLLADOR: Jesus chirinos
FECHA:18-10-2020

MODIFICADO POR:

-->

@extends('template')

@section('seccion')

<div class="shadow mb-5 bg-white rounded">
  <div class="card-header d-flex flex-row align-items-center justify-content-between">
    <div class="container">
      <h3 class="m-0 font-weight-bold text-dark">Enviar formulario</h3>
      <div class="dropdown-divider"></div>
      <small>Selecciona el programa eduactivo y la escribe el período al que requieres enviar el formulario</small>
      <div class="dropdown-divider"></div>
      <div class="d-flex flex-row align-items-center justify-content-between"><h5 class="text float-left">Buscar ahora</h5></div>
       <form>
        <div class="form-row">
          <div class="form-group col-sm-6">
            <label>Selecciona programa educativo</label>
            <select class="form-control form-control-lg" id="programa_nombre" name="programa_nombre">
              <option value="" hidden="" selected="">Seleccionar programa educativo...</option>
              @foreach($proEdu as $pe)
                <option value="{{$pe->programa_nombre}}">{{$pe->programa_descripcion}}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group col-sm-6">
            <label>Buscar por año</label>
            <input type="text" name="generacion" id="generacion" class="form-control">
            <!-- <select class="form-control form-control-lg" id="generacion" name="generacion">
              <option value="" hidden="" selected="">Seleccionar Generación...</option>
              @foreach ($gen as $g)
              <option value="{{$g->generacion}}">{{$g->generacion}}</option>
              @endforeach
            </select> -->
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-mb-8 col-lg-7">
    <div class="card mb-4">
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-dark">Datos Generales</h6>
        <div class="dropdown no-arrow">
          <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
            aria-labelledby="dropdownMenuLink">
            <div class="dropdown-header">Herramientas</div>
            <a class="dropdown-item" href="#">alumno</a>
            <!-- <a class="dropdown-item" href="#">Action</a> -->
          </div>
        </div>
      </div>
      <div class="card-body">
         <form id="formDataEmail" action="" method="">

          <!-- {{ csrf_field() }} -->
          <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

          <div class="form-group">
            <label for="email">Correo Electrónico del administrador</label>
            @auth                  
            <input type="email" class="form-control" disabled="" id="emailAdmin"
              value="{{auth()->user()->email}}" name="email_admin">
            @endauth
          </div>
          
          <div class="form-group">
            <div class="d-flex bd-highlight justify-content-between">
              <div class="bd-highlight"><label for="">Alumnos encontrados</label></div>
              <div class="bd-highlight"><a class="text-primary" id="selectAll" style="font-size: 10px;">Seleccionar todo</a></div>
            </div>
            <div class="row mx-md-n5">
              <div class="col px-md-5">
                <div class="border bg-light" id="alumnos" style="padding-top:2%;padding-bottom:2%;">
                </div>
                <span class="text-danger span" id="alumno_id_errors"></span>
              </div>
            </div>
            <div id="selectedCount"></div>
          </div>
        
      </div>
    </div>
  </div>

  <div class="col-mb-4 col-lg-5">
    <div class="card mb-4">
      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-dark">Selecciona Formulario a enviar</h6>
      </div>
      <div class="card-body">


        <div class="form-group" id="contForm">
          <label>Formulario</label>
          <select multiple class="form-control" id="formulario" name="formulario[]">
            @php $i=0; @endphp
            @foreach ($form as $f)
            @php $i++; @endphp
            <option value="{{$f->id}}"><b>{{$i}}.-</b> {{$f->formulario}}</option>
            @endforeach
          </select>
          <span class="text-danger span" id="formulario_errors"></span> 
        </div>

        <div class="form-group" id="contFec">
          <label>Fecha de Publicación</label>
          <input type="datetime-local" class="form-control" id="fechaPublicacion" name="fecha_publicacion">
          <span class="text-danger span" id="fecha_publicacion_errors"></span> 
        </div>

        <input type="submit" class="btn btn-outline-success float-right" id="btnPub">
        </form>
      </div>
    </div>
  </div>
</div> 



<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css"/>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script type="text/javascript">

$('#contForm').hide();
$('#contFec').hide();
$('#btnPub').hide();

$("#selectAll").click(function (){
  if($(".alumno").length == $(".alumno:checked").length){ 
    $(".alumno").prop("checked", false);
    $("#selectAll").html('Seleccionar todo');
    $("#selectedCount").html("");
  } else {
    $(".alumno").prop("checked", true);
    $("#selectAll").html('Deshabilitar todo');

    var checkboxes = $("input[id=alumno][type=checkbox]:checked");
    var cont = 0;
    for (var x=0; x < checkboxes.length; x++) {
      if (checkboxes[x].checked) {
        cont = cont + 1;
      }
    }
    $("#selectedCount").html("<label>seleccionado un total de " + cont + " registros</label>");
  }
});

$(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  }); 


    // $("#programa_nombre").change(function(){
      $("#generacion").keyup(function(e){
      var programa = $('#programa_nombre').val();
      var generacion = $(this).val();

        $.ajax({
        url: "{{url('/form/search/alumno/')}}/"+programa+"/"+generacion,
        type: 'POST',
        dataType: 'json',
        data: {'programa':programa,'generacion':generacion},
        success: function(response){
          $('#contForm').show();
          $('#contFec').show();
          $('#btnPub').show();
          if (response.error == false) {
            var datos = "";
                $.each(response.data, function(i, value){
                  if (value.desc_situacion == 'Egresado') { 
                    var desc_situacion='<span class="badge badge-warning">'+value.desc_situacion+'</span>';
                  } else if (value.desc_situacion == 'Activo') {
                    var desc_situacion='<span class="badge badge-success">'+value.desc_situacion+'</span>';
                  } else {
                    var desc_situacion='<span class="badge badge-danger">'+value.desc_situacion+'</span>';
                  }
                  datos += '<label class="font-weight-lighter" style="margin: 1% 0% 0% 3%;"><input type="checkbox" class="alumno" id="alumno" name="alumno[]" value="'+value.mail+'" data-id="'+value.matricula+'" data-codigo="'+response.lsGeneracion+response.lsCarrera+'"> ';
                  datos += value.nombre+' '+value.apaterno+' '+value.amaterno+' | ';
                  datos += desc_situacion+'</label> ';
                  datos += '<span class="badge badge-primary">'+value.mail+'</span><br>';
                  
                });

              $('#alumnos').html(datos);
          }else if (response.error == true){
              $('#alumnos').html(response.data);
          }   
        }
        });
      });
    // });


      $(document).ready(function(){
        $("#formDataEmail").on('submit', function(e){
          e.preventDefault();
          let alId, alVal, alValData, formulario, fechaPublicacion, csrf_token;

          csrf_token = $('#csrf-token').val();
          formulario = $('#formulario').val();
          fechaPublicacion = $('#fechaPublicacion').val();
          alVal = $('input[id=alumno][type=checkbox]:checked');

          $.each(alVal, function(key, value) {
            alId = $(this).attr("data-id");
            alCo = $(this).data("codigo");
            alValData = $(value).val();

          $.ajax({
          url: "{{route('send.form.email')}}",
          type: 'POST',
          dataType: 'json',
          data:{alumno:alId,
                email:alValData,
                formulario:formulario,
                fecha_publicacion:fechaPublicacion,
                csrf_token:csrf_token,
                codigo:alCo},
              beforeSend: function(){
                $('#btnPub').fadeIn().val('enviando...');
                setTimeout(function(){ $('#btnPub').fadeIn().val('publicado'); }, 1000);
              },
              success: function(response){
                if (response.error == false) {
                  toastr.success(response.resp, "Formulario Envio", {
                      "closeButton": true,
                      "debug": true,
                      "newestOnTop": false,
                      "progressBar": true,
                      "positionClass": "toast-top-right",
                      "preventDuplicates": false,
                      "showDuration": "13000",
                      "hideDuration": "1000",
                  });
                  $('#btnPub').val('enviar');
                  $("#formDataEmail").trigger("reset");
                } else if (response.error == true){
                  toastr.error(response.resp, "Formulario Envio", {
                      "closeButton": true,
                      "debug": true,
                      "newestOnTop": false,
                      "progressBar": true,
                      "positionClass": "toast-top-right",
                      "preventDuplicates": false,
                      "showDuration": "13000",
                      "hideDuration": "1000",
                  });
                }   
              },
              error: function(response){
                if (response.status === 422) {
                  var errors = response.responseJSON.errors;
                  console.log(errors);
                  if (errors['formulario']) {
                    $('#formulario_errors').show().fadeIn().html(errors['formulario'][0]);
                    setTimeout(function(){ $('#formulario_errors').fadeOut(); }, 1500);
                  }
                  if (errors['alumno']) {
                    $('#alumno_id_errors').show().fadeIn().html(errors['alumno'][0]);
                    setTimeout(function(){ $('#alumno_id_errors').fadeOut(); }, 1500);
                  }
                  if (errors['fecha_publicacion']) {
                    $('#fecha_publicacion_errors').show().fadeIn().html(errors['fecha_publicacion'][0]);
                    setTimeout(function(){ $('#fecha_publicacion_errors').fadeOut(); }, 1500);
                  }
                }
              }
            });
          });
        });
      });


});
</script>

@endsection