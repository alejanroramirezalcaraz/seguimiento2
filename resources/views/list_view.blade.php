<!--
----------------------------------------------------------------------------------
PROPÓSITO DE ESTA SECCIÓN: vista donde se muestera  el data table de los programas educativos 
NOMBRE DEL DESARROLLADOR:alejandro ramirez
FECHA:4/10/2020
---------------------------------------------------------------------------------
-->
@extends('template')

@section('seccion')

<div class="shadow p-3 mb-5 bg-white rounded">
  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
    <span class="float-left">
      <h5 class="text-success">Programa educativo: {{$programa_educativo}}, año de egreso: {{$año_egreso}}</h5>
    </span>
    <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#staticBackdrop">Agregar<i class="fas fa-plus text-success"></i></button>  
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">        
          <table id="showDataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>

                <th>#</th>
                <th>Nombre</th>
                <th>Apellido paterno</th>
                <th>Apellido materno</th>
                <th>Género</th>
                <th>Matrícula</th>
                <th>Generación</th>
                <th>Programa</th>
                <th>Correo</th>
                <th>Télefono</th>
                <th>Año de egreso</th>
                
              </thead>
              <tbody>
                @php $i=0; @endphp
                @foreach ($alumnos as $alumno )
                @php $i++; @endphp

                @php $z=0; @endphp
                @foreach ($respuestas as $respuesta )
                  
                @if ($alumno->alumno_id == $respuesta->respuesta_alumno_id)
                  @php $z=1 @endphp
                @endif 
                @endforeach 

               @if ($z == 1)
                   <tr>
                  <td class="text-success">{{$i}} </td>
                  <td class="text-success">{{$alumno->alumno_nombre}} </td>
                  <td class="text-success">{{$alumno->alumno_ap_paterno}} </td>
                  <td class="text-success">{{$alumno->alumno_ap_materno}} </td>
                  <td class="text-success">{{$alumno->alumno_genero}} </td>
                  <td class="text-success">{{$alumno->alumno_matricula}} </td>
                  <td class="text-success">{{$alumno->alumno_generacion}} </td>
                  <td class="text-success">{{$alumno->alumno_programa_educativo}} </td>
                  <td class="text-success">{{$alumno->email}} </td>
                  <td class="text-success">{{$alumno->alumno_telefono}} </td>
                  <td class="text-success">{{$alumno->alumno_anio_egreso}} </td>
                </tr>
                @endif 
                  @if ($z == 0)
                   <tr>
                  <td class="text-warning">{{$i}} </td>
                  <td class="text-warning">{{$alumno->alumno_nombre}} </td>
                  <td class="text-warning">{{$alumno->alumno_ap_paterno}} </td>
                  <td class="text-warning">{{$alumno->alumno_ap_materno}} </td>
                  <td class="text-warning">{{$alumno->alumno_genero}} </td>
                  <td class="text-warning">{{$alumno->alumno_matricula}} </td>
                  <td class="text-warning">{{$alumno->alumno_generacion}} </td>
                  <td class="text-warning">{{$alumno->alumno_programa_educativo}} </td>
                  <td class="text-warning">{{$alumno->email}} </td>
                  <td class="text-warning">{{$alumno->alumno_telefono}} </td>
                  <td class="text-warning">{{$alumno->alumno_anio_egreso}} </td>
                </tr>
                @endif 



               
                @endforeach

              </tbody>        
            </table>                  
          </div>
        </div>
      </div>  
    </div>    

  </div>


  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css"/>
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

  <script>
    $(function(){
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $('#alerta').hide();
      $('.span').hide();

      var table = $('#showDataTable').DataTable({
        // dom: 'fB<"top"l>rt<"bottom"ip><"clear">',
        dom: '<"float-left"Bl><"float-right"f>t<"float-left"i><"float-right"p>',
        buttons: {
          dom: {
            button: {
                    className: 'btn btn-outline-secundary mr-1 mb-2' //Primary class for all buttons
                  }
                },
                buttons: [                  
                {
                  extend: 'excelHtml5', 
                  className: 'btn btn-outline-success',
                  title: 'reporte_en_excel',
                },
                {
                  extend: 'pdf',
                  className: 'btn btn-outline-danger',
                  title: 'reporte_en_pdf',
                },
                {
                  extend: 'copy',
                  className: 'btn btn-outline-info',
                    // text: '<i class="fas fa-print"></i> IMPRIMIR',
                    title: 'Copiado',
                  }
                  ]
                },

                "language": {
                  "sProcessing":     "Procesando...",
                  "sLengthMenu":     "Mostrar _MENU_ registros",
                  "sZeroRecords":    "No se encontraron resultados",
                  "sEmptyTable":     "Ningún dato disponible en esta tabla",
                  "sInfo":           "Mostrando del _START_ al _END_ con un total de _TOTAL_ registros",
                  "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
                  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                  "sInfoPostFix":    "",
                  "sSearch":         "Buscar:",
                  "sUrl":            "",
                  "sInfoThousands":  ",",
                  "sLoadingRecords": "Cargando...",
                  "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                  },
                  "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                  },
                  "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad",
                    "pdf": "<i class='fas fa-file'></i>",
                    "excel": "<i class='fas fa-file-excel'></i>"
                  }
                }
              });



    });
  </script>

  @endsection