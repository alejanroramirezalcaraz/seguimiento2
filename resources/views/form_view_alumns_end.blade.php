<!--
PROPÓSITO DE ESTA SECCIÓN: formualrios 
NOMBRE DEL DESARROLLADOR: Jesus chirinos
FECHA:18-10-2020

MODIFICADO POR:

-->
@extends('layouts.template')
@section('seccionalumns')

<!-- <div class="loader-page">
  <label class="d-flex justify-content-center text-loading">Cargando...</label>
</div> -->

<div class="shadow-sm mb-5 bg-white rounded">
  <div class="card-header d-flex flex-row align-items-center justify-content-between">
    <div class="container">
      @if ($error == 'false')
      @php $codSolucion = 'Tu encuesta se ha completado satisfactoriamente, gracias por tu tiempo.'; @endphp
      @php $titulo = 'Ha Finalizado:'; @endphp
      @else
      @php $codSolucion = 'lo sentimos la ocurrió algún error al enviar los datos. para asegurarnos de que tu encuesta ha sido realizada te pedimos que la hagas de nuevo.'; @endphp
      @php $titulo = 'Algo salió mal:'; @endphp
      @endif
      <h3 class="m-0 font-weight-bold text-dark"><?php echo$titulo;?></h3>
      <h4 class="m-0 font-weight-bold text-dark">{{$data}}</h4>
      <div class="row mt-3">
        <div class="col-sm-12">
          <div class="form-group">
            <h6 class="text-dark"><?php echo$codSolucion;?></h6>
            <div class="dropdown-divider"></div>
            <small id="fecha_local"></small>
          </div>
        </div>
      </div>
    </div>
  </div> 
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
  $(window).on('load', function () {
    setTimeout(function () {
      $(".loader-page").css({'visibility':"hidden",'opacity':"0"})
    }, 100);

    var date = new Date();

    var h = date.getHours(), m = date.getMinutes(), s = date.getSeconds();
    var time = (h > 12) ? (h-12 + ":" + m + ":" +s+ " p.m.") : (h + ":" + m + ":" +s+ " a.m.");

    var dia = new Array(7);
    dia[0]="Domingo";
    dia[1]="Lunes";
    dia[2]="Martes";
    dia[3]="Miercoles";
    dia[4]="Jueves";
    dia[5]="Viernes";
    dia[6]="Sabado";
    var dias = dia[date.getDay()];

    var numDia = date.getDate();

    var month = date.getMonth() + 1;
    var mes = (month < 10) ? '0' + month : month;
    var mes = new Array(12);
    mes[0]="Enero";
    mes[1]="Febrero";
    mes[2]="Marzo";
    mes[3]="Abril";
    mes[4]="Mayo";
    mes[5]="Junio";
    mes[6]="Julio";
    mes[7]="Agosto";
    mes[8]="Septiembre";
    mes[9]="Octubre";
    mes[10]="Noviembre";
    mes[11]="Diciembre";
    var meses = mes[date.getMonth()];

    $('#fecha_local').html(dias + " " + numDia + " de " + meses + " de " + date.getFullYear() + ". hora UTC: " + time);

  });
</script>

@endsection