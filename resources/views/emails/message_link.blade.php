<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
	<style>
        @media  only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media  only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>
<body style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; color: #74787E; height: 100%; hyphens: auto; line-height: 1.4; margin: 0; -moz-hyphens: auto; -ms-word-break: break-all; width: 100% !important; -webkit-hyphens: auto; -webkit-text-size-adjust: none; word-break: break-word;">
	
		

	<table class="wrapper" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
        <tr>
            <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                <table class="content" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
               		<tr>
				    	<td class="header" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 25px 0; text-align: center;">
				        	<a href="{{ url('/') }}" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #bbbfc3	; font-size: 19px; font-weight: bold; text-decoration: none; text-shadow: 0 1px 0 white;">UTSEM
				        	</a>
				    	</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<div style="margin: 5%; text-align: left;">
		<label>
			La Universidad Tecnológica del Sur del Estado de Morelos se preocupa por sus estudiantes, es por eso que te solicita contestar una encuesta para conocer sobre tu experiencia laboral y tus nuevos conocimientos.
		</label>
	</div>
	<small style="margin: 5%; text-transform: uppercase; text-align: justify;"><b>Para iniciar la encuesta da clic en el botón</b></small><br><br>

	<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
	    <tr>
	        <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
	            <table border="0" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
	                <tr>
	                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
	                    	@foreach($demo['formulario'] as $formulario)
	  						<a href="{{ url('/form/encuesta/')}}/{{$formulario}}/{{$demo['alumno']}}/{{$demo['csrf_token']}}/{{$demo['codigo']}}" class="button button-blue" target="_blank" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #FFF; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color:  #1F926F; border-top: 10px solid  #1F926F; border-right: 18px solid  #1F926F; border-bottom: 10px solid  #1F926F; border-left: 18px solid  #1F926F;">Encuesta</a>
	                        @endforeach
	                    </td>
	                </tr>
	            </table>
	        </td>
	    </tr>
	</table>

	<table class="subcopy" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-top: 1px solid #EDEFF2; margin: 1%; padding-top: 25px;">
	    <tr>
	        <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
	            <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; line-height: 1.5em; margin-top: 0; text-align: left; font-size: 12px;">Si tiene problemas con el botón, copia y pega la siguiente URL en la barra de tu navegador o da clic sobre ella para iniciar: 
	            @foreach($demo['formulario'] as $formulario)
	            <a href="{{ url('/form/encuesta/')}}/{{$formulario}}/{{$demo['alumno']}}/{{$demo['csrf_token']}}/{{$demo['codigo']}}" target="_blank" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #3869D4;">{{ url('/form/encuesta/')}}/{{$formulario}}/{{$demo['alumno']}}/{{$demo['csrf_token']}}/{{$demo['codigo']}}</a>
	            @endforeach
	        	</p>
	        </td>
	    </tr>
	</table>

</body>
</html>
