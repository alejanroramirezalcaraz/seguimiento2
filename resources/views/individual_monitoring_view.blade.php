<!--
----------------------------------------------------------------------------------
PROPÓSITO DE ESTA SECCIÓN: es la plantilla general del sistema
NOMBRE DEL DESARROLLADOR:alejandro ramirez
FECHA:2/10/2020
---------------------------------------------------------------------------------
-->
<!--
----------------------------------------------------------------------------------
PROPÓSITO DE ESTA SECCIÓN:vista  donde se miestra la lista de usuarios
NOMBRE DEL DESARROLLADOR:alejandro ramirez
FECHA:2/10/2020
---------------------------------------------------------------------------------
-->

@extends('template')



@section('seccion')

<div class="shadow p-3 mb-5 bg-white rounded">



  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      <span class="float-left">
       <h5 class="text-success">seguimiento individual</h5>
   </span>
   <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#staticBackdrop">Agregar<i class="fas fa-plus text-success"></i></button>  
</div>

<div class="container ">

<form>
  <div class="row">
  
  <div class="form-group col-md-4">
    <label for="exampleFormControlSelect1">programa educativo</label>
    <select class="form-control" id="f1">
      <option></option>
      <option codigo="03" value="03">agricultura sustentable</option>
      
      <option >ingeniería en tecnologias de la información</option>
      <option>etc</option>
      <option>etc</option>
    </select>
  </div>

   <div class="form-group col-md-4">
    <label for="exampleFormControlSelect1">Generación</label>
    <select class="form-control" id="f2">
      <option codigo="03" value="4">2019</option>
      <option codigo="03" value="5">2018</option>
    
      
    </select>
  </div>




<div class="form-group col-md-4">
    <label for="exampleFormControlSelect1">alumnos</label>
    <select class="form-control" id="f3" onchange="mostrar();" >
      <option value="0" ></option>
      <option value="1" >KAROL JULIANA  ALVARADO LOPEZ 20173ti028</option>
      <option value="0">KAROL JULIANA  ALVARADO LOPEZ 20173ti028</option>
      <option value="0">KAROL JULIANA  ALVARADO LOPEZ 20173ti028</option>
      <option value="0">KAROL JULIANA  ALVARADO LOPEZ 20173ti028</option>
      <option value="0">KAROL JULIANA  ALVARADO LOPEZ 20173ti028</option>
      <option value="0">KAROL JULIANA  ALVARADO LOPEZ 20173ti028</option>
    
    
      
    </select>
  </div>


</div>

</form>

<script type="text/javascript" src="{{ asset('https://www.gstatic.com/charts/loader.js') }}"></script>
<script type="text/javascript">
  google.charts.load("current", {packages:['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ["Element", "Density", { role: "style" } ],
      ["2016 operador", 3000, "#b87333"],
      ["2017 operador", 3000, "silver"],
      ["2018  supervisor", 3400, "gold"],
      ["2019 supervisor", 3900, "color: #e5e4e2"]
      ]);

    var view = new google.visualization.DataView(data);
    view.setColumns([0, 1,
     { calc: "stringify",
     sourceColumn: 1,
     type: "string",
     role: "annotation" },
     2]);

    var options = {
      title: "Grafica de salarios",
      width: 700,
      height: 300,
      bar: {groupWidth: "95%"},
      legend: { position: "none" },
    };
    var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
    chart.draw(view, options);
  }
</script>


<div id="flotante" style="display:none;">
   <div class="col-md-6">

    <div id="columnchart_values" style="width: 700px; height: 300px;"></div>
  </div>
 
</div>



   <script languague="javascript">
     
            
        

        function mostrar() {  
          var f3 = document.getElementById("f3").value;
           
           if ( f3 == 1) {
             div = document.getElementById('flotante');
            div.style.display = '';
           }else{

             div = document.getElementById('flotante');
            div.style.display = 'none';
           }
        }

       
</script>
    
    <script type="text/javascript">
        // Consigue el elemento provincias/poblaciones por su identificador id. Es un método del DOM de HTML
        var id1 = document.getElementById("f1");
        var id2 = document.getElementById("f2");
         
        
        // Añade un evento change al elemento id1, asociado a la función cambiar()
        if (id1.addEventListener) {     // Para la mayoría de los navegadores, excepto IE 8 y anteriores
            id1.addEventListener("change", cambiar);
        } else if (id1.attachEvent) {   // Para IE 8 y anteriores
            id1.attachEvent("change", cambiar); // attachEvent() es el método equivalente a addEventListener()
        }
      

        // Definición de la función cambiar()
        function cambiar() {
            for (var i = 0; i < id2.options.length; i++)
            
            // -- Inicio del comentario -- 
            // Muestra solamente los id2 que sean iguales a los id1 seleccionados, según la propiedad display
            if(id2.options[i].getAttribute("codigo") == id1.options[id1.selectedIndex].getAttribute("codigo")){
                id2.options[i].style.display = "block";
            }else{
                id2.options[i].style.display = "none";
            }
            // -- Fin del comentario --
                    
            id2.value = "";
        }

        // Llamada a la función cambiar()
        cambiar();
    </script>



  
</div>    
</div>
@endsection