<!--
PROPÓSITO DE ESTA SECCIÓN: formualrios 
NOMBRE DEL DESARROLLADOR: Jesus chirinos
FECHA:18-10-2020

MODIFICADO POR:

-->
@extends('layouts.template')
@section('seccionalumns')

<div class="loader-page">
  <label class="d-flex justify-content-center text-loading">Cargando...</label>
</div>

<div class="container">
  <div class="row justify-content-md-center">
    <div class="col"></div>
    <div class="col-6">
      <div class="shadow-sm mb-3 bg-white rounded">
        <div class="card-header d-flex flex-row align-items-center justify-content-between">
          <div class="container">
            @foreach ($viewForm as $vF)
            <h3 class="m-0 font-weight-bold text-dark">Formulario:</h3>
            <h6 class="m-0 font-weight-bold text-dark">{{$vF->formulario}}</h6>
            <div class="dropdown-divider"></div>
            <div class="row mt-3">
              <div class="col-sm-12">
                <div class="form-group">
                  <small>{{$vF->formulario_descripcion}}</small>
                </div>
              </div>
            </div>
          </div>
        </div> 
      </div>


      <div class="shadow-sm mb-5 bg-white rounded">
        <div class="card-header d-flex flex-row align-items-center justify-content-between">
          <div class="container">
          
          <form id="formaResp" action="" method="POST">  
              
              {{ csrf_field() }}

              @php $i=0; @endphp
              @foreach ($viewFormPreg as $vFP)
              @php $i++; @endphp
              
              @if($vFP->pregunta_obligatorio == 'on')
              @php $ob=''; @endphp
              @else
              @php $ob='<label class="m-2 text-danger">*</label>'; @endphp
              @endif
              

            <!-- <form id="formOpc" method="POST">
              {{ csrf_field() }}   -->

            <fieldset>
              <input type="hidden" name="formulario" value="{{$vF->formulario}}">
              <input type="hidden" name="alumno_id" id="alumno" value="{{$aluID}}">
              <input type="hidden" name="formulario_id" id="formulario" value="{{$vF->id}}">
              <input type="hidden" name="pregunta_id{{$vFP->pregunta_token}}[]" id="pregunta" value="{{$vFP->id}}">
              
              <input type="hidden" name="pregunta_token[]" id="pregunta_token" value="{{$vFP->pregunta_token}}">

              <div class="row mt-3">
                <div class="form-group col-sm">
                  <label><h6 class="m-0 font-weight-bold text-dark">{{$i}}.- {{$vFP->pregunta}}<?php echo$ob;?></h6></label>
                  <div class="row">
                    <div class="col-sm">
                      <div class="control" id="opciones_{{$vFP->pregunta_token}}">

                      </div>       
                    </div>
                  </div>
                </div>
              </div>
              <div class="dropdown-divider"></div>
            </fieldset>

            <!-- </form> -->

              @endforeach
              @endforeach
              <input type="submit" class="btn btn-primary float-right mb-3 mt-3" id="add"> 
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="col"></div>
  </div>
</div>





<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css"/>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">


<script type="text/javascript">

$(window).on('load', function () {
  setTimeout(function () {
    $(".loader-page").css({'visibility':"hidden",'opacity':"0"})
  }, 100);

  
  var pregunta = $('input[id=pregunta]');
  $.each(pregunta, function(key, value) {          
    var pregID = $(value).val();
    $.getJSON("{{url('/form/encuesta/opcion/')}}/"+pregID, { "pregID" : pregID })
      .done(function(data, textStatus, jqXHR ) {
        if (data.error == false) {
        var datos = "";
        $.each(data.value, function(i, value){
          switch(value.opcion){
            case '1':
              if (value.pregunta_obligatorio == 'on') {
                datos += '<input type="hidden" name="opcion_token[]" value="'+value.opcion_token+'"> ';
                datos += '<input type="text"  class="form-control pb-4 pt-4 border border-top-0 border-right-0 border-left-0" data-id="'+value.id+'" name="opfu'+value.opcion_token+'[]" placeholder="'+value.opcion_futura+'">';
              }else{
                datos += '<span id="opcionesFut'+value.opcion_token+'" tabindex="0"></span>';
                datos += '<input type="hidden" name="opcion_token[]" value="'+value.opcion_token+'"> ';
                datos += '<input type="text"  class="form-control pb-4 pt-4 border border-top-0 border-right-0 border-left-0" data-id="'+value.opcion_token+'" name="opfu'+value.opcion_token+'[]" id="opfu_req" placeholder="'+value.opcion_futura+'" required>';
              }
              $('#opciones_'+value.opcion_token).html(datos);
            break;

            case '2':
              if (value.pregunta_obligatorio == 'on') {
                datos += '<input type="hidden" name="opcion_token[]" value="'+value.opcion_token+'"> ';
                datos += '<textarea class="form-control textarea" data-id="'+value.id+'" name="opfu'+value.opcion_token+'[]" rows="2" placeholder="'+value.opcion_futura+'"></textarea>';
              }else{
                datos += '<span id="opcionesFut'+value.opcion_token+'" tabindex="0"></span>';
                datos += '<input type="hidden" name="opcion_token[]" value="'+value.opcion_token+'"> ';
                datos += '<textarea class="form-control textarea" data-id="'+value.opcion_token+'" name="opfu'+value.opcion_token+'[]" id="opfu_req" rows="2" placeholder="'+value.opcion_futura+'" required></textarea>';
              }
              $('#opciones_'+value.opcion_token).html(datos);
                
            break;

            case '3':
              var datosRadio = '';
              if (value.pregunta_obligatorio == 'on') {
                datosRadio += '<input type="hidden" name="opcion_token[]" value="'+value.opcion_token+'">';
                $.each(data.value, function(i, valueRad){
                    datosRadio += '<label class="font-weight-lighter" style="margin: 0% 0% 0% 3%;"> ';
                    datosRadio += '<input type="radio" id="opcion_id" name="'+valueRad.opcion_token+'[]" data-id="'+valueRad.id+'" value="'+valueRad.id+'"> ';
                    datosRadio += valueRad.opcion_futura+'</label><br>';
                })
              }else{
                datosRadio += '<input type="hidden" name="opcion_token[]" value="'+value.opcion_token+'">';
                $.each(data.value, function(i, valueRad){
                    datosRadio += '<label class="font-weight-lighter" style="margin: 0% 0% 0% 3%;"> ';
                    datosRadio += '<input type="radio" id="radio_req'+value.opcion_token+'" name="'+valueRad.opcion_token+'[]" data-id="'+value.opcion_token+'" value="'+valueRad.id+'"> ';
                    datosRadio += '<span class="d-inline-block" id="radio'+value.opcion_token+'" tabindex="0">';
                    datosRadio += valueRad.opcion_futura+'</label></span><br>';
                })
              }
              $('#opciones_'+value.opcion_token).html(datosRadio);
            break;

            case '4':
              var datosChec = '';
              if (value.pregunta_obligatorio == 'on') {
                datosChec += '<input type="hidden" name="opcion_token[]" value="'+value.opcion_token+'">';
                $.each(data.value, function(i, valueChec){
                  datosChec += '<label class="font-weight-lighter" style="margin: 0% 0% 0% 3%;"> ';
                  datosChec += '<input type="checkbox" id="opcion_id" name="'+valueChec.opcion_token+'[]" data-id="'+valueChec.id+'" value="'+valueChec.id+'"> ';
                  datosChec += valueChec.opcion_futura+'</label><br>';
                })
              }else{
                datosChec += '<input type="hidden" name="opcion_token[]" value="'+value.opcion_token+'">';
                $.each(data.value, function(i, valueChec){  
                  datosChec += '<label class="font-weight-lighter" style="margin: 0% 0% 0% 3%;">';
                  datosChec += '<input type="checkbox" id="checkbox_req" name="'+valueChec.opcion_token+'[]" data-id="'+value.opcion_token+'" value="'+valueChec.id+'" > ';
                  datosChec += '<span class="d-inline-block" id="checkbox'+value.opcion_token+'" tabindex="0">';
                  datosChec += valueChec.opcion_futura+'</label></span><br>';
                })
              }
              $('#opciones_'+value.opcion_token).html(datosChec);
            break;

            case '5':
              var datosSel = '';
              if (value.pregunta_obligatorio == 'on') {
                datosSel += '<input type="hidden" name="opcion_token[]" value="'+value.opcion_token+'">';
                datosSel += '<select class="form-control form-control-lg" name="'+value.opcion_token+'[]">';
                datosSel += '<option value="0" hidden>Seleccionar respuesta...</option>';
                $.each(data.value, function(i, valueSel){
                datosSel += '<option data-id="'+valueSel.id+'" value="'+valueSel.id+'">'+valueSel.opcion_futura+'</option>';
                })
                datosSel += '</select>';
                datosSel += '<small>Selecciona una respuesta</small>';
              }else{
                datosSel += '<input type="hidden" name="opcion_token[]" value="'+value.opcion_token+'">';
                datosSel += '<span id="select'+value.opcion_token+'" tabindex="0">';
                datosSel += '<select class="form-control form-control-lg" id="select_req" data-id="'+value.opcion_token+'" name="'+value.opcion_token+'[]" >';
                datosSel += '<option value="0" hidden>Seleccionar respuesta...</option>';
                $.each(data.value, function(i, valueSel){
                datosSel += '<option data-id="'+valueSel.id+'" value="'+valueSel.id+'">'+valueSel.opcion_futura+'</option>';
                })
                datosSel += '</span></select>';
                datosSel += '<small>Selecciona una respuesta</small>';
              }
              $('#opciones_'+value.opcion_token).html(datosSel);
            break;

            case '6':
              if (value.pregunta_obligatorio == 'on') {
                datos += '<input type="hidden" name="opcion_token[]" value="'+value.opcion_token+'">';
                datos += '<input type="datetime-local" class="form-control" data-id="'+value.id+'" name="opfu'+value.opcion_token+'[]">';
              }else{
                datos += '<span id="opcionesFut'+value.opcion_token+'" tabindex="0"></span>';
                datos += '<input type="hidden" name="opcion_token[]" value="'+value.opcion_token+'">';
                datos += '<input type="datetime-local" class="form-control" data-id="'+value.opcion_token+'" name="opfu'+value.opcion_token+'[]" id="opfu_req" required>';
              }
              $('#opciones_'+value.opcion_token).html(datos);
            break;
          }
        })     
      }else if (data.error == true){
       console.log(data.value);
      }
      })
      .fail(function(jqXHR, textStatus, errorThrown ) {
          if ( console && console.log ) {
              console.log( "Algo ha fallado: " +  textStatus );
          }
    });
  });
});


$(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  }); 

      $(document).ready(function(){
        $("#formaResp").on("submit", function(e){
          e.preventDefault();
          
          // var dataCh = $('input[id=checkbox_req][type=checkbox]:checked');
          // var tokenCh = $('input[id=checkbox_req][type=checkbox]').data('id');
          // var al_menos_uno = false;
          // var hasError = false;
          var valid = true;
          // for (var i = 0; i < dataCh.length; i++) {
          //   if (dataCh[i].checked) {
          //     al_menos_uno = true;
          //   }
          // }

          // var opcFut = $('#opfu_req').val(); 
          //   if (opcFut.length == 0 || $('#opfu_req').text() == '' || opcFut.indexOf(" ") !== -1) {
          //     var data = $("#opfu_req").data('id');
          //     for(var x = 0; x <= opcFut.length; x++){
          //       $("#opfu_req").prop('required',true);
          //       $("#opcionesFut"+data).attr("title","Debe seleccionar una respuesta").attr("data-original-title", "Debe seleccionar una respuesta");
          //       $('#opcionesFut'+data).tooltip('show');
          //       $('#add').val('obligatorias *').removeClass('btn btn-primary').addClass('btn btn-outline-danger');
                
          //     } 
          //   }
            
            // if (!document.querySelector('input[id="radio_req"]:checked')) {
            //   var tokenRa = $('input:radio[id=radio_req]').data('id');
            //   $("#radio"+tokenRa).attr("title","Debe seleccionar una respuesta").attr("data-original-title", "Debe seleccionar una respuesta");
            //   $('#radio'+tokenRa).tooltip('show');
            //   $('#add').val('obligatorias ra *').removeClass('btn btn-primary').addClass('btn btn-outline-danger');
            //   return true;
            // } if ($("#select_req")[0].selectedIndex==0) {
            //   var data =$("#select_req").attr('data-id');
            //   $("#select_req").prop('required',true);
            //   $("#select"+data).attr("title","Debe seleccionar una respuesta").attr("data-original-title", "Debe seleccionar una respuesta");
            //   $('#select'+data).tooltip('show');
            //   $('#add').val('obligatorias se *').removeClass('btn btn-primary').addClass('btn btn-outline-danger');
            //   return true;
            // } if (!al_menos_uno){
            //   $("#checkbox"+tokenCh).attr("title","Debe seleccionar una casilla minimo").attr("data-original-title", "Debe seleccionar una casilla minimo");
            //   $('#checkbox'+tokenCh).tooltip('show');
            //   $('#add').val('obligatorias ce *').removeClass('btn btn-primary').addClass('btn btn-outline-danger');
            //   return true;
            // } else {
            //   hasError = false;
            //   al_menos_uno = false;
            //   $("#select_req").prop('required',false);
            // } 

            // $('#opfu_req').find('input,textarea,datetime-local').each((idx, token) => {
            // let $field = $('#opfu_req').data('id');
      
            // console.log($field, token);
            //   // Si el campo es un checkbox o un radio y no se selecciono una opcion
            //   if (($field.prop('type') == 'checkbox' || $field.prop('type') == 'radio') &&
            //       !$field.find('input[name="'+$field.prop('name')+'"]:checked').length) {
            //     valid = false;
            //   alert('aquie estoy');
            //   }
            //   // Si el campo NO es un checkbox ni un radio y esta vacio
            //   else if ($field.prop('type') != 'checkbox' && $field.prop('type') != 'radio' &&
            //     !$field.val()) {
            //     valid = false;
            //    alert('no estoy aqui');
            //   } else {
            //     $fieldCont.removeClass('error');
            //   }
            // });
         
          $.ajax({
          url: "{{route('create.encuesta')}}",
          type: 'POST',
          dataType: 'json',
          data: $(this).serialize(),
          beforeSend: function(){
            $('#add').fadeIn().val('enviando...').removeClass('btn btn-outline-danger').addClass('btn btn-primary');
            setTimeout(function(){ $('#add').fadeIn().val('enviado'); }, 1000);
          },
          success: function(response){
            if (response.error == false) { 
              document.location.href="{{url('/form/encuesta/view/end/')}}/"+false+'/'+response.resp;
            } else if (response.error == true){
              document.location.href="{{url('/form/encuesta/view/end/')}}/"+true+'/'+response.resp;
            }   
          },
          error: function(response){
            if (response.status === 422) {
              var errors = response.responseJSON.errors;
              console.log(errors);
              if (errors['formulario']) {
                $('#formulario_errors').show().fadeIn().html(errors['formulario'][0]);
                setTimeout(function(){ $('#formulario_errors').fadeOut(); }, 1500);
              }
              if (errors['alumno']) {
                $('#alumno_id_errors').show().fadeIn().html(errors['alumno'][0]);
                setTimeout(function(){ $('#alumno_id_errors').fadeOut(); }, 1500);
              }
              if (errors['fecha_publicacion']) {
                $('#fecha_publicacion_errors').show().fadeIn().html(errors['fecha_publicacion'][0]);
                setTimeout(function(){ $('#fecha_publicacion_errors').fadeOut(); }, 1500);
              }
            }
          }
        });
      });
    });

});


</script>

@endsection