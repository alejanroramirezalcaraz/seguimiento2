<!---- 
PROPÓSITO DE ESTA SECCIÓN: formalario creacion
NOMBRE DEL DESARROLLADOR: jesus chirinos
FECHA:10-11-2020

MODIFICADO POR:

---->

@extends('template')

@section('seccion')

<div class="shadow p-3 mb-5 bg-white rounded">
  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
    <h5 class="text-success float-left">Formularios</h5>
    <a type="button" href="javascript:void(0)" class="btn btn-outline-success" data-toggle="modal" data-target="#staticBackdrop" id="add">Agregar<i class="fas fa-plus text-success"></i></a>  
  </div>

  <div class="container ">
    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">        
          <table id="showDataTable" class="table table-striped table-bordered align-items-center table-flush table-hover align-self-center text-center" cellspacing="0" width="100%">
            <thead class="thead-light">
                <tr>
                  <th>#</th>
                  <th>Formulario</th> 
                  <th>Descripción</th>
                  <th>Estado</th>
                  <th>Fecha de entrega</th>
                  <th>Fecha de cierre</th>
                  <th>Editar</th>
                  <th>Eliminar</th>
               </tr>
            </thead>
            <tfoot>
              <tr>
                  <th>#</th>
                  <th>Formulario</th> 
                  <th>Descripciòn</th>
                  <th>Estatus</th>
                  <th>Fecha de entrega</th>
                  <th>Fecha de cierre</th>
                  <th>Editar</th>
                  <th>Eliminar</th>
               </tr>
            </tfoot>
            <tbody>        
            </tbody>        
          </table>                  
        </div>
      </div>
    </div>  
  </div>    
</div>



<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabeTitulModal">Registro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">

          <div class="row justify-content-center">
            <div class="col-sm-11">
              <div class="alert alert-danger" id="alerta"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              </div>
            </div>
          </div>

           <form class="form-group" action="#" id="formControl" method="POST" enctype="multipart/form-data" >

           {{ csrf_field() }}

            <input type="hidden" name="id" id="id">

            <div class="form-group">
              <label>Formulario *</label>
              <input required="" type="text" class="form-control" name="formulario" id="formulario">
              <span class="text-danger span" id="formulario_errors"></span>      
            </div>

            <div class="form-group">
              <label>Descripción *</label>
              <textarea type="text" class="form-control" name="formulario_descripcion" id="formulario_descripcion" rows="2"></textarea>
              <span class="text-danger span" id="formulario_descripcion_errors" >El campo es obligatorio</span>
            </div>
          
            <div class="form-group">
              <label>Estado</label>
              <select class="form-control" name="formulario_estatus" id="formulario_estatus">
                <option value="1">Activo</option>
                <option value="0">Inactivo</option>
              </select>
            </div>

          <div class="form-row">
            <div class="form-group col-sm-6">
              <label>Fecha de inicio *</label>
              <input type="datetime-local" class="form-control" name="formulario_fecha_publicacion_inicio" id="formulario_fecha_publicacion_inicio">
              <span class="text-danger span" id="formulario_fecha_publicacion_inicio_errors" >El campo es obligatorio</span>
            </div>

            <div class="form-group col-sm-6">
              <label>Fecha de cierre *</label>
              <input type="datetime-local" class="form-control" name="formulario_fecha_publicacion_fin" id="formulario_fecha_publicacion_fin">
              <span class="text-danger span" id="formulario_fecha_publicacion_fin_errors" >El campo es obligatorio</span>
            </div>
          </div> 

          <button type="submit" class="btn btn-outline-success float-right mb-3 mt-3" id="btn">Cargando</button>
        </form>
      </div>
    </div>
  </div>
</div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css"/>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

<script>
$(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $('#alerta').hide();
    $('.span').hide();

    var table = $('#showDataTable').DataTable({
        // dom: 'fB<"top"l>rt<"bottom"ip><"clear">',
        dom: '<"float-left"Bl><"float-right"f>t<"float-left"i><"float-right"p>',
        buttons: {
          dom: {
              button: {
                className: 'btn btn-outline-secundary mr-1 mb-2'
              }
          },
          buttons: [                  
              {
                extend: 'excelHtml5', 
                className: 'btn btn-outline-success',
                title: 'reporte_en_excel',
              },
              {
                extend: 'pdf',
                className: 'btn btn-outline-danger',
                title: 'reporte_en_pdf',
              },
              {
                extend: 'copy',
                className: 'btn btn-outline-info',
                title: 'Copiado',
              }
          ]
        },
        retrieve: true,
        deferRender: true,
        processing: true,
        serverSide: true,
        ajax: "{{ route('index.forms') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'formulario', name: 'formulario',
            'render': function(data, type, row) {
                return '<a href="'+row['id']+'">'+data+' <i class="fas fa-share"></i></a>';
            }},
            {data: 'formulario_descripcion', name: 'formulario_descripcion'},
            {data: 'formulario_estatus', width: '10', className: 'text-center',
            'render': function(data, type, row) {
                return (data == 1) ? '<span class="badge text-success">Activo</span>' 
                    : '<span class="badge text-danger"> Inactivo </span>';
            }},
            {data: 'formulario_fecha_publicacion_inicio', name: 'formulario_fecha_publicacion_inicio'},
            {data: 'formulario_fecha_publicacion_fin', name: 'formulario_fecha_publicacion_fin'},            
            {data: 'edit', name: 'edit'},
            {data: 'delete', name: 'delete', orderable: false, searchable: false},
        ],"language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando del _START_ al _END_ con un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          },
          "buttons": {
              "copy": "Copiar",
              "colvis": "Visibilidad",
              "pdf": "<i class='fas fa-file'></i>",
              "excel": "<i class='fas fa-file-excel'></i>"
          }
      }
    });


    $('#add').click(function () {
        $('#formControl').trigger("reset");
        $('#alerta').hide();
        $(".span").hide();
        $("#staticBackdropLabeTitulModal").show().html('Registrar formulario');
        $('#btn').show().html('Registrar');
        $("#formControl").attr("action","{{Route('created.forms')}}");
    });


   $('body').on('click', '#edit', function () {
      var id = $(this).data('id');
      $.get("{{url('/form/search/edit/')}}" +'/'+ id, function (data) {
          $("#staticBackdropLabeTitulModal").show().html('Actualizar formulario');
          $('#alerta').hide();
          $(".span").hide();
          $('#btn').show().html('Actualizar');
          $("#formControl").attr("action","{{url('/form/update/')}}" +'/'+ id);
          $('#id').val(data.id);
          $('#formulario').val(data.formulario);
          $('#formulario_descripcion').val(data.formulario_descripcion);
          $('#formulario_estatus').val(data.formulario_estatus);
          $('#formulario_fecha_publicacion_inicio').val(data.formulario_fecha_publicacion_inicio);
          $('#formulario_fecha_publicacion_fin').val(data.formulario_fecha_publicacion_fin);
      })
    });


    $(document).ready(function(){
      $('#formControl').on('submit', function(e){
        e.preventDefault();
        $.ajax({
          url: $(this).attr('action'),
          type: $(this).attr('method'),
          data: $(this).serialize(),
          dataType: 'json',
          beforeSend: function(){
            $('#btn').fadeIn().html('Enviando...');
            setTimeout(function(){ $('#btn').fadeIn() }, 1000);
          },
          success: function(response){
            // $('#formControl').trigger("reset");
            $('#staticBackdrop').modal('hide');
            $("#formControl")[0].reset();
            if (response.error == false) {
              toastr.success(response.alert, "Formularios", {
                  "closeButton": true,
                  "debug": true,
                  "newestOnTop": false,
                  "progressBar": true,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "showDuration": "13000",
                  "hideDuration": "1000",
              });
              table.draw();
            } else if (response.error == 'falseCreate'){
              window.location.href = "{{url('/form/preg/')}}/"+response.data[0]+"/"+response.data[1];
            } else if (response.error == true){
              toastr.error(response.alert, "Formularios", {
                  "closeButton": true,
                  "debug": true,
                  "newestOnTop": false,
                  "progressBar": true,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "showDuration": "13000",
                  "hideDuration": "1000",
              });
            }   
          },
          error: function(response){
            if (response.status === 422) {
              var saltos = '';
              var errors = response.responseJSON.errors;
              $.each(errors, function(key, val) {
                $('#alerta').show().html(saltos += val[0] + '<br>').fadeIn();
                setTimeout(function(){ $('#alerta').fadeOut(); }, 10000);
              });
              if (errors['formulario']) {
                $('#formulario_errors').show().html(errors['formulario'][0]);
              }
              if (errors['formalario_descripcion']) {
                $('#formulario_descripcion_errors').show().html(errors['formalario_descripcion'][0]);
              }
              if (errors['formulario_fecha_publicacion_inicio']) {
                $('#formulario_fecha_publicacion_inicio_errors').show().html(errors['formulario_fecha_publicacion_inicio'][0]);
              }
              if (errors['formulario_fecha_publicacion_fin']) {
                $('#formulario_fecha_publicacion_fin_errors').show().html(errors['formulario_fecha_publicacion_fin'][0]);
              }
            }
          }
        });
      });
    });


   $(document).on('click', '#delete', function () {
      var id = $(this).val();
      var formulario = $(this).data('id');
      swal({
        title: "¿Seguro que deseas continuar?",
        text: "Eliminar a "+formulario+", no podrás deshacer este paso!",
        icon: "warning",
        buttons: [true, "¡Aceptar!"],
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajax({
            method: 'DELETE',
            url: "{{url('/form/delete/')}}/"+id,
            dataType: 'json',
            data: {id:id},
            success: function (response) {
              if (response.error == false) {
                swal({
                  title : "Correcto!",
                  text  : response.alert,
                  icon  : "success",
                })
                table.draw();
              }else if (response.error == true){
                swal({
                  title : "Ocurrio un error dentro del servidor!",
                  text  : response.alert,
                  icon  : "error",
                });
              }
            }
          });
        } else {
          swal({title : "Cancelado",
            text  : "Tu registro está seguro..."});
        }
      });
    });


  });
</script>

@endsection