<!--
PROPÓSITO DE ESTA SECCIÓN: formualrios 
NOMBRE DEL DESARROLLADOR: Jesus chirinos
FECHA:18-10-2020

MODIFICADO POR:

-->
@extends('layouts.template')
@section('seccionalumns')


<div class="loader-page">
  <label class="d-flex justify-content-center text-loading">Cargando...</label>
</div>


<div class="shadow mb-5 bg-white rounded">
  <div class="card-header d-flex flex-row align-items-center justify-content-between">
    <div class="container">
      @foreach ($viewForm as $vF)
      <h3 class="m-0 font-weight-bold text-dark">Formulario:</h3>
      <h6 class="m-0 font-weight-bold text-dark">{{$vF->formulario}}</h6>
      <div class="dropdown-divider"></div>
      <div class="row mt-3">
        <div class="col-sm-12">
          <div class="form-group">
            <small>{{$vF->formulario_descripcion}}</small>
          </div>
        </div>
      </div>
    </div>
  </div> 
</div>


<div class="shadow mb-5 bg-white rounded">
  <div class="card-header d-flex flex-row align-items-center justify-content-between">
    <div class="container">
    
    <form id="formaResp" action="" method="POST">  
        
        {{ csrf_field() }}

        @php $i=0; @endphp
        @foreach ($viewFormPreg as $vFP)
        @php $i++; @endphp
        
        @if($vFP->pregunta_obligatorio == 'on')
        @php $ob=''; @endphp
        @else
        @php $ob='<label class="m-2 text-danger">*</label>'; @endphp
        @endif
        <input type="hidden" name="alumno" id="alumno" value="{{$aluID}}">
        <input type="hidden" name="formulario" id="formulario" value="{{$vF->id}}">  
        <input type="hidden" name="pregunta[]" id="pregunta" value="{{$vFP->id}}">

        {{ csrf_field() }}        

        <div class="row">
          <div class="form-group col-sm-12">
            <label><h6 class="m-0 font-weight-bold text-dark">{{$i}}.- {{$vFP->pregunta}}<?php echo$ob;?></h6></label>
          </div>

        @foreach ($viewFormPregOpc as $vfpo)
          <div class="form-group col-sm-12">
            <!-- <div class="control" id="opciones_{{$vFP->pregunta_token}}"> -->                  
                @switch($vfpo->opcion)
                    @case(1)
                      @if ($vfpo->pregunta_obligatorio == 'on')
                        @php
                        $pre = '<input type="hidden" name="opcion" value="<?php $vfpo->opcion ?>">
                                <input type="text"  class="form-control pb-4 pt-4 border border-top-0 border-right-0 border-left-0" name="opcion_futura[]" id="opcion_futura" placeholder="">'; 
                        @endphp
                      @else
                        @php
                        $pre = '<input type="hidden" name="opcion" value="<?php $vfpo->opcion ?>">
                                <input type="text"  class="form-control pb-4 pt-4 border border-top-0 border-right-0 border-left-0" name="opcion_futura[]" id="opcion_futura" placeholder="" required="">'; 
                        @endphp
                      @endif
                    @break

                    @case(2)
                      @if ($vfpo->pregunta_obligatorio == 'on')
                        @php
                        $pre = '<input type="hidden" name="opcion" value="{{$vfpo->opcion}}">
                                <textarea class="form-control textarea" name="opcion_futura[]"  id="opcion_futura" rows="2"  placeholder="{{$vfpo->opcion_futura}}"></textarea>'; 
                        @endphp
                      @else
                        @php
                        $pre = '<input type="hidden" name="opcion" value="{{$vfpo->opcion}}">
                                <textarea class="form-control textarea" name="opcion_futura[]"  id="opcion_futura" rows="2"  placeholder="{{$vfpo->opcion_futura}}" required=""></textarea>'; 
                        @endphp
                      @endif
                    @break

                    @case(3)
                      @if ($vfpo->pregunta_obligatorio == 'on')
                        @php
                        $pre = '<input type="hidden" name="opcion" value="{{$vfpo->opcion}}">
                                <label class="font-weight-lighter" style="margin: 1% 0% 0% 3%;">
                                <input type="radio" id="opcion_futura" name="opcion_futura[]" value="{{$vfpo->opcion_futura}}">{{$vfpo->opcion_futura}}</label><br>'; 
                        @endphp
                      @else
                        @php
                        $pre = '<input type="hidden" name="opcion" value="{{$vfpo->opcion}}">
                                <label class="font-weight-lighter" style="margin: 1% 0% 0% 3%;">
                                <input type="radio" id="opcion_futura" name="opcion_futura[]" value="{{$vfpo->opcion_futura}}" required="">{{$vfpo->opcion_futura}}</label><br>';
                        @endphp
                      @endif  
                    @break

                    @case(4)
                      @if ($vfpo->pregunta_obligatorio == 'on')
                        @php
                        $pre = '<input type="hidden" name="opcion" value="{{$vfpo->opcion}}">
                                <label class="font-weight-lighter" style="margin: 1% 0% 0% 3%;">
                                <input type="checkbox" id="opcion_futura" name="opcion_futura[]" value="{{$vfpo->opcion_futura}}">{{$vfpo->opcion_futura}}</label><br>'; 
                        @endphp
                      @else
                        @php
                        $pre = '<input type="hidden" name="opcion" value="{{$vfpo->opcion}}">
                                <label class="font-weight-lighter" style="margin: 1% 0% 0% 3%;">
                                <input type="checkbox" id="opcion_futura" name="opcion_futura[]" value="{{$vfpo->opcion_futura}}">{{$vfpo->opcion_futura}}</label><br>'; 
                        @endphp
                      @endif
                    @break

                    @case(5)
                      @if ($vfpo->pregunta_obligatorio == 'on')
                        @php
                        $pre = '<input type="hidden" name="opcion" value="{{$vfpo->opcion}}">
                                <select class="form-control form-control-lg" id="opcion_futura" name="opcion_futura[]">
                                <option hidden="">Seleciona tu respuesta...</option>
                                <option value="{{$vfpo->opcion_futura}}">{{$vfpo->opcion_futura}}</option>
                                </select>
                                <small>Selecciona una respuesta</small>'; 
                        @endphp
                      @else
                        @php
                        $pre = '<input type="hidden" name="opcion" value="{{$vfpo->opcion}}">
                                <select class="form-control form-control-lg" id="opcion_futura" name="opcion_futura[]">
                                <option hidden="">Seleciona tu respuesta...</option>
                                <option value="{{$vfpo->opcion_futura}}">{{$vfpo->opcion_futura}}</option>
                                </select>
                                <small>Selecciona una respuesta</small>'; 
                        @endphp
                      @endif
                    @break

                    @case(6)
                      @if ($vfpo->pregunta_obligatorio == 'on')
                        @php
                        $pre = '<input type="hidden" name="opcion" value="{{$vfpo->opcion}}">
                                <input type="datetime-local" class="form-control" name="opcion_futura[]" id="opcion_futura">'; 
                        @endphp
                      @else
                        @php
                        $pre = '<input type="hidden" name="opcion" value="{{$vfpo->opcion}}">
                                <input type="datetime-local" class="form-control" name="opcion_futura[]" id="opcion_futura">';  
                        @endphp
                      @endif
                    @break

                    @default
                      @php $pre = '<label>no hay datos</label>'; @endphp
                @endswitch
                @if ($vFP->pregunta_token == $vfpo->opcion_token)
                <?php echo $pre; ?>
                <!-- {{$vfpo->opcion}}{{$vfpo->opcion_futura}} -->
                @endif 

                
            </div>
          @endforeach
        </div>
        <div class="dropdown-divider"></div>

        @endforeach
        @endforeach
        <input type="submit" class="btn btn-primary float-right mb-3 mt-3" id="add"> 
      </form>
    </div>
  </div>
</div>



<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css"/>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script type="text/javascript">


$(window).on('load', function () {
  setTimeout(function () {
    $(".loader-page").css({'visibility':"hidden",'opacity':"0"})
  }, 100);

  
  var pregunta = $('input[id=pregunta]');
  $.each(pregunta, function(key, value) {          
    var pregID = $(value).val();
    $.getJSON("{{url('/form/encuesta/opcion/')}}/"+pregID, { "pregID" : pregID })
      .done(function(data, textStatus, jqXHR ) {
        if (data.error == false) {
        var datos = "";
        $.each(data.value, function(i, value){
          switch(value.opcion){
            case '1':
              if (value.pregunta_obligatorio == 'on') {
                datos += '<input type="text"  class="form-control pb-4 pt-4 border border-top-0 border-right-0 border-left-0" data-id="'+value.id+'" name="opcion[]" id="opcion" placeholder="'+value.opcion_futura+'">';
              }else{
                datos += '<input type="text"  class="form-control pb-4 pt-4 border border-top-0 border-right-0 border-left-0" data-id="'+value.id+'" name="opcion[]" id="opcion" placeholder="'+value.opcion_futura+'" required>';
              }
              $('#opciones_'+value.opcion_token).html(datos);
            break;

            case '2':
              if (value.pregunta_obligatorio == 'on') {
                datos += '<textarea class="form-control textarea" data-id="'+value.id+'" name="opcion[]"  id="opcion" rows="2"  placeholder="'+value.opcion_futura+'"></textarea>';
              }else{
                datos += '<textarea class="form-control textarea" data-id="'+value.id+'" name="opcion[]"  id="opcion" rows="2"  placeholder="'+value.opcion_futura+'" required></textarea>';
              }
              $('#opciones_'+value.opcion_token).html(datos);
                
            break;

            case '3':
              var datosRadio = '';
              if (value.pregunta_obligatorio == 'on') {
                $.each(data.value, function(i, valueOpc){
                    datosRadio += '<label class="font-weight-lighter" style="margin: 1% 0% 0% 3%;"><input type="radio" id="opcion" name="opcion[]" data-id="'+valueOpc.id+'" value="'+valueOpc.opcion_futura+'"> ';
                    datosRadio += valueOpc.opcion_futura+'</label><br>';
                })
              }else{
                $.each(data.value, function(i, valueOpc){
                    datosRadio += '<label class="font-weight-lighter" style="margin: 1% 0% 0% 3%;"><input type="radio" id="opcion" name="opcion[]" data-id="'+valueOpc.id+'" value="'+valueOpc.opcion_futura+'"> ';
                    datosRadio += valueOpc.opcion_futura+'</label><br>';
                })
              }
              $('#opciones_'+value.opcion_token).html(datosRadio);
            break;

            case '4':
              var datosChec = '';
              if (value.pregunta_obligatorio == 'on') {
                $.each(data.value, function(i, valueChec){
                  datosChec += '<label class="font-weight-lighter" style="margin: 1% 0% 0% 3%;"><input type="checkbox" id="opcion" name="opcion[]" data-id="'+valueChec.id+'" value="'+valueChec.opcion_futura+'"> ';
                  datosChec += valueChec.opcion_futura+'</label><br>';
                })
              }else{
                $.each(data.value, function(i, valueChec){
                  datosChec += '<label class="font-weight-lighter" style="margin: 1% 0% 0% 3%;"><input type="checkbox" id="opcion" name="opcion[]" data-id="'+valueChec.id+'" value="'+valueChec.opcion_futura+'"> ';
                  datosChec += valueChec.opcion_futura+'</label><br>';
                })
              }
              $('#opciones_'+value.opcion_token).html(datosChec);
            break;

            case '5':
              var datosSel = '';
              if (value.pregunta_obligatorio == 'on') {  
                datosSel += '<select class="form-control form-control-lg" id="opcion" name="opcion[]">';
                datosSel += '<option hidden="">Seleciona tu respuesta...</option>';
                $.each(data.value, function(i, valueSel){
                datosSel += '<option data-id="'+valueSel.id+'" value="'+valueSel.opcion_futura+'">'+valueSel.opcion_futura+'</option>';
                })
                datosSel += '</select>';
                datosSel += '<small>Selecciona una respuesta</small>';
              }else{
                datosSel += '<select class="form-control form-control-lg" id="opcion" name="opcion[]">';
                $.each(data.value, function(i, valueSel){
                datosSel += '<option data-id="'+valueSel.id+'" value="'+valueSel.opcion_futura+'">'+valueSel.opcion_futura+'</option>';
                })
                datosSel += '</select>';
                datosSel += '<small>Selecciona una respuesta</small>';
              }
              $('#opciones_'+value.opcion_token).html(datosSel);
            break;

            case '6':
              if (value.pregunta_obligatorio == 'on') {
                datos += '<input type="datetime-local" class="form-control" data-id="'+value.id+'" name="opcion[]" id="opcion">';
              }else{
                datos += '<input type="datetime-local" class="form-control" data-id="'+value.id+'" name="opcion[]" id="opcion" required>';
              }
              $('#opciones_'+value.opcion_token).html(datos);
            break;
          }
        })     
      }else if (data.error == true){
       console.log(data.value);
      }
      })
      .fail(function(jqXHR, textStatus, errorThrown ) {
          if ( console && console.log ) {
              console.log( "Algo ha fallado: " +  textStatus );
          }
    });
  });
});


$(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  }); 

      $(document).ready(function(){
        $("#formaResp").on("submit", function(e){
          e.preventDefault();
      //     console.log(e);
          // let opcion, formulario, pregunta, alumno, valOpc, idOp;

          // formulario = $('#formulario').val();
          // alumno = $('#alumno').val();
          

          // console.log(pregunta, formulario);
          // opcion = $('input[id=opcion]');
          // opcionch = $('input[id=opcionch][type=checkbox]:checked');
          // opcionra = $('input:radio[id=opcionra]:checked');
          // idOp = $('select[id=opcionse] option:selected').attr('data-id');
          // valOpc = $('select[id=opcionse] option:selected').val();
          // $.each(opcionse, function(key, valueOpc) {
          //     idOp = $(this).attr('data-id');
          //     valOpc = $(valueOpc).val();
          //   });
          // console.log(idOp,valOpc);
          

          // $.each($('input[id=pregunta]'), function(key, valuePreg) {
          //   pregunta = $(valuePreg).val();
          //    if ($('input[id=opcion]').length > 0) {

          //       $.each($('input[id=opcion]'), function(key, valueOpc) {
          //         idOp = $(this).attr('data-id');
          //         valOpc = $(valueOpc).val();
          //         console.log(idOp, valOpc);
          //       })
          //     } if ($('input[id=opcionch][type=checkbox]:checked').length > 0) {
          //       $.each($('input[id=opcionch][type=checkbox]:checked'), function(key, valueOpc) {
          //         idOp = $(this).attr('data-id');
          //         valOpc = $(valueOpc).val();
          //         console.log(idOp, valOpc);
          //       })
          //     } if ($('input:radio[id=opcionra]:checked').length > 0){
          //       $.each($('input:radio[id=opcionra]:checked'), function(key, valueOpc) {
          //         idOp = $(this).attr('data-id');
          //         valOpc = $(valueOpc).val();
          //         console.log(idOp, valOpc);
          //       })
          //     } if ($('select[id=opcionse] option:selected').length > 0){
          //       $.each($('select[id=opcionse] option:selected'), function(key, valueOpc) {
          //         idOp = $(this).attr('data-id');
          //         valOpc = $(valueOpc).val();
          //         console.log(idOp, valOpc);
          //       })
          //     }
          //   console.log(pregunta, formulario, alumno);
          //   console.log(idOp, valOpc);
          // });
       
          alert('The two forms can not have the same name !!');
          $.ajax({
          url: "{{route('create.encuesta')}}",
          type: 'POST',
          dataType: 'json',
          data: $(this).serialize(),

          // data:{alumno:alId,
          //       email:alValData,
          //       formulario:formulario,
          //       fecha_publicacion:fechaPublicacion,
          //       csrf_token:csrf_token},
          //     beforeSend: function(){
          //       $('#btnPub').fadeIn().val('enviando...');
          //       setTimeout(function(){ $('#btnPub').fadeIn().val('publicado'); }, 1000);
          //     },
          //     success: function(response){
          //       if (response.error == false) {
          //         
          //         // $("#formDataEmail").trigger("reset");
          //       } else if (response.error == true){
          //         
          //       }   
          //     },
          //     error: function(response){
          //       if (response.status === 422) {
          //         var errors = response.responseJSON.errors;
          //         console.log(errors);
          //         if (errors['formulario']) {
          //           $('#formulario_errors').show().fadeIn().html(errors['formulario'][0]);
          //           setTimeout(function(){ $('#formulario_errors').fadeOut(); }, 1500);
          //         }
          //         if (errors['alumno']) {
          //           $('#alumno_id_errors').show().fadeIn().html(errors['alumno'][0]);
          //           setTimeout(function(){ $('#alumno_id_errors').fadeOut(); }, 1500);
          //         }
          //         if (errors['fecha_publicacion']) {
          //           $('#fecha_publicacion_errors').show().fadeIn().html(errors['fecha_publicacion'][0]);
          //           setTimeout(function(){ $('#fecha_publicacion_errors').fadeOut(); }, 1500);
          //         }
          //       }
          //     }
          //   });
          });
        
        });
      });


});


</script>

@endsection