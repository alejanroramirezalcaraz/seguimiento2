@extends('layouts.app')

@section('seccion')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a class="dropdown-item" href="/{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout').submit();"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
              Salir
          </a>


          <form id="logout" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
          </form>

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
