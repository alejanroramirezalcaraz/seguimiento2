<!--
----------------------------------------------------------------------------------
PROPÓSITO DE ESTA SECCIÓN: el objetivo de esta vista es mostrar las  graficas que comparan de manera general a los alumnos egresados de la utsem
NOMBRE DEL DESARROLLADOR:alejandro ramirez
FECHA:5/10/2020
---------------------------------------------------------------------------------
-->


@extends('template')



@section('seccion')
<h5 class="text-success">Programa educativo: {{$programa_educativo}}, año de egreso: {{$año_egreso}}</h5>




<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>  
@php $i=0; @endphp
@foreach ($preguntas as $pregunta)
@php $i++; @endphp

<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart<?php echo$i;?>);

  function drawChart<?php echo$i;?>() {

    var data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],   
    <?php foreach ($opciones as $var): ?>
      ['<?php echo $var->opcion_futura ?>',  
      <?php $z=0; ?>
      <?php if ($pregunta->id == $var->opcion_pregunta_id): ?>
        <?php foreach($respuestas as $res): ?>
          <?php if ($res->respuesta_opcion_id == $var->id): ?>
            <?php foreach ($alumnos as $alum): ?>
             <?php if ($alum->alumno_id == $res->respuesta_alumno_id ): ?>
               <?php $z++; ?>
             <?php endif ?>
           <?php endforeach ?>
         <?php endif ?>
       <?php endforeach ?>
     <?php endif ?>
     <?php echo $z ?>],
   <?php endforeach?>

   ]);


   var options = {
    title: '<?php echo $pregunta->pregunta; ?>'
  };

  var chart = new google.visualization.PieChart(document.getElementById('piechart<?php echo$i;?>'));

  chart.draw(data, options);
}
</script>

  <div class="shadow-lg p-3 mb-5 bg-white rounded">
<div class="container row ">

  <div class="col-md-2"></div>
  <div class="col-md-8">
    <div id="recargar">
    
     <div id="piechart<?php echo$i;?>" style="width: 1000px; height: 500px;">
       
     </div>
   </div>
   </div>
 </div>
</div>






@endforeach

@endsection
