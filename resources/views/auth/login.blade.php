@extends('layouts.app')

@section('seccion')
<div class="container">
  <div class="row justify-content-md-center">
    <div class="col-12 d-flex align-items-center justify-content-center">
      <div class="col-lg-5 col-md-8 col-10 box-shadow-2 p-0"><br><br>
        <div class="card border-grey border-lighten-3 px-1 py-1 m-0 p-1 mb-1 bg-white rounded">
          <div class="card-header" style="background-color: white;">
            <div class="card-title text-center">
              <img src="img/logo/logo-utsem.png" style="width:130px;" alt="branding logo">
            </div>         
              <h4 style="text-transform: uppercase; margin-top: -7%;" class="text-muted text-center"><b>Bienvenido</b></h4>
              <h6 style="text-transform: uppercase;" class="text-muted text-center">Sistema de seguimiento</h6>
              <span align="center" class="text-muted text-center" >Si tiene algún problema con la plataforma, escribanos a soporte@utsem-morelos.edu.mx</span>
          </div>

          <div class="card-content" id="userFormContainer">
            <div class="card-body">
             
              <div class="row justify-content-center">
                <div class="col-sm-12">
                  <div class="flash-message">
                   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                     @if(Session::has('alert-' . $msg))
                      @if($msg == 'danger' || $msg == 'success' || $msg == 'info')
                        @php $text = 'white' @endphp
                      @else
                        @php $text = 'dark' @endphp
                      @endif
                     <p class="alert alert-{{$msg}} text-{{$text}}">{{ Session::get('alert-'.$msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                     @endif
                   @endforeach
                  </div>
                </div>
              </div>

              <form id="userForm" class="form-horizontal"  method="POST" action="{{route('login')}}">
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                  <fieldset class="form-group position-relative has-icon-left">
                    <small id="emailHelp" class="form-text text-muted">Correo electronico</small>
                    <input type="text" class="form-control input-sm" id="email" name="email"
                      placeholder="si eres alumno ingresa tu MATRICULA: 20173TI010" value="{{ old('email') }}" required autofocus>
                    {!! $errors->first('email','<span class="help-block text-danger">:message</span>') !!} 
                  </fieldset>
                </div>
                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                  <fieldset class="form-group position-relative has-icon-left">
                    <small id="emailHelp" class="form-text text-muted">Contraseña</small>

                    <div class="input-group md-3">
                      <input type="password" class="form-control input-sm" id="passwordd" name="password"
                      placeholder="si eres alumno usa tu contraseña de SICA: *************" required>
                      <div class="input-group-append">
                        <div class="btn input-group-text" id="mostrarContrasena"><i id="icon" class="far fa-eye"></i></div>
                      </div>
                    </div>
                    {!! $errors->first('password','<span class="help-block text-danger">:message</span>') !!}
                    <div class="form-control-position" style="margin-top: 1%;">
                      <h6 class="card-subtitle line-on-side text-muted font-small-3 pt-2">
                         <label><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        Recordarme</label>
                      </h6>
                    </div>
                  </fieldset>
                </div>
                
                <button type="submit" class="btn btn-outline-secondary btn-block" style="margin-top: 5%;">Acceder</button>
                
                <small class="form-text text-muted" style="margin-top: 20%;">¿Has olvidado tu contraseña? <a href="{{ route('password.request') }}">recuperar</a></small>
                

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

@endsection