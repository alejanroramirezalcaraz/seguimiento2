@extends('layouts.app')

@section('seccion')
<div class="container">
  <div class="row justify-content-md-center">
    <div class="col-12 d-flex align-items-center justify-content-center">
      <div class="col-lg-5 col-md-8 col-10 box-shadow-2 p-0"><br><br><br><br>
        <div class="card border-grey border-lighten-3 px-1 py-1 m-0 p-1 mb-1 bg-white rounded">
            <div class="card-header" style="background-color: white; padding-top: 1.5rem; padding-right: 1.2rem; padding-left: 1.2rem;">        
            <h4 style="text-transform: uppercase;" class="text-muted text-center"><b>Cambia tu contraseña ha hora...</b></h4>
          </div>

          <div class="card-content" id="userFormContainer">
            <div class="card-body">
              <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                    {{ csrf_field() }}

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <small id="emailHelp" class="form-text text-muted col-md">Correo electronico</small>

                        <div class="col-md">
                            <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus placeholder="ingresa tu correo electronico">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <small id="emailHelp" class="form-text text-muted col-md" >Contraseña</small>

                        <div class="col-md">
                            <input placeholder="Contraseña: *************" id="password" type="password" class="form-control" name="password" id="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <small id="emailHelp" class="form-text text-muted col-md">Confirmar Contraseña</small>
                        <div class="input-group col-md">
                            <input placeholder="Confirmar Contraseña: *************" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong class="text-danger">{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif

                            <div class="input-group-append">
                                <div class="btn input-group-text" id="mostrarContrasena"><i id="icon-dos" class="far fa-eye"></i></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md">
                            <button type="submit" class="btn btn-outline-primary btn-block mt-10" id="btnConfirm">
                            </button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>
$('#mensaje_error').hide();
$('#btnConfirm').attr('disabled', true);
$('#btnConfirm').html('Desactivado');

var cambioDePass = function() {
    var cont = $('#password').val();
    var cont2 = $('#password-confirm').val();
    if (cont == cont2) {
        $('#btnConfirm').attr('disabled', false);
        $('#btnConfirm').html('Restablecer contraseña');
    } else {
        $('#btnConfirm').attr('disabled', true);
        $('#btnConfirm').html('Confirmar que ambas contraseñas coincidan...');
    }
}

$("#btnConfirm").on('keyup', cambioDePass);
$("#password-confirm").on('keyup', cambioDePass);

$(document).ready(function () {
  $('#mostrarContrasena').click(function() {
    if ($('#password-confirm').is(':password')) {
      $('#password-confirm').attr('type', 'text');
      $('#icon-dos').removeClass('far fa-eye').addClass('far fa-eye-slash');
    } else {
      $('#password-confirm').attr('type', 'password');
      $('#icon-dos').removeClass('far fa-eye-slash').addClass('far fa-eye');
    }
  });
});
</script>

@endsection
