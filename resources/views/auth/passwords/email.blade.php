@extends('layouts.app')

@section('seccion')
<div class="container">
  <div class="row justify-content-md-center">
    <div class="col-12 d-flex align-items-center justify-content-center">
      <div class="col-lg-5 col-md-8 col-10 box-shadow-2 p-0"><br><br><br><br>
        <div class="card border-grey border-lighten-3 px-1 py-1 m-0 p-1 mb-1 bg-white rounded">
          <div class="card-header" style="background-color: white; padding-top: 1.5rem; padding-right: 1.2rem; padding-left: 1.2rem;">        
            <h4 style="text-transform: uppercase;" class="text-muted text-center"><b>Restablecer Contraseña</b></h4>
          </div>

            <div class="row justify-content-center">
                <div class="col-sm-12">
                  <div class="flash-message">
                   @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                     @if(Session::has('alert-' . $msg))
                     <p class="alert alert-{{ $msg }} text-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                     @endif
                   @endforeach
                  </div>
                </div>
              </div>

          <div class="card-content" id="userFormContainer">
            <div class="card-body">
                @if (session('status'))
                    <p class="alert alert-success">
                        {{ session('status') }}
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                @endif

              <form id="userForm" class="form-horizontal"  method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}
                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                  <fieldset class="form-group position-relative has-icon-left">
                    <small id="emailHelp" class="form-text text-muted">Correo electronico</small>
                    <input type="email" class="form-control" id="email" name="email"
                      placeholder="Correo electronico" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                    @endif 
                  </fieldset>
                </div>

                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                  <fieldset class="form-group position-relative has-icon-left">
                    <small id="emailHelp" class="form-text text-muted">Repitir Correo electronico</small>
                    <input type="email" class="form-control" id="emailConfirm" name="emailConfirm"
                      placeholder="Correo electronico" value="{{ old('emailConfirm') }}" required autofocus>
                    <label id="mensaje_error" style="display: none;">Las constraseñas si coinciden</label>
                  </fieldset>
                </div>
                
                <button type="submit" class="btn btn-outline-primary btn-block mt-10" id="btnConfirm" >
                    enviar correo electronico
                </button>
                    

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>
$('#mensaje_error').hide();
$('#btnConfirm').attr('disabled', true);
$('#btnConfirm').html('Desactivado');

var cambioDePass = function() {
    var cont = $('#email').val();
    var cont2 = $('#emailConfirm').val();
    if (cont == cont2) {
        $('#mensaje_error').hide();
        $('#mensaje_error').attr("class", "control-label col-md-12 text-success");
        $('#mensaje_error').show();
        $('#mensaje_error').html("Los correos si coinciden, ya puede enviar.");
        $('#btnConfirm').attr('disabled', false);
        $('#btnConfirm').html('Enviar correo electronico');
    } else {
        $('#mensaje_error').hide();
        $('#mensaje_error').attr("class", "control-label col-md-12 text-danger");
        $('#mensaje_error').show();
        $('#mensaje_error').html("Los correos no coinciden, por favor verifique.");
        $('#btnConfirm').attr('disabled', true);
        $('#btnConfirm').html('Confirmar ambos correos electronicos...');
    }
}

$("#btnConfirm").on('keyup', cambioDePass);
$("#emailConfirm").on('keyup', cambioDePass);
</script>
@endsection
