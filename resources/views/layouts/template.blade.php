<!-- -------------------------------------------------------------------------------
PROPÓSITO DE ESTA SECCIÓN: es la plantilla general del sistema
NOMBRE DEL DESARROLLADOR:alejandro ramirez
FECHA:2/10/2020
--------------------------------------------------------------------------------- -->
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="{{ asset('logoB.png')}}" rel="icon">
  <title>{{ config('app.name', 'UTSEM') }}</title>
  <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/ruang-admin.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/switch.css') }}" rel="stylesheet">
  <link href="{{ asset('css/spinners.css') }}" rel="stylesheet">
  <link href="{{ asset('datatables/datatables.min.css') }}" rel="stylesheet">
  <link href="{{ asset('datatables/datatables.css') }}" rel="stylesheet">

  <style type="text/css">
  .tooltip-inner{
          background: #00a48d !important;
      }
  .bs-tooltip-top .arrow::before {
      border-top-color: #00a48d !important;
  }
  </style>
</head>

<body id="page-top">
  <div id="wrapper">

    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
       

<!-- Container Fluid-->
        <div class="container-fluid" id="container-wrapper" style="background-color: ghostwhite;">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
          </div>
         
          @yield('seccionalumns')

        </div>
      </div>
    </div>
  </div>

<script src="{{ asset('js/ruang-admin.min.js') }}"></script>
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
<script src="{{ asset('datatables/datatables.min.js')}}"></script>
<script src="{{ asset('datatables/datatables.js')}}"></script>
<script src="{{ asset('js/password.js') }}"></script>

<body>
</html>