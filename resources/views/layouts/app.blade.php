<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="UTSEM">
  <meta name="author" content="Chirinos">
  <link href="{{ asset('logoB.png')}}" rel="icon">
  <title>{{ config('app.name', 'UTSEM') }}</title>
  <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/ruang-admin.min.css') }}" rel="stylesheet" t>
</head>

<body id="page-top">

@yield('seccion')

<script src="{{ asset('js/ruang-admin.min.js') }}"></script>
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
<script src="{{asset('jquery/jquery-3.3.1.min.js')}}"  ></script>
<script src="{{asset('popper/popper.min.js') }}"></script>
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('main.js') }}"></script>  
<script src="{{ asset('js/password.js') }}"></script>

<body>
</html>