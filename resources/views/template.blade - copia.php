<!-- -------------------------------------------------------------------------------
PROPÓSITO DE ESTA SECCIÓN: es la plantilla general del sistema
NOMBRE DEL DESARROLLADOR:alejandro ramirez
FECHA:2/10/2020
--------------------------------------------------------------------------------- -->
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="{{ asset('logoB.png')}}" rel="icon">
  <title>{{ config('app.name', 'UTSEM') }}</title>
  <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/ruang-admin.min.css') }}" rel="stylesheet">
  <!-- <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"> -->
  <link href="{{ asset('datatables/datatables.min.css') }}" rel="stylesheet">
  <link href="{{ asset('datatables/datatables.css') }}" rel="stylesheet">

</head>

<body id="page-top">
  <div id="wrapper">
    <!-- Sidebar ---------------------------------------------------->
    <ul  style="background-color: #1F926F !important;" class="navbar-nav sidebar  accordion" id="accordionSidebar">
      <a  style="background-color: white" class="sidebar-brand d-flex align-items-center
        justify-content-center" href="#!">
        <div class="sidebar-brand-icon">
          <img src="{{ asset('logoB.png')}}">
        </div>
        <div style="color: black;" class="sidebar-brand-text mx-3">SEGUIMIENTO A EGRESADOS </div>
      </a>

      <li class="nav-item" >        
        <a class="nav-link text-white" href="{{-- url('/administrador/inicio')--}}">
          <i class="fas fa-home"></i>
          <span> Inicio </span>
        </a>
      </li>

      <!-- <hr class="sidebar-divider"> --><div class="dropdown-divider"></div>

      <li class="nav-item" >        
        <a class="nav-link text-white" href="{{url('/users')}}">
          <i class="fas fa-users"></i>
          <span>Usuarios</span>
        </a>
      </li>

      <li class="nav-item" >        
        <a class="nav-link text-white" href="{{url('/form')}}">
          <i class="fas fa-users"></i>
          <span>Formulario donamico</span>
        </a>
      </li>

      <li class="nav-item" >        
        <a class="nav-link text-white" href="{{url('/form/send')}}">
          <i class="fas fa-users"></i>
          <span>Formulario</span>
        </a>
      </li>

      <li class="nav-item" >        
        <a class="nav-link text-white" href="{{url('/form/send/email')}}">
          <i class="fas fa-users"></i>
          <span>Enviar Formulario</span>
        </a>
      </li>

      <li class="nav-item" >        
        <a class="nav-link text-white" href="{{url('/programs')}}">
          <i class="fas fa-table"></i>
          <span>Programas educativos</span>
        </a>
      </li>

      <li class="nav-item" >        
        <a class="nav-link text-white" href="{{url('/generations')}}">
          <i class="fas fa-table"></i>
          <span>Generaciones</span>
        </a>
      </li>

       <li class="nav-item" >        
        <a class="nav-link text-white" href="{{url('/dataCapture')}}">
          <i class="fas fa-table"></i>
          <span>Datos en captura</span>
        </a>
      </li>

      <li class="nav-item" >        
        <a class="nav-link text-white" href="{{url('/individualMonitoring')}}">
          <i class="fas fa-star"></i>
          <span>seguimiento individual</span>
        </a>
      </li>

      <li class="nav-item" >        
        <a class="nav-link text-white" href="{{url('/generalMonitoring')}}">
          <i class="fas fa-star"></i>
          <span>seguimiento general</span>
        </a>
      </li>
    </ul>
      <!-- Sidebar -->
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <!-- TopBar -->
        <nav  class="navbar navbar-expand navbar-light  topbar mb-4 static-top">
          <button  id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
            <i class="fa fa-bars text-dark"></i>
          </button>
          <ul class="navbar-nav ml-auto">
            <!--bara superior-->
            <div class="topbar-divider d-none d-sm-block"></div>
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="img-profile rounded-circle" src="{{ asset('img/man.png')}}" style="max-width: 60px">
                <small style="color: black;" class="ml-2 d-none d-lg-inline text-black small text-capitalize">
                 @auth
                  {{ auth()->user()->user_nombre .' '. auth()->user()->user_ap_paterno .' '. auth()->user()->user_ap_materno }}
                    <span class="text-muted">
                      <br>Administrador
                    </span>
                  @endauth 
                </small>
              </a>
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
               <a class="dropdown-item" href="/{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout').submit();"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>Salir</a>
                <form id="logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
              </div>
            </li>
          </ul>
        </nav>
<!-- Topbar -->

<!-- Container Fluid-->
        <div class="container-fluid" id="container-wrapper">
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
          </div>
         
          

          <!-- Footer -->
          <footer class="sticky-footer bg-white">
            <div class="container my-auto">
              <div class="copyright text-center my-auto">
                <span><script> document.write(new Date().getFullYear()); </script> 
                  <b><a href="https://indrijunanda.gitlab.io/" target="_blank"> </a></b>
                </span>
              </div>
            </div>
          </footer>
          <!-- Footer -->
        </div>
      </div>
    </div>
  </div>
<!-- Scroll to top -->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>


<!-- <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('js/demo/chart-area-demo.js') }}"></script>
<script src="{{ asset('js/demo/chart-pie-demo.js') }}"></script>
<script src="{{ asset('js/demo/chart-bar-demo.js') }}"></script>
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
<script src="{{ asset('js/ruang-admin.min.js') }}"></script> -->

<!-- <script src="{{ asset('datatables/datatables.min.js')}}"  type="text/javascript" ></script> -->

<script src="{{ asset('js/ruang-admin.min.js') }}"></script>
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

<!-- <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('js/demo/chart-area-demo.js') }}"></script>
<script src="{{ asset('js/demo/chart-pie-demo.js') }}"></script>
<script src="{{ asset('js/demo/chart-bar-demo.js') }}"></script> -->

<script src="{{ asset('datatables/datatables.min.js')}}"></script>
<script src="{{ asset('datatables/datatables.js')}}"></script>
<!-- <script src="{{ asset('vendor/datatables/jquery.dataTables.js') }}"></script> -->
<!-- <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script> -->

<!-- <script src="{{ asset('js/dataTableConfig.js') }}"></script> -->

<script src="{{ asset('js/password.js') }}"></script>

<!-- <script type="text/javascript" src="{{ asset('main.js') }}"></script>   -->


<body>
</html>

<!-- @extends('layouts.app')

@section('seccion') -->