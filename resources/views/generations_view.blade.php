<!---- 
PROPÓSITO DE ESTA SECCIÓN: es la plantilla general del sistema
NOMBRE DEL DESARROLLADOR:alejandro ramirez
FECHA:2/10/2020

MODIFICADO POR:
alejandro ramirez -> 02/10/2020
jesus chirinos -> 15/02/2020
---->

@extends('template')

@section('seccion')
<div class="shadow p-3 mb-5 bg-white rounded">
  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
    <h5 class="text-success float-left">Generaciones</h5>
    <a type="button" href="javascript:void(0)" class="btn btn-outline-success" data-toggle="modal" data-target="#staticBackdrop" id="add">Agregar<i class="fas fa-plus text-success"></i></a>  
  </div>

  <div class="container ">
    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">        
          <table id="showDataTable" class="table table-striped table-bordered align-items-center table-flush table-hover align-self-center text-center" cellspacing="0" width="100%">
            <thead class="thead-light">
                <tr>
                   <th>#</th>
                  <th>Código</th>
                  <th>Generación</th>
                  <th>Programa</th>
                  <th>Descripción</th>
                  <th>Nivel educativo</th>
                  <th>Modalidad</th>
                  <th>Cuatrimestre de egreso</th>
                  <th>Año de egreso</th>
                  <th>Estado</th>

                  <th>Editar</th>
                  <th>Eliminar</th>
               </tr>
            </thead>
            <tfoot>
              <tr>
                  <th>#</th>
                  <th>Código</th>
                  <th>Generación</th>
                  <th>Programa</th>
                  <th>Descripción</th>
                  <th>Nivel educativo</th>
                  <th>Modalidad</th>
                  <th>Cuatrimestre de egreso</th>
                  <th>Año de egreso</th>
                  <th>Estado</th>

                  <th>Editar</th>
                  <th>Eliminar</th>
               </tr>
            </tfoot>
            <tbody>            
            </tbody>        
          </table>                  
        </div>
      </div>
    </div>  
  </div>    
</div>



<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabeTitulModal">Registro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">

          <div class="row justify-content-center">
            <div class="col-sm-11">
              <div class="alert alert-danger" id="alerta"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              </div>
            </div>
          </div>

           <form class="form-group" action="#" id="formControl" method="POST" enctype="multipart/form-data" >

           {{ csrf_field() }}

          <input type="hidden" name="id" id="id">

          <div class="form-row">
            <div class="form-group col-sm-4">
              <label>Código *</label>
              <input required="" type="text" class="form-control" name="codigo" id="codigo">
              <span class="text-danger span" ></span>      
            </div>

            <div class="form-group col-sm-4">
              <label>Generación *</label>
              <input required="" type="text" class="form-control" name="generacion" id="generacion">
              <span class="text-danger span" id="user_ap_paterno_errors" >El campo es obligatorio</span>
            </div>

            <div class="form-group col-sm-4">
              <label>Programa *</label>
                <select class="form-control" name="programa" id="programas">
                  @foreach ($programas as $programa )
                     <option value="{{$programa->programa_nombre}}">{{$programa->programa_nombre}}</option>
                 @endforeach
              
               
              </select>
            </div>
          </div>
          

          <div class="form-group">
            <label>Descripción *</label>
 
             <input required="" type="text" class="form-control" name="descripcion" id="desc">
              <span class="text-danger span" id="user_nombre_errors"></span>   
          </div>

          <div class="form-row">
            <div class="form-group col-sm-6">
              <label>Nivel *</label>
               <select class="form-control" name="nivel" >
                
                  <option value="TSU">TSU</option>
                <option value="LICENCIATURA">LICENCIATURA</option>
              </select>

              
            </div>

            <div class="form-group col-sm-6">
              <label>Modalidad</label>
             <input required="" type="text" class="form-control" name="modalidad" id="modalidad">
              <span class="text-danger span" id="user_nivel_errors" >El campo es obligatorio</span>
            </div>
          </div>

         <div class="form-row">
            <div class="form-group col-sm-4">
              <label>Cuatrimestre *</label>
                <select class="form-control" name="cuatrimestre" id="user_estatus">
               <option value="Mayo - Agosto">Mayo - Agosto</option>
                <option value="Enero - Abril">Enero - Abril</option>
              </select>    
            </div>

            <div class="form-group col-sm-4">
              <label>Año de egreso *</label>
              <input required="" type="text" class="form-control" name="año" id="año">
              <span class="text-danger span" id="user_ap_paterno_errors" >El campo es obligatorio</span>
            </div>

            <div class="form-group col-sm-4">
              <label>Estado *</label>
           <select class="form-control" name="estado" id="user_estatus">
               <option value="1">Activo</option>
                <option value="0">Inactivo</option>
              </select>
            </div>
          </div>

          <button type="submit" class="btn btn-outline-success float-right mb-3 mt-3" id="btn">Cargando</button>
        </form>
      </div>
    </div>
  </div>
</div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css"/>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

<script>
$(function(){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

    $('#alerta').hide();
    $('.span').hide();

    var table = $('#showDataTable').DataTable({
        // dom: 'fB<"top"l>rt<"bottom"ip><"clear">',
        dom: '<"float-left"Bl><"float-right"f>t<"float-left"i><"float-right"p>',
        buttons: {
            dom: {
                button: {
                    className: 'btn btn-outline-secundary mr-1 mb-2' //Primary class for all buttons
                }
            },
            buttons: [                  
                {
                    extend: 'excelHtml5', 
                    className: 'btn btn-outline-success',
                    title: 'reporte_en_excel',
                },
                {
                    extend: 'pdf',
                    className: 'btn btn-outline-danger',
                    title: 'reporte_en_pdf',
                },
                {
                    extend: 'copy',
                    className: 'btn btn-outline-info',
                    // text: '<i class="fas fa-print"></i> IMPRIMIR',
                    title: 'Copiado',
                }
            ]
        },
        retrieve: true,
        deferRender: true,
        processing: true,
        serverSide: true,
        ajax: "{{ route('index.generaciones') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
             {data: 'generacion_codigo', name: 'generacion_codigo'}, 
              {data: 'generacion', name: 'generacion'},
            {data: 'generacion_programa_educativo', name: 'generacion_programa_educativo'},
          
           
             {data: 'generacion_programa_descripcion', name: 'generacion_programa_descripcion'},
              {data: 'generacion_nivel', name: 'generacion_nivel'},
            {data: 'generacion_modalidad', name: 'generacion_modalidad'},
            {data: 'generacion_cuatrimestre_egreso', name: 'generacion_cuatrimestre_egreso'},
            {data: 'generacion_anio', name: 'generacion_anio',
            'render': function(data, type, row) {
                return '<span class="badge badge-primary">'+data+'</span>';
            }},
            {data: 'generacion_estatus', width: '10', className: 'text-center',
            'render': function(data, type, row) {
                return (data == 1) ? '<span class="badge text-success">Activo</span>' 
                    : '<span class="badge text-danger"> Inactivo </span>';
            }},
            {data: 'edit', name: 'edit'},
            {data: 'delete', name: 'delete', orderable: false, searchable: false},
        ],"language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando del _START_ al _END_ con un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          },
          "buttons": {
              "copy": "Copiar",
              "colvis": "Visibilidad",
              "pdf": "<i class='fas fa-file'></i>",
              "excel": "<i class='fas fa-file-excel'></i>"
          }
      }
    });


    $('#add').click(function () {
        $('#formControl').trigger("reset");
        $('#alerta').hide();
        $(".span").hide();
        $("#staticBackdropLabeTitulModal").show().html('Registrar Usuario');
        $('#btn').show().html('Registrar');
        $("#formControl").attr("action","{{Route('created.generaciones')}}");
    });



$("#programas").on('click', function (e) {
  e.preventDefault();
  
  var nombre = $(this).val();

          
   
   
    $.post("{{url('/generaciones/nombre/')}}/"+nombre, { "nombre" : nombre })
      .done(function(nombre, textStatus, jqXHR ) { 
       console.log(nombre);




$.each(nombre, function(i, value){
$.each(value, function(i, valued){
  console.log(valued.programa_descripcion);


if(valued.programa_descripcion == 'undefined'){
 var data = '';
                $('#programadescripcion').html(data);
} else {
          document.getElementById('desc').value = valued.programa_descripcion;
$('#programadescripcion').html(data);
}




        })

        })




      })
      .fail(function(jqXHR, textStatus, errorThrown ) {
          if ( console && console.log ) {
              console.log( "Algo ha fallado: " +  textStatus );
          }
    });

});




   $('body').on('click', '#edit', function () {
      var id = $(this).data('id');
   
      $.get("{{url('/generaciones/search/edit/')}}" +'/'+ id, function (data) {
          $("#staticBackdropLabeTitulModal").show().html('<h6>Programa: '+data.generacion_programa_educativo+ '</h6>');
          $('#alerta').hide();
          $(".span").hide();
          $('#btn').show().html('Actualizar');
          $("#formControl").attr("action","{{url('/generaciones/update/')}}" +'/'+ id);
          $('#codigo').val(data.generacion_codigo);
          $('#generacion').val(data.generacion);
          $('#modalidad').val(data.generacion_modalidad);
          $('#año').val(data.generacion_anio)

          
          
      })
    });


    $(document).ready(function(){
      $('#formControl').on('submit', function(e){
        e.preventDefault();
        $.ajax({
          url: $(this).attr('action'),
          type: $(this).attr('method'),
          data: $(this).serialize(),
          dataType: 'json',
          beforeSend: function(){
            $('#btn').fadeIn().html('Enviando...');
            setTimeout(function(){ $('#btn').fadeIn() }, 1000);
          },
          success: function(response){
            // $('#formControl').trigger("reset");
            $('#staticBackdrop').modal('hide');
            $("#formControl")[0].reset();
            if (response.error == false) {
              toastr.success(response.alert, "Usuarios", {
                  "closeButton": true,
                  "debug": true,
                  "newestOnTop": false,
                  "progressBar": true,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "showDuration": "13000",
                  "hideDuration": "1000",
              });
              table.draw();
            }else if (response.error == true){
              toastr.error(response.alert, "Usuarios", {
                  "closeButton": true,
                  "debug": true,
                  "newestOnTop": false,
                  "progressBar": true,
                  "positionClass": "toast-top-right",
                  "preventDuplicates": false,
                  "showDuration": "13000",
                  "hideDuration": "1000",
              });
            }   
          },
          error: function(response){
            if (response.status === 422) {
              var saltos = '';
              var errors = response.responseJSON.errors;
              $.each(errors, function(key, val) {
                $('#alerta').show().html(saltos += val[0] + '<br>').fadeIn();
                setTimeout(function(){ $('#alerta').fadeOut(); }, 10000);
              });
              if (errors['user_nombre']) {
                $('#user_nombre_errors').show().html(errors['user_nombre'][0]);
              }
              if (errors['user_ap_paterno']) {
                $('#user_ap_paterno_errors').show().html(errors['user_ap_paterno'][0]);
              }
              if (errors['user_ap_materno']) {
                $('#user_ap_materno_errors').show().html(errors['user_ap_materno'][0]);
              }
              if (errors['user_telefono']) {
                $('#user_telefono_errors').show().html(errors['user_telefono'][0]);
              }
              if (errors['user_nivel']) {
                $('#user_nivel_errors').show().html(errors['user_nivel'][0]);
              }
              if (errors['email']) {
                $('#email_errors').show().html(errors['email'][0]);
              }
              if (errors['password']) {
                $('#password_errors').show().html(errors['password'][0]);
              }
            }
          }
        });
      });
    });


   $(document).on('click', '#delete', function () {
      var id = $(this).val();
      var user = $(this).data('id');
      swal({
        title: "¿Seguro que deseas continuar?",
        text: "Eliminar a "+user+", no podrás deshacer este paso!",
        icon: "warning",
        buttons: [true, "¡Aceptar!"],
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajax({
            method: 'DELETE',
            url: "{{url('/generaciones/delete/')}}/"+id,
            dataType: 'json',
            data: {id:id},
            success: function (response) {
              if (response.error == false) {
                swal({
                  title : "Correcto!",
                  text  : response.alert,
                  icon  : "success",
                })
                table.draw();
              }else if (response.error == true){
                swal({
                  title : "Ocurrio un error dentro del servidor!",
                  text  : response.alert,
                  icon  : "error",
                });
              }
            }
          });
        } else {
          swal({title : "Cancelado",
            text  : "Tu registro está seguro..."});
        }
      });
    });


  });





</script>

@endsection