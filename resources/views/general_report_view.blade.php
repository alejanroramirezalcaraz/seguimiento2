<!--
----------------------------------------------------------------------------------
PROPÓSITO DE ESTA SECCIÓN: vista donde se muestera  el data table de los programas educativos 
NOMBRE DEL DESARROLLADOR:alejandro ramirez
FECHA:4/10/2020
---------------------------------------------------------------------------------
-->
@extends('template')

@section('seccion')

<div class="shadow p-3 mb-5 bg-white rounded">
  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
    <span class="float-left">
      <h5 class="text-success">Listado de programas</h5>
    </span>
    <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#staticBackdrop">Agregar<i class="fas fa-plus text-success"></i></button>  
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="table-responsive">        
          <table id="showDataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
             <tr>
               <th>NOMBRE DE CARRERA</th>
               <th>CÓDIGO SIESE</th>
               <th>NIVEL EDUCATIVO</th>
               <th>MODALIDAD BIS / TRADICIONAL</th>
               <th>CUATRIMESTRE DE EGRESO</th>
               <th>AÑO DE EGRESO</th>
               @foreach ($preguntas as $pregunta)
               <th>{{$pregunta->pregunta}}</th>
               @foreach ($opciones as $opcion)
               @if ( $opcion->opcion_pregunta_id == $pregunta->id)
               <th style="background-color:white;">{{$opcion->opcion_futura}}</th>
               @endif 
               @endforeach
               @endforeach
             </tr>
           </thead>
           <tbody>
             @php $i=1; @endphp
             @foreach ($listado as $key )
             <tr>
              <td>{{$key->listado_programa_educativo}}</td>
              <td>{{$key->listado_codigo}} </td>
              <td>{{$key->listado_nivel_educativo}}</td>
              <td>{{$key->listado_modalidad}}</td>
              <td>{{$key->listado_cuatrimestre_de_egreso}}</td>
              <td>{{$key->listado_anio_egreso}}</td>     
              @foreach ($preguntas as $pregunta)
              <!--se genera el espacio de la pregunta-->
              <th></th>
              @foreach ($opciones as $opcion)
              @php $z=0; @endphp
              @if ( $opcion->opcion_pregunta_id == $pregunta->id)
              @foreach($respuestas as $respuesta)
              @if ($respuesta->respuesta_opcion_id == $opcion->id)
              @foreach ($alumnos as $alumno)
              @if ($alumno->alumno_id == $respuesta->respuesta_alumno_id )
              @if ($alumno->alumno_programa_educativo == $key->listado_programa_educativo)
              @php $z++; @endphp
              @endif
              @endif
              @endforeach
              @endif
              @endforeach
              <td>{{$z}}</td>
              @endif     
              @endforeach
              @endforeach           
            </tr>
            @endforeach 
          </tbody>        
        </table>                  
      </div>
    </div>
  </div>  
</div>    
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css"/>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

<script>
  $(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $('#alerta').hide();
    $('.span').hide();

    var table = $('#showDataTable').DataTable({
        // dom: 'fB<"top"l>rt<"bottom"ip><"clear">',
        dom: '<"float-left"Bl><"float-right"f>t<"float-left"i><"float-right"p>',
        buttons: {
          dom: {
            button: {
                    className: 'btn btn-outline-secundary mr-1 mb-2' //Primary class for all buttons
                  }
                },
                buttons: [                  
                {
                  extend: 'excelHtml5', 
                  className: 'btn btn-outline-success',
                  title: 'reporte_en_excel',
                },
                {
                  extend: 'pdf',
                  className: 'btn btn-outline-danger',
                  title: 'reporte_en_pdf',
                },
                {
                  extend: 'copy',
                  className: 'btn btn-outline-info',
                    // text: '<i class="fas fa-print"></i> IMPRIMIR',
                    title: 'Copiado',
                  }
                  ]
                },

                "language": {
                  "sProcessing":     "Procesando...",
                  "sLengthMenu":     "Mostrar _MENU_ registros",
                  "sZeroRecords":    "No se encontraron resultados",
                  "sEmptyTable":     "Ningún dato disponible en esta tabla",
                  "sInfo":           "Mostrando del _START_ al _END_ con un total de _TOTAL_ registros",
                  "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
                  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                  "sInfoPostFix":    "",
                  "sSearch":         "Buscar:",
                  "sUrl":            "",
                  "sInfoThousands":  ",",
                  "sLoadingRecords": "Cargando...",
                  "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                  },
                  "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                  },
                  "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad",
                    "pdf": "<i class='fas fa-file'></i>",
                    "excel": "<i class='fas fa-file-excel'></i>"
                  }
                }
              });



  });
</script>

@endsection