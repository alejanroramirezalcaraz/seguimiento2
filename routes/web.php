<?php

//login
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


//Password Reset
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


// Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


// Users
Route::get('/users', 'UsersController@index')->name('index.users');
Route::post('/users/created', 'UsersController@created')->name('created.users');
Route::get('/users/search/edit/{id}', 'UsersController@edit');
Route::post('/users/update/{id}', 'UsersController@update')->name('update.users');
Route::delete('/users/delete/{id}', 'UsersController@delete');


//reportes
Route::get('/report', 'ReportsController@index')->name('index.reports');
Route::post('/report/created', 'ReportsController@created')->name('created.reports');
Route::delete('/report/delete/{id}', 'ReportsController@delete');
Route::get('/report/search/edit/{id}', 'ReportsController@edit');
Route::post('/report/update/{id}', 'ReportsController@update')->name('update.report');


//Generaciones
Route::get('/generaciones', 'GenerationsController@index')->name('index.generaciones');
Route::post('/generaciones/created', 'GenerationsController@created')->name('created.generaciones');
Route::delete('/generaciones/delete/{id}', 'GenerationsController@delete');
Route::post('/generaciones/nombre/{nombre}', 'GenerationsController@json');
Route::get('/generaciones/search/edit/{id}', 'GenerationsController@edit');
Route::post('/generaciones/update/{id}', 'GenerationsController@update')->name('update.generaciones');

//Form
Route::get('/form', 'FormularioController@index')->name('index.forms');
Route::post('/form/created', 'FormularioController@create')->name('created.forms');
Route::get('/form/search/edit/{id}', 'FormularioController@edit');
Route::post('/form/update/{id}', 'FormularioController@update')->name('update.forms');
Route::delete('/form/delete/{id}', 'FormularioController@destroy');


//FormPregDin
Route::get('/form/preg/{id}/{token}', 'FormularioPregDinaController@indexFormSend');
Route::post('/form/update/preg/din/{id}', 'FormularioPregDinaController@updateForm');
Route::post('/form/create/preg', 'FormularioPregDinaController@createPregForm')->name('create.preg');
Route::post('/form/create/preg/opc', 'FormularioPregDinaController@createPregOpc')->name('create.preg.opc');
Route::delete('/form/delete/preg/din/opc/all/{id}', 'FormularioPregDinaController@deleteAllOpc');
Route::delete('/form/delete/preg/din/{id}', 'FormularioPregDinaController@deletePregDin');
Route::delete('/form/delete/preg/din/opc/{id}', 'FormularioPregDinaController@deletePregDinOpc');


//FormEmail
Route::get('/form/send/email', 'FormularioSendController@index');
Route::post('/form/search/alumno/{programa}/{generacion}', 'FormularioSendController@formSearchAlum');
Route::post('/form/send/form/email', 'FormularioSendController@sendFormEmail')->name('send.form.email');

Route::get('/form/encuesta/{forID}/{aluID}/{token}/{codigo}', 'FormularioContestController@formEncuesta');
Route::get('/form/encuesta/opcion/{pregID}', 'FormularioContestController@formOpcion');
Route::post('/form/encuesta/add', 'FormularioContestController@formEncuestaAdd')->name('create.encuesta');
Route::get('/form/encuesta/view/end','FormularioContestController@formViewAlumnsEnd');
// Route::post('/form/encuesta/view/end','FormularioContestController@formViewAlumnsEnd')->name('end.encuesta');
// Route::get('/form/encuesta/view/end/{cod}/{data}','FormularioContestController@formViewAlumnsEnd');


Route::post('/lista', 'DataCaptureController@lista')->name('lista');

Route::get('/reports', 'ReportsController@index')->name('reports');
Route::get('/history', 'backgroundController@index')->name('history');
Route::post('/students', 'studentController@index')->name('students');

Route::post('/report2', 'DataCaptureController@report2')->name('report2');

Route::get('/prueba', 'studentController@prueba')->name('prueba');

Route::post('/excel', 'DataCaptureController@excel_reporte')->name('excel');

Route::get('/dataCapturePrograms/{id}', 'DataCaptureController@programsCapture')->name('dataCapturePrograms');
Route::get('/dataCapture', 'DataCaptureController@index')->name('dataCapture');
Route::get('/programs', 'EducationalProgramsController@index')->name('programs');
Route::get('/generations', 'GenerationsController@index')->name('generations');
Route::get('/individualMonitoring', 'IndividualMonitoringController@index')->name('individualMonitoring');
Route::post('/generalMonitoring', 'GeneralMonitoringController@index')->name('generalMonitoring');
Route::get('/generalMonitoring2/{id}', 'GeneralMonitoringController@grafic')->name('generalMonitoring');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
