<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableListado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('listado', function (Blueprint $table) {
            $table->increments('listado_id');
             $table->integer('listado_reporte_id')->unsigned()->nullable();   
            $table->foreign('listado_reporte_id')
                  ->references('reporte_id')
                  ->on('reportes')
                  ->onDelete('cascade');
            $table->string('listado_programa_educativo', 125);
            $table->string('listado_codigo', 125);
            $table->string('listado_nivel_educativo', 125);
            $table->string('listado_modalidad', 125);
            $table->string('listado_cuatrimestre_de_egreso', 125);
            $table->string('listado_anio_egreso', 125);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     Schema::dropIfExists('listado');
    }
}
