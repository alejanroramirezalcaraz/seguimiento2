<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRespuestas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuestas', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('respuesta_reporte_id')->unsigned()->nullable();   
            $table->foreign('respuesta_reporte_id')
                ->references('reporte_id')
                ->on('reportes')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('respuesta_formulario_id')->unsigned()->nullable();   
            $table->foreign('respuesta_formulario_id')
                ->references('id')
                ->on('formularios')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('respuesta_pregunta_id')->unsigned()->nullable();   
            $table->foreign('respuesta_pregunta_id')
                ->references('id')
                ->on('preguntas')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('respuesta_opcion_id')->unsigned()->nullable();   
            $table->foreign('respuesta_opcion_id')
                ->references('id')
                ->on('opciones')
                ->onDelete('cascade')
                ->onUpdate('cascade');


            $table->integer('respuesta_alumno_id')->unsigned()->nullable();   
            $table->foreign('respuesta_alumno_id')
                ->references('alumno_id')
                ->on('alumnos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->string('respuesta', 100)->nullable();
            $table->string('respuesta_codigo', 45)->nullable();
            $table->string('respuesta_status', 25);
            // $table->string('respuesta_opcion', 40);
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuestas');
    }
}
