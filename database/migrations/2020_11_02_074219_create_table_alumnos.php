<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAlumnos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('alumnos', function (Blueprint $table) {
            $table->increments('alumno_id');
            $table->string('alumno_nombre', 60);
            $table->string('alumno_ap_paterno', 50);
            $table->string('alumno_ap_materno', 50);
            $table->string('alumno_genero', 30);
            $table->string('alumno_matricula', 40);
            $table->string('alumno_generacion', 30);
            $table->string('alumno_programa_educativo', 80);
            $table->string('email', 100);
            $table->string('alumno_telefono', 45);
            $table->string('alumno_anio_egreso', 25);
            $table->string('alumno_nivel', 40);
            $table->string('alumno_situacion', 45);
            $table->timestamps();   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     Schema::dropIfExists('alumnos');
    }
}
