<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeblePrograms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('programas_educativos', function (Blueprint $table) {
            $table->increments('programas_id');
            $table->string('programa_codigo', 125);
            $table->string('programa_nombre', 125);
            $table->string('programa_descripcion', 125);
            $table->string('programa_area', 125);
            $table->string('programa_estatus', 125);
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('programas_educativos');
    }
}
