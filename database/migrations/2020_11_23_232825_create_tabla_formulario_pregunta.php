<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablaFormularioPregunta extends Migration
{
    public function up()
    {
        Schema::create('formulario_pregunta', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->uuid('token')->unique()->nullable()->index();         

            $table->integer('formulario_id')->unsigned()->nullable();
            $table->foreign('formulario_id')
                    ->references('id')
                    ->on('formularios')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->integer('pregunta_id')->unsigned()->nullable();
            $table->foreign('pregunta_id')
                    ->references('id')
                    ->on('preguntas')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            // $table->string('body');
            // $table->morphs('commentable');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('formulario_pregunta');
    }
}
