<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFormularioAlumno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulario_alumno', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('formulario_id')->unsigned()->nullable();
            $table->foreign('formulario_id')
                    ->references('id')
                    ->on('formularios')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->integer('alumno_id')->unsigned()->nullable();
            $table->foreign('alumno_id')
                    ->references('alumno_id')
                    ->on('alumnos')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->string('email', 100);
            $table->string('token', 150);
            $table->dateTime('fecha_publicacion',0);

            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('formulario_alumno');
    }
}
