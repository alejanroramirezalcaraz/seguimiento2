<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOpciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('opciones', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('opcion_pregunta_id')->unsigned()->nullable();   
            $table->foreign('opcion_pregunta_id')
                    ->references('id')
                    ->on('preguntas')
                    ->onDelete('cascade');
            $table->string('opcion', 40);
            $table->string('opcion_futura', 80)->nullable();
            $table->bigInteger('opcion_token');
            $table->string('opcion_futura_token', 30);
            $table->timestamps();        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    Schema::dropIfExists('opciones');
    }
}
