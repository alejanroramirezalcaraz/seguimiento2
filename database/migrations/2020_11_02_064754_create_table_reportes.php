-<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReportes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reportes', function (Blueprint $table) {
            $table->increments('reporte_id');
            $table->string('reporte_codigo', 125);
            $table->string('reporte_nombre', 125);
            // $table->string('reporte_respuesta_codigo', 45);
            // $table->bigInteger('reporte_formulario_id');

            // $table->integer('formulario_id')->unsigned()->nullable();
            // $table->foreign('formulario_id')
            //         ->references('id')
            //         ->on('formularios')
            //         ->onDelete('cascade')
            //         ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    Schema::dropIfExists('reportes');
    }
}
