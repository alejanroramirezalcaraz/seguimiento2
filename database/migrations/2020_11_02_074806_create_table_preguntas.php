<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePreguntas extends Migration
{
    public function up()
    {
         Schema::create('preguntas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pregunta', 125);
            // $table->set('pregunta_obligatorio', ['off', 'on']);
            $table->enum('pregunta_obligatorio', ['off', 'on'])->default('on')->nullable();
            // $table->string('pregunta_obligatorio', 25);
            $table->bigInteger('pregunta_token');
            $table->timestamps();
        
        });
    }

    // php artisan migrate:fresh --seed
    // php artisan make:migration create_tabla_formulario_pregunta
    // php artisan make:migration create_tabla_pregunta_opcion
    public function down()
    {
        Schema::dropIfExists('preguntas');
    }
}
