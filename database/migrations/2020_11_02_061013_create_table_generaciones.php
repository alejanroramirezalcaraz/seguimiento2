<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGeneraciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('generaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('generacion_codigo', 45);
             $table->string('generacion', 40);
            // $table->string('generacion_programa_educativo', 125);
            // $table->integer('generacion_programa_educativo_id')->unsigned()->nullable();   
            // $table->foreign('generacion_programa_educativo_id')
            //       ->references('programas_id')
            //       ->on('programas_educativos')
            //       ->onDelete('cascade');
            $table->string('generacion_programa_educativo', 40);
            $table->string('generacion_programa_descripcion', 80);
            $table->string('generacion_nivel', 40);
            $table->string('generacion_modalidad', 40);
            $table->string('generacion_cuatrimestre_egreso', 40);
             $table->string('generacion_anio', 40);
            $table->string('generacion_estatus', 20);
            
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('generaciones');
    }
}
