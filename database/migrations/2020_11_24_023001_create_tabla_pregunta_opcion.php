<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablaPreguntaOpcion extends Migration
{
    public function up()
    {
        Schema::create('pregunta_opcion', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->uuid('token')->unique()->nullable()->index();

            $table->integer('pregunta_id')->unsigned()->nullable();
            $table->foreign('pregunta_id')
                    ->references('id')
                    ->on('preguntas')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->integer('opcion_id')->unsigned()->nullable();
            $table->foreign('opcion_id')
                    ->references('id')
                    ->on('opciones')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    
    public function down()
    {
    	Schema::dropIfExists('pregunta_opcion');
    }
}
