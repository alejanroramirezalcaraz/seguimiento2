<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFormularios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formularios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('formulario', 45);
            $table->string('formulario_descripcion', 80);
            $table->integer('formulario_estatus');
            $table->dateTime('formulario_fecha_publicacion_inicio',0);
            $table->dateTime('formulario_fecha_publicacion_fin', 0);
            $table->string('formulario_token', 150);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formularios');
    }
}
