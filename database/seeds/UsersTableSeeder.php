<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
	private $users= array(
        array(  'user_nombre'  => 'admin',
	            'user_ap_paterno'  => 'admin',
	            'user_ap_materno'  => 'admin',
	            'user_telefono'  => 1111111111,
	            'user_nivel'  => 'administrador',
	            'user_estatus'  => 1,
	            'email'     => 'admin@admin.com',
	            'password' => 'admin'),
         array( 'user_nombre'  => 'subadmin',
	            'user_ap_paterno'  => 'subadmin',
	            'user_ap_materno'  => 'subadmin',
	            'user_telefono'  => 1111111110,
	            'user_nivel'  => 'subadministrador',
	            'user_estatus'  => 0,
	            'email'     => 'subadmin@subadmin.com',
	            'password' => 'subadmin'),
	);

    //contador de filas creadas php artisan migrate:fresh --seed
    private $rows = 0;
    public function userSeed() {
        for($i=0;$i<count($this->users);$i++){

            $this->command->info($this->users[$i]['email']);
            $this->command->info($this->users[$i]['password']);

            $user_seed = new User();            
            $user_seed->user_nombre = $this->users[$i]['user_nombre'];
            $user_seed->user_ap_paterno = $this->users[$i]['user_ap_paterno'];
            $user_seed->user_ap_materno = $this->users[$i]['user_ap_materno'];
            $user_seed->user_telefono = $this->users[$i]['user_telefono'];
            $user_seed->user_nivel = $this->users[$i]['user_nivel'];
            $user_seed->user_estatus = $this->users[$i]['user_estatus'];
            $user_seed->email = $this->users[$i]['email'];
            $user_seed->password = Hash::make($this->users[$i]['password']);
            $user_seed->save();

            $this->rows++;
        }
    }

    //function run
    public function run()
    {
        self::userSeed();
        $this->command->info($this->rows.' :Users cargados!');

    }

    
}
