$(document).ready(function () {
  $('#mostrarContrasena').click(function() {
    if ($('#passwordd').is(':password')) {
      $('#passwordd').attr('type', 'text');
      $('#icon').removeClass('far fa-eye').addClass('far fa-eye-slash');
    } else {
      $('#passwordd').attr('type', 'password');
      $('#icon').removeClass('far fa-eye-slash').addClass('far fa-eye');
    }
  });
});